var express = require('express');
var path = require('path');
//var favicon =require('serve-favicon');
//var session = require('express-session');
var orm = require("orm");
var logger = require('morgan');
var expressSession = require('express-session');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var properties = require('./utils/properties');
var LocalStrategy = require('passport-local').Strategy;
//var path = require("path");
var routes = require('./routes/index');
var loginRoutes = require('./routes/login');
var controllers = require('./routes/controllers');
var users = require('./routes/users');
var generalParameters = require('./routes/general_parameter');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var YahooStrategy = require('passport-yahoo-oauth').Strategy;


passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

passport.use(new GoogleStrategy({
        clientID: '629495738977-me8qai84t06fsve387r85e0rn9p9v3n0.apps.googleusercontent.com',
        clientSecret: 'kikuTSF13pWh1no59rf7lSa9',
        callbackURL: "http://127.0.0.1:3000/auth/google/callback"
    },
    function (accessToken, refreshToken, profile, done) {
        var email = profile.id;

        Account.findByUsername(email, function (err, obj) {
            if (err) {
                console.error(err);
                return done(err, false);
            } else {
                //profile.identifier = identifier;
                if (obj) {
                    return done(null, obj);
                } else {
                    Account.register(new Account({
                        username: email,
                        name: profile.name.givenName,
                        family: profile.name.familyName,
                        email: email,
                        image:profile.photos[0].value,
                        role: 'google-user'
                    }), "google-oauth", function (err, account) {
                        if (err) {
                            console.error(err);
                            return done(null,false);
                        } else {
                            return done(null, account);
                        }
                    });
                }
            }
        });
    }
));

passport.use(new YahooStrategy({
        consumerKey: "dj0yJmk9NUc3Y2RQWnY2SjZHJmQ9WVdrOVRFWnhibVZPTkdFbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1mMQ--",
        consumerSecret: "58f7c23bd96832879770978f87d569e1ce449a00",
        callbackURL: "http://pooya-pc.com:3000/auth/yahoo/callback"
    },
    function (token, tokenSecret, profile, done) {
        var email = profile.id;

        Account.findByUsername(email, function (err, obj) {
            if (err) {
                console.error(err);
                return done(err, false);
            } else {
                //profile.identifier = identifier;
                if (obj) {
                    return done(null, obj);
                } else {
                    Account.register(new Account({
                        username: email,
                        name: profile.displayName,
                        family: profile.name.familyName,
                        email: email,
                        image:'app/resources/images/admin.png',
                        role: 'yahoo-user'
                    }), "yahoo-oauth", function (err, account) {
                        if (err) {
                            console.error(err);
                            res.redirect("/register?exception");
                        } else {
                            return done(null, account);
                        }
                    });
                }
            }
        });
    }
));

var app = express();


//app.use(session({ secret: 'keyboard cat' }));
//app.use(passport.session());


app.use("/app", express.static(path.join(__dirname + "/../public/app")));
// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
//app.use(express.methodOverride());
app.use(expressSession(
    {secret: 'tobo!', cookie: {maxAge: new Date(Date.now() + 3600000)}}
));

app.use(express.static(path.join(__dirname, 'public')));
//app.use(require('express-session')({
//    secret: 'keyboard cat',
//    resave: false,
//    saveUninitialized: false
//}));
app.use(passport.initialize());
app.use(passport.session());


app.get('/', ensureAuthenticated, function (req, res) {
    res.sendFile(path.resolve('../views/index.html'), {user: req.user});
});

app.get('/auth/yahoo',
    passport.authenticate('yahoo'));

app.get('/auth/yahoo/callback',
    passport.authenticate('yahoo', {failureRedirect: '/start'}),
    function (req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
    });
app.get('/auth/google',
    passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }),
    function(req, res){
        // The request will be redirected to Google for authentication, so this
        // function will not be called.
    });

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    function(req, res) {
        res.redirect('/');
    });
//app.use(passport.initialize());
//app.use(passport.session());
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
//passport.serializeUser(Account.serializeUser());
//passport.deserializeUser(Account.deserializeUser());


mongoose.connect('mongodb://localhost/newsreader');

app.all('/ir/*', ensureAuthenticated, function (req, res, next) {
    //if (req.params === '/' || req.params === '/login') {
    next();
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/start');
}


app.use('/ir', controllers);
//app.use('/ir/phsys/sitereader/users', users);
//app.use('/ir/phsys/sitereader/generalparameters', generalParameters);

app.use('/', routes);
app.use('/login', loginRoutes);

//app.use('/users', users);
//passport.serializeUser(function (user, done) {
//    console.log('serializeUser: ' + user.username);
//    done(null, user.username);
//});
//
//passport.deserializeUser(function (id, done) {
//    //db.users.findById(id, function (err, user) {
//    //    console.log(user)
//    //    if (!err) done(null, user);
//    done(null, {name:"pooya"});
//    //    done(err, null)
//    //});
//});
//passport.use(new LocalStrategy(
//    function (username, password, done) {
//
//        //UserManagement.findOne({ username: username }, function (err, user) {
//        //    if (err) { return done(err); }
//        //    if (!user) {
//        //        return done(null, false, { message: 'Incorrect username.' });
//        //    }
//        //    if (!user.validPassword(password)) {
//        //        return done(null, false, { message: 'Incorrect password.' });
//        //    }
//        //    return done(null, user);
//        //});
//        console.log(this);
//        if (username == "pooya" && password == "123") {
//            return done(null, {username:username});
//        }
//    }
//));
//
//app.post('/login', passport.authenticate('local', {
//    successRedirect: '/',
//    failureRedirect: '/login'
//}));

//629495738977-me8qai84t06fsve387r85e0rn9p9v3n0.apps.googleusercontent.com
//kikuTSF13pWh1no59rf7lSa9

module.exports = app;