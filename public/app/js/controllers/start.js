var startModule = angular.module('application', ['ui.bootstrap', 'services', 'ngRoute', 'ngResource']);
startModule.controller('StartController', ['translator', '$scope', 'UtilService', function (translator, $scope, UtilService) {

    $scope.dynamicPopover = {
        loginTemplateUrl: 'loginTemplate.html',
        langTemplateUrl: 'languageTemplate.html',
        langTitle: 'Language',
        loginTitle: 'Login'
    };

    $scope.getLocaleLabel = function () {
        var locale=$scope.getLocale();
        switch (locale.toLowerCase()) {
            case "en_us":
                return "English";
            case "fa_ir":
                return "فارسی";
            case "de_de":
                return "Deutsch";
        }
    };

    (function () {
        var promises = [];
        promises.push(UtilService.loadPropertyFilePromise('app/resources/start.i18n.properties', function (x) {
            return translator.load(x);
        }));

        UtilService.loadAllPromises(promises).then(
            function () {
                $scope.i18nLoaded = true;
            }, function () {
                $scope.i18nLoaded = false;
            }
        );
    })();
}]);