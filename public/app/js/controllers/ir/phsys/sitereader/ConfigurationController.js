/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */



(function (module) {

    module.controller('ConfigurationCtrl', ['$scope', '$timeout', '$http', 'ModelService', 'ConfigurationRestService', '$location', 'PrintService'
        , 'ConfigurationCrudService',
        function ($scope, $timeout, $http, ModelService, ConfigurationRestService, $location, PrintService
            , ConfigurationCrudService) {

            $scope.config = 'borna {' +
                'name = "خبرگزاری برنا"' +
                'urls = ["http://www.bornanews.ir/feeds/"]' +
                'dateFormat = "E, dd MMM yyyy HH:mm:ss"' +
                'followRedirects=true' +
                'selects = [' +
                '"div.article-body>p",' +
                '"div.article-body>div",' +
                '"div.article-body>p>span",' +
                '"div.article-body>div>div>p",' +
                '"div.article-body>div>div>p>span>span"' +
                '            ]' +
                '        };';

            function gotoMainPage() {
                $location.url("/");
            }

            $scope.Action = {};
            $scope.SearchConfiguration = {};
            $scope.selected = {};


            $scope.ConfigurationGrid = {
                columnDefs: getConfigurationGridHeaders(),
                data: 'ConfigurationGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.ConfigurationGrid.onRegisterApi = function (gridApi) {
                $scope.ConfigurationGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.ConfigurationGrid.isSelected = row.isSelected;
                    $scope.ConfigurationGrid.gridApi.grid.selectedRowId = row.entity.id;
                })
            };

            (function () {
                var count = 0;
                var i;
                for (i = 0; i < $scope.ConfigurationGrid.columnDefs.length; i++) {
                    if ($scope.ConfigurationGrid.columnDefs[i].visible != false) {
                        count += 1;
                    }
                }
                var array = makeCollectiveArray(88, count - 1);
                var visibleIndex = 0;

                for (i = 0; i < $scope.ConfigurationGrid.columnDefs.length - 1; i++) {
                    var columnDef = $scope.ConfigurationGrid.columnDefs[i];
                    if (columnDef.visible != false) {
                        columnDef.width = array[visibleIndex++] + "%";
                    }
                }
                $scope.ConfigurationGrid.columnDefs[$scope.ConfigurationGrid.columnDefs.length - 1].width = "12%";
            })();

            $scope.$watch('ConfigurationGrid.content', function () {
                $scope.ConfigurationGrid.isSelected = false;
            });

            $scope.ConfigurationGrid.content = [];


            function getConfigurationGridHeaders() {
                return [
                    {
                        field: 'name',
                        displayName: $scope.translate('view.ir.phsys.sitereader.Configuration.name.label')
                    },
                    {
                        field: 'id',
                        displayName: $scope.translate('view.ir.phsys.sitereader.Configuration.id.label')
                        , visible: false
                    },
                    {
                        name: "operations",
                        displayName: '',
                        cellTemplate: '<div ng-show="row.entity.id==grid.selectedRowId"> <button class="btn btn-primary btn-small glyphicon glyphicon-pencil" tooltip="{{translate(\'app.button.edit\')}}"ng-click="$emit(\'edit\',row.entity)"></button><button class="btn btn-primary btn-small glyphicon glyphicon-th-list" tooltip="{{translate(\'app.button.show\')}}"ng-click="$emit(\'show\',row.entity)"></button><button class="btn btn-danger btn-small glyphicon glyphicon-minus" ng-click="$emit(\'delete\',row.entity)"tooltip="{{translate(\'app.button.delete\')}}"></button></div>'
                    }
                ];
            }

            $scope.$on('show', function (event, args) {
                ConfigurationCrudService.show(args);
            });
            $scope.Action.insert = function () {
                var model = {};
                ConfigurationCrudService.create(model).then(
                    function (result) {
                        ConfigurationRestService.create(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                            }
                        );
                    });
            };

            $scope.Action.reset = function () {
                $scope.SearchConfiguration = {};
            };

            $scope.Action.showPrint = function () {
                PrintService.print('/ir/phsys/sitereader/configurations/report/', getModel());
            };

            $scope.$on('edit', function (event, args) {
                ConfigurationCrudService.edit(args).then(
                    function (result) {
                        ConfigurationRestService.update(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                                ModelService.changeGridItem($scope.ConfigurationGrid.content, "id", result);
                            }
                        );
                    });
            });

            function getModel() {
                return $scope.SearchConfiguration;
            }

            $scope.Action.find = function () {

                ConfigurationRestService.searchConfiguration(getModel()).$promise.then(
                    function (data) {
                        $scope.ConfigurationGrid.content = data;
                        if (data.length <= 0) {
                            notify($scope.translate('app.not-found'), "warning");
                        }
                    }
                );
            };
            $scope.$on('delete', function (event, args) {
                question().then(function (success) {
                    ConfigurationRestService.delete({id: args.id}).$promise.then(
                        function (success) {
                            notify($scope.translate('app.success'), "success");
                            $scope.ConfigurationGrid.isSelected = false;
                            $timeout(function () {
                                $scope.$apply(function () {
                                    ModelService.removeGridItem($scope.ConfigurationGrid.content, args);
                                });
                            })
                        }
                    );
                });
            });
            $scope.Action.cancel = gotoMainPage;

            $scope.Action.fillSelected = function () {
                $scope.selected = $scope.ConfigurationGrid.gridApi.selection.getSelectedRows()[0];
            };


            $scope.Action.searchColumn = function (columnName, value) {
                return ConfigurationRestService.searchColumns({column: columnName, value: value}).$promise.then(
                    function (success) {
                        return success.map(function (item) {
                            var str = "";
                            for (var i in item) {
                                if (item.hasOwnProperty(i)) {
                                    str += item[i];
                                }
                            }
                            return str;
                        });
                    }
                );
            };

        }]);
})(angular.module('application'));