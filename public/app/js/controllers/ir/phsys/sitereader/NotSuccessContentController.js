/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */



(function (module) {

    module.controller('NotSuccessContentCtrl', ['$scope', '$timeout', '$http', 'ModelService', 'ManualRunRestService', '$location', 'PrintService'
        , 'ManualPopupService', '$interval',
        function ($scope, $timeout, $http, ModelService, ManualRunRestService, $location, PrintService
            , ManualPopupService, $interval) {

            function gotoMainPage() {
                $location.url("/");
            }

            $scope.Action = {};
            $scope.SearchManualRun = {};
            $scope.Model = {};
            $scope.selected = {};

            //function getGridConf(name) {
            //    return {
            //        columnDefs: getAgencyStatusGridHeaders(),
            //        data: name + '.content',
            //        multiSelect: false,
            //        isSelected: true,
            //        enableHighlighting: true,
            //        enableColumnReordering: true,
            //        enableColumnResize: true,
            //        enableRowSelection: true,
            //        enableRowHeaderSelection: false,
            //        rowHeight: 28
            //    }
            //}

            //$scope.AgencyStatusGrid1 = getGridConf('AgencyStatusGrid1');
            //
            //$scope.AgencyStatusGrid2 = getGridConf('AgencyStatusGrid2');
            //$scope.AgencyStatusGrid3 = getGridConf('AgencyStatusGrid3');

            //$scope.$watch('AgencyStatusGrid1.content', function () {
            //    $scope.AgencyStatusGrid1.isSelected = false;
            //});
            //
            //$scope.$watch('AgencyStatusGrid2.content', function () {
            //    $scope.AgencyStatusGrid2.isSelected = false;
            //});
            //
            //$scope.$watch('AgencyStatusGrid3.content', function () {
            //    $scope.AgencyStatusGrid3.isSelected = false;
            //});

            //$scope.AgencyStatusGrid1.content = [];
            //$scope.AgencyStatusGrid2.content = [];
            //$scope.AgencyStatusGrid3.content = [];
            //
            $scope.StatusGrid = {
                columnDefs: getStatusGridHeaders(),
                data: 'StatusGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enablePaginationControls: false,
                paginationPageSize: 10,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                rowTemplate: '<div  ><div class="ui-grid-cell" ng-class="{warningContent : row.entity.status==\'empty\',failureContent : row.entity.status==\'failed\'}" ng-repeat="col in colContainer.renderedColumns track by col.colDef.name" ui-grid-cell></div></div>'

            };
            $scope.StatusGrid.content = [];

            $scope.StatusGrid.onRegisterApi = function (gridApi) {
                $scope.StatusGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.StatusGrid.isSelected = row.isSelected;
                    $scope.StatusGrid.gridApi.grid.selectedRowId = row.entity.id;
                })
            };


            function getStatusGridHeaders() {
                return [
                    {
                        field: 'publisher',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.publisher'),
                        width: "10%"
                    },
                    {
                        field: 'title',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.title'),
                        width: "56%"

                    },
                    {
                        field: 'links',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.link'),
                        cellTemplate: '<a href="{{row.entity.links}}" target="_blank"><div class="glyphicon glyphicon-link"></div></a>',
                        width: "7%"

                    },
                    {
                        field: 'pubDate',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.pubDate'),
                        width: "22%",
                        cellFilter: 'date:\'yyyy-MM-dd HH:mm:ss\''
                    },
                    {
                        field: 'status',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false
                    },
                    {
                        field: 'id',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false
                    },
                    {
                        field: 'operation',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label'),
                        width: "5%",
                        cellTemplate: '<button ng-show="row.entity.id==grid.selectedRowId" class="btn btn-success btn-small glyphicon glyphicon-ok" ng-click="$emit(\'makeFixed\',row.entity)"></button>'
                    }

                ];
            }

            $scope.$on('makeFixed', function (event, args) {
                ManualRunRestService.makeStatusFixed({id: args.id}).$promise.then(function (success) {
                    for (var i in $scope.StatusGrid.content) {
                        if(i.indexOf("$")!=0 && $scope.StatusGrid.content[i].id==args.id) {
                            $timeout(function () {
                                delete $scope.StatusGrid.content[i];
                                $scope.Action.refresh();
                            });

                        }
                    }
                });
            });

            function getAgencyStatusGridHeaders() {
                return [
                    {
                        field: 'name',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.tag.label'),
                        width: '65%'
                    },
                    {
                        field: 'tag',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false,
                        width: '0%'
                    },
                    {
                        name: "successCount",
                        displayName: 'S',
                        width: '10%',
                        cellTemplate: '<div class="{{(row.entity.successCount!=0) ? \'btn indicator-btn btn-success\' : \'\' }}" ng-show="row.entity.successCount>0" ng-click="$emit(\'showStatus\',row.entity)">{{row.entity.successCount}}</div>'
                    },
                    {
                        name: "failureCount",
                        displayName: 'F',
                        width: '10%',
                        cellTemplate: '<div class="{{(row.entity.failureCount!=0) ? \'btn indicator-btn btn-danger\' : \'\' }}" ng-show="row.entity.failureCount>0" ng-click="$emit(\'showStatus\',row.entity)">{{row.entity.failureCount}}</div>'
                    },
                    {
                        name: "emptyCount",
                        displayName: 'E',
                        width: '10%',
                        cellTemplate: '<div class="{{(row.entity.emptyCount!=0) ? \'btn indicator-btn btn-warning\':\'\'}}" ng-show="row.entity.emptyCount>0" ng-click="$emit(\'showStatus\',row.entity)">{{row.entity.emptyCount}}</div>'
                    }
                ];
            }

            $scope.Action.run = function () {
                ManualRunRestService.runReading($scope.Model).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'), "success");
                        $scope.Action.refresh();
                    },
                    function (failure) {
                        console.log(failure);
                    }
                )
            };


            $scope.$watch("Model.tag", function (newValue, oldValue) {
                if (newValue != oldValue) {
                    ManualRunRestService.getNotSuccessStatus({tag: $scope.Model.tag}).$promise.then(function (success) {
                        $scope.StatusGrid.content = success;
                    });

                }
            });

            //$scope.$on('showStatus',function(event,args){
            //    var tag = args.tag;
            //    ManualRunRestService.getNotSuccessStatus(args).$promise.then(function (success) {
            //        ManualPopupService.showStatus(success);
            //    });
            //
            //
            //});

            $scope.Action.cancel = gotoMainPage;

            $scope.Action.refresh = function () {
                var tag = $scope.Model.tag;

                ManualRunRestService.getAllNonSuccessStatus().$promise.then(function (success) {
                    $scope.StatusGrid.content = success;
                });

            };

            //    var map = {};
            //    for (var i in $scope.gridContent) {
            //        if ($scope.gridContent.hasOwnProperty(i)) {
            //            if (i.indexOf("$") != 0) {
            //                map[$scope.gridContent[i].tag] = i;
            //                ManualRunRestService.getAgencyStatus($scope.gridContent[i]).$promise.then(function (success) {
            //                    var keys = Object.keys(success);
            //                    for (var j in keys) {
            //                        if (keys.hasOwnProperty(j)) {
            //                            $scope.gridContent[map[success.tag]][keys[j]] = success[keys[j]];
            //                        }
            //                    }
            //                })
            //            }
            //        }
            //    }
            //};

            //$scope.$watch('gridContent', function (newValue, oldValue) {
            //    if (newValue != oldValue) {
            //        $scope.AgencyStatusGrid1.content = newValue.slice(0, newValue.length / 3);
            //        $scope.AgencyStatusGrid2.content = newValue.slice(newValue.length / 3, (newValue.length / 3) * 2);
            //        $scope.AgencyStatusGrid3.content = newValue.slice((newValue.length / 3) * 2);
            //    }
            //});


            (function () {
                ManualRunRestService.getAgencies().$promise.then(
                    function (success) {
                        var convert = [];
                        //$scope.gridContent = success;
                        for (var i in success) {
                            convert.push({code: success[i].tag, description: success[i].name})
                        }
                        $scope.Agencies = convert;
                    });
            })();

            //$interval(function () {
            //    $scope.Action.refresh();
            //}, 60 * 1000);
        }]);


})(angular.module('application'));