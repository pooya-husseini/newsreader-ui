/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 4:05:03 PM
 * @version 1.0.0
 */

(function (module) {

    module.controller('SearchNewsCtrl', ['$scope', '$timeout', '$http', 'ModelService', 'SearchNewsRestService', '$location', 'PrintService'
        , 'SearchNewsCrudService', 'ManualRunRestService', 'projectCache', '$filter',
        function ($scope, $timeout, $http, ModelService, SearchNewsRestService, $location, PrintService
            , SearchNewsCrudService, ManualRunRestService, projectCache, $filter) {

            function gotoMainPage() {
                $location.url("/");
            }

            $scope.Action = {};
            $scope.SearchSearchNews = {};
            $scope.Paging = {
                numPages: 5,
                maxSize: 3,
                totalItems: 5
            };
            $scope.selected = {};

            $scope.SearchNewsGrid = {
                columnDefs: getSearchNewsGridHeaders(),
                data: 'SearchNewsGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.SearchNewsGrid.onRegisterApi = function (gridApi) {
                $scope.SearchNewsGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.SearchNewsGrid.isSelected = row.isSelected;
                    $scope.SearchNewsGrid.gridApi.grid.selectedRowId = row.entity.id;
                })
            };

            (function () {
                var count = 0;
                var i;
                for (i = 0; i < $scope.SearchNewsGrid.columnDefs.length; i++) {
                    if ($scope.SearchNewsGrid.columnDefs[i].visible != false) {
                        count += 1;
                    }
                }
                var array = makeCollectiveArray(88, count - 1);
                var visibleIndex = 0;

                for (i = 0; i < $scope.SearchNewsGrid.columnDefs.length - 1; i++) {
                    var columnDef = $scope.SearchNewsGrid.columnDefs[i];
                    if (columnDef.visible != false) {
                        columnDef.width = array[visibleIndex++] + "%";
                    }
                }
                $scope.SearchNewsGrid.columnDefs[$scope.SearchNewsGrid.columnDefs.length - 1].width = "12%";
            })();

            $scope.$watch('SearchNewsGrid.content', function () {
                $scope.SearchNewsGrid.isSelected = false;
            });

            $scope.SearchNewsGrid.content = [];

            function getSearchNewsGridHeaders() {
                return [
                    {
                        field: 'id',
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.id.label')
                        , visible: false
                    },
                    {
                        field: 'title',
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.title.label')
                    },
                    {
                        field: 'category',
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.category.label')
                    },
                    {
                        field: 'publisher',
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.publisher.label')
                    },
                    {
                        field: 'pubDate',
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.pubDate.label')
                        , cellFilter: 'date:\'yyyy-MM-dd HH:mm:ss\''
                    },
                    {
                        field: 'links',
                        displayName: 'links',
                        visible: false
                    },
                    {
                        name: "operations",
                        displayName: '',
                        cellTemplate: '<div ng-show="row.entity.id==grid.selectedRowId"> <a href="#" ng-show="row.entity.id==grid.selectedRowId" target="_blank" ng-href="{{row.entity.links}}">نمایش</a></div>'
                    }
                ];
            }

            $scope.Action.insert = function () {
                var model = {};
                SearchNewsCrudService.create(model).then(
                    function (result) {
                        SearchNewsRestService.create(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                            }
                        );
                    });
            };

            $scope.Action.reset = function () {
                $scope.SearchSearchNews = {};
            };

            $scope.Action.showPrint = function () {
                PrintService.print('/ir/phsys/sitereader/searchnews/report/', getModel());
            };

            $scope.$on('edit', function (event, args) {
                SearchNewsCrudService.edit(args).then(
                    function (result) {
                        SearchNewsRestService.update(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                                ModelService.changeGridItem($scope.SearchNewsGrid.content, "id", result);
                            }
                        );
                    });
            });

            function getModel() {
                if ($scope.SearchNewsGrid.currentPage == undefined) {
                    $scope.SearchSearchNews.start = 0
                } else {
                    $scope.SearchSearchNews.start = ($scope.SearchNewsGrid.currentPage - 1) * 12;
                }
                $scope.SearchSearchNews.rows = 12;
                return $scope.SearchSearchNews;
            }

            $scope.Action.find = function () {
                SearchNewsRestService.searchNews(getModel()).$promise.then(
                    function (data) {
                        $scope.SearchNewsGrid.content = data;
                        if (data.length <= 0) {
                            if ($scope.SearchNewsGrid.currentPage > 0) {
                                $scope.SearchNewsGrid.currentPage -= 1;
                                $scope.Paging.totalItems = $scope.SearchNewsGrid.currentPage;
                                $scope.Action.find();
                            } else {
                                notify($scope.translate('app.not-found'), "warning");
                            }
                        }
                    }
                );
            };
            $scope.$on('delete', function (event, args) {
                question().then(function (success) {
                    SearchNewsRestService.delete({id: args.id}).$promise.then(
                        function (success) {
                            notify($scope.translate('app.success'), "success");
                            $scope.SearchNewsGrid.isSelected = false;
                            $timeout(function () {
                                $scope.$apply(function () {
                                    ModelService.removeGridItem($scope.SearchNewsGrid.content, args);
                                });
                            })
                        }
                    );
                });
            });
            $scope.Action.cancel = gotoMainPage;

            $scope.Action.fillSelected = function () {
                $scope.selected = $scope.SearchNewsGrid.gridApi.selection.getSelectedRows()[0];
            };

            $scope.Action.searchColumn = function (columnName, value) {
                return SearchNewsRestService.searchColumns({column: columnName, value: value}).$promise.then(
                    function (success) {
                        return success.map(function (item) {
                            var str = "";
                            for (var i in item) {
                                if (item.hasOwnProperty(i)) {
                                    str += item[i];
                                }
                            }
                            return str;
                        });
                    }
                );
            };

            $scope.Action.init = function () {
                if ($scope.Agencies == undefined || $scope.Agencies.length <= 0) {
                    var tags = projectCache.get("tag.keys");
                    var convert = [];
                    for (var i in tags) {
                        convert.push({code: tags[i], description: $filter('PublisherNameFilter')(tags[i])});
                    }
                    $scope.Agencies = convert;
                }
                //ManualRunRestService.getAgencies().$promise.then(
                //    function (success) {
                //        var convert = [];
                //        for (var i in success) {
                //            convert.push({code: success[i].tag, description: success[i].name})
                //        }
                //        $scope.Agencies = convert;
                //        //$scope.Agencies = success;
                //    });
            };
        }
    ])
    ;
})
(angular.module('application'));