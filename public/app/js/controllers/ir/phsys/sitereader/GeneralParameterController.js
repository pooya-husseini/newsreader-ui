/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 3:03:28 PM
* @version 1.0.0
*/



(function(module){

module.controller('GeneralParameterCtrl',['$scope','$timeout','$http','ModelService','GeneralParameterRestService','$location','PrintService'
,'GeneralParameterCrudService',
function ($scope,$timeout,$http,ModelService,GeneralParameterRestService,$location,PrintService
,GeneralParameterCrudService) {

    function gotoMainPage(){
        $location.url("/");
    }

    $scope.Action = {};
    $scope.SearchGeneralParameter = {};
    $scope.selected = {};

    
    $scope.GeneralParameterGrid = {
        columnDefs:getGeneralParameterGridHeaders(),
        data: 'GeneralParameterGrid.content',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.GeneralParameterGrid.onRegisterApi = function (gridApi) {
        $scope.GeneralParameterGrid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.GeneralParameterGrid.isSelected = row.isSelected;
            $scope.GeneralParameterGrid.gridApi.grid.selectedRowId = row.entity.id;
        })
    };

    (function(){
        var count = 0;
        var i;
        for(i=0;i< $scope.GeneralParameterGrid.columnDefs.length;i++){
            if($scope.GeneralParameterGrid.columnDefs[i].visible!=false){
                count+=1;
            }
        }
        var array = makeCollectiveArray(88, count - 1);
        var visibleIndex = 0;

        for(i=0;i< $scope.GeneralParameterGrid.columnDefs.length-1;i++){
            var columnDef = $scope.GeneralParameterGrid.columnDefs[i];
            if(columnDef.visible!=false){
                columnDef.width = array[visibleIndex++] + "%";
            }
        }
        $scope.GeneralParameterGrid.columnDefs[$scope.GeneralParameterGrid.columnDefs.length - 1].width = "12%";
    })();

    $scope.$watch('GeneralParameterGrid.content', function () {
        $scope.GeneralParameterGrid.isSelected = false;
    });

    $scope.GeneralParameterGrid.content=[];

    
        function getGeneralParameterGridHeaders(){
            return  [
                                    {
                        field: 'id', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.id.label')
                        ,visible:false                                                                                            },
                                    {
                        field: 'mainType', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.mainType.label')
                                                                                                 ,cellFilter: 'TranslationFilter:"view.ir.phsys.sitereader.enums.GeneralParameterType"'                     },
                                    {
                        field: 'code', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.code.label')
                                                                                                                    },
                                    {
                        field: 'description', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.description.label')
                                                                                                                    },
                                                {
                    name: "operations",
                    displayName: '',
                    cellTemplate: '<div ng-show="row.entity.id==grid.selectedRowId"> <button class="btn btn-primary btn-small glyphicon glyphicon-pencil" tooltip="{{translate(\'app.button.edit\')}}"ng-click="$emit(\'edit\',row.entity)"></button><button class="btn btn-primary btn-small glyphicon glyphicon-th-list" tooltip="{{translate(\'app.button.show\')}}"ng-click="$emit(\'show\',row.entity)"></button><button class="btn btn-danger btn-small glyphicon glyphicon-minus" ng-click="$emit(\'delete\',row.entity)"tooltip="{{translate(\'app.button.delete\')}}"></button></div>'
                }
                ];
        }
    
                $scope.$on('show', function (event, args) {
                    GeneralParameterCrudService.show(args);
                });
    $scope.Action.insert = function () {
        var model={};
                GeneralParameterCrudService.create(model).then(
            function(result){
            GeneralParameterRestService.create(result).$promise.then(
                function (success) {
                    notify($scope.translate('app.success'),  "success");
                }
            );
        });
    };
                    
    $scope.Action.reset = function () {
        $scope.SearchGeneralParameter = {};
    };

    $scope.Action.showPrint = function () {
                PrintService.print('/ir/phsys/sitereader/generalparameters/report/',getModel());
    };

                $scope.$on('edit', function (event, args) {
        GeneralParameterCrudService.edit(args).then(
            function(result){
                GeneralParameterRestService.update(result).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'),"success");
                        ModelService.changeGridItem($scope.GeneralParameterGrid.content,"id",result);
                    }
                );
            });
                    });

    function getModel(){
                return $scope.SearchGeneralParameter;
    }
    $scope.Action.find=function(){

        GeneralParameterRestService.searchGeneralParameter(getModel()).$promise.then(
            function (data) {
                $scope.GeneralParameterGrid.content=data;
                if(data.length<=0){
                    notify($scope.translate('app.not-found'), "warning");
                }
            }

        );
    };
        $scope.$on('delete', function (event, args) {
            question().then(function(success) {
                GeneralParameterRestService.delete({id:args.id}).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'), "success");
                        $scope.GeneralParameterGrid.isSelected=false;
                        $timeout(function() {
                            $scope.$apply(function () {
                                ModelService.removeGridItem($scope.GeneralParameterGrid.content,args);
                            });
                        })
                    }
            );
            });
        });
    $scope.Action.cancel = gotoMainPage;

    $scope.Action.fillSelected = function(){
        $scope.selected = $scope.GeneralParameterGrid.gridApi.selection.getSelectedRows()[0];
    };



    $scope.Action.searchColumn = function (columnName, value) {
        return GeneralParameterRestService.searchColumns({column: columnName, value: value}).$promise.then(
            function (success) {
                return success.map(function (item) {
                    var str = "";
                    for(var i in item){
                        if(item.hasOwnProperty(i)){
                            str += item[i];
                        }
                    }
                    return str;
                });
            }
        );
    };


    (function() {
        GeneralParameterRestService.getGeneralParameterTypes().$promise.then(
         function(success){
    
            $scope.GeneralParameterType = ModelService.convertEnum(success,'ir.phsys.sitereader.enums.GeneralParameterType');

        });
})();
    }]);
})(angular.module('application'));