/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */



(function (module) {

    module.controller('ManualRunCtrl', ['$scope', '$timeout', '$http', 'ModelService', 'ManualRunRestService', '$location', 'PrintService'
        , 'ManualPopupService', '$interval', 'SearchNewsRestService',
        function ($scope, $timeout, $http, ModelService, ManualRunRestService, $location, PrintService
            , ManualPopupService, $interval, SearchNewsRestService) {

            function gotoMainPage() {
                $location.url("/");
            }

            $scope.Action = {};
            $scope.SearchManualRun = {};
            $scope.Model = {};
            $scope.selected = {};

            //function getGridConf(name) {
            //    return {
            //        columnDefs: getAgencyStatusGridHeaders(),
            //        data: name + '.content',
            //        multiSelect: false,
            //        isSelected: true,
            //        enableHighlighting: true,
            //        enableColumnReordering: true,
            //        enableColumnResize: true,
            //        enableRowSelection: true,
            //        enableRowHeaderSelection: false,
            //        rowHeight: 28
            //    }
            //}

            //$scope.AgencyStatusGrid1 = getGridConf('AgencyStatusGrid1');
            //
            //$scope.AgencyStatusGrid2 = getGridConf('AgencyStatusGrid2');
            //$scope.AgencyStatusGrid3 = getGridConf('AgencyStatusGrid3');

            //$scope.$watch('AgencyStatusGrid1.content', function () {
            //    $scope.AgencyStatusGrid1.isSelected = false;
            //});
            //
            //$scope.$watch('AgencyStatusGrid2.content', function () {
            //    $scope.AgencyStatusGrid2.isSelected = false;
            //});
            //
            //$scope.$watch('AgencyStatusGrid3.content', function () {
            //    $scope.AgencyStatusGrid3.isSelected = false;
            //});

            //$scope.AgencyStatusGrid1.content = [];
            //$scope.AgencyStatusGrid2.content = [];
            //$scope.AgencyStatusGrid3.content = [];
            //
            $scope.StatusGrid = {
                columnDefs: getStatusGridHeaders(),
                data: 'StatusGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enablePaginationControls: false,
                paginationPageSize: 10,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                rowTemplate: '<div  ><div class="ui-grid-cell" ng-class="{warningContent : row.entity.status==\'empty\',failureContent : row.entity.status==\'failed\'}" ng-repeat="col in colContainer.renderedColumns track by col.colDef.name" ui-grid-cell></div></div>'

            };
            $scope.StatusGrid.content = [];

            $scope.StatusGrid.onRegisterApi = function (gridApi) {
                $scope.StatusGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.StatusGrid.isSelected = row.isSelected;
                    $scope.StatusGrid.gridApi.grid.selectedRowId = row.entity.id;
                })
            };


            function getStatusGridHeaders() {
                return [
                    {
                        field: 'publisher',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.publisher'),
                        width: "10%"
                    },
                    {
                        field: 'title',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.title'),
                        width: "56%"

                    },
                    {
                        field: 'links',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.link'),
                        cellTemplate: '<a href="{{row.entity.links}}" target="_blank"><div class="glyphicon glyphicon-link"></div></a>',
                        width: "7%"

                    },
                    {
                        field: 'pubDate',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.pubDate'),
                        width: "22%",
                        cellFilter: 'date:\'yyyy-MM-dd HH:mm:ss\''
                    },
                    {
                        field: 'status',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false
                    },
                    {
                        field: 'id',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false
                    },
                    {
                        field: 'operation',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label'),
                        width: "5%",
                        cellTemplate: '<button ng-show="row.entity.id==grid.selectedRowId" class="btn btn-success btn-small glyphicon glyphicon-ok" ng-click="$emit(\'makeFixed\',row.entity)"></button>'
                    }

                ];
            }

            $scope.$on('makeFixed', function (event, args) {
                ManualRunRestService.makeStatusFixed({id: args.id}).$promise.then(function (success) {
                    for (var i in $scope.StatusGrid.content) {
                        if (i.indexOf("$") != 0 && $scope.StatusGrid.content[i].id == args.id) {
                            $timeout(function () {
                                delete $scope.StatusGrid.content[i];
                                $scope.Action.refresh();
                            });

                        }
                    }
                });
            });

            $scope.indexer = function () {
                return new Array(100);
            };

            function getAgencyStatusGridHeaders() {
                return [
                    {
                        field: 'name',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.tag.label'),
                        width: '65%'
                    },
                    {
                        field: 'tag',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false,
                        width: '0%'
                    },
                    {
                        name: "successCount",
                        displayName: 'S',
                        width: '10%',
                        cellTemplate: '<div class="{{(row.entity.successCount!=0) ? \'btn indicator-btn btn-success\' : \'\' }}" ng-show="row.entity.successCount>0" ng-click="$emit(\'showStatus\',row.entity)">{{row.entity.successCount}}</div>'
                    },
                    {
                        name: "failureCount",
                        displayName: 'F',
                        width: '10%',
                        cellTemplate: '<div class="{{(row.entity.failureCount!=0) ? \'btn indicator-btn btn-danger\' : \'\' }}" ng-show="row.entity.failureCount>0" ng-click="$emit(\'showStatus\',row.entity)">{{row.entity.failureCount}}</div>'
                    },
                    {
                        name: "emptyCount",
                        displayName: 'E',
                        width: '10%',
                        cellTemplate: '<div class="{{(row.entity.emptyCount!=0) ? \'btn indicator-btn btn-warning\':\'\'}}" ng-show="row.entity.emptyCount>0" ng-click="$emit(\'showStatus\',row.entity)">{{row.entity.emptyCount}}</div>'
                    }
                ];
            }

            $scope.Action.run = function () {
                $scope.disablity = true;
                ManualRunRestService.runReading($scope.Model).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'), "success");
                        $scope.Action.refresh();
                        $scope.disability = false;
                    },
                    function (failure) {
                        $scope.disability = false;
                        console.log(failure);
                    }
                )
            };


            $scope.$watch("Model.tag", function (newValue, oldValue) {
                if (newValue != oldValue) {
                    ManualRunRestService.getNotSuccessStatus({tag: $scope.Model.tag}).$promise.then(function (success) {
                        $scope.StatusGrid.content = success;
                    });

                }
            });

            //$scope.$on('showStatus',function(event,args){
            //    var tag = args.tag;
            //    ManualRunRestService.getNotSuccessStatus(args).$promise.then(function (success) {
            //        ManualPopupService.showStatus(success);
            //    });
            //
            //
            //});

            $scope.Action.cancel = gotoMainPage;

            $scope.Action.refresh = function () {
                ManualRunRestService.getAgencies().$promise.then(
                    function (success) {
                        var convert = [];

                        for (var i in success) {
                            if (i.indexOf("$") != 0) {
                                convert.push({disabled: false});


                                (function (index) {
                                    var tag = success[index].tag;
                                    SearchNewsRestService.searchNews({
                                        publisher: tag,
                                        start: 0,
                                        rows: 1
                                    }).$promise.then(function (lastContent) {
                                            var diff = 1000; // a sample big number
                                            var theClass = "";
                                            if (lastContent.hasOwnProperty("0")) {
                                                diff = new Date().getTime() - lastContent[0].pubDate;
                                                if (diff > 0) {
                                                    diff = diff / (1000 * 60 * 60 /* hours*/);

                                                }
                                                if (diff < 12) {
                                                    theClass = 'btn-success';
                                                } else if (diff <= 24) {
                                                    theClass = 'btn-warning';
                                                } else {
                                                    theClass = 'btn-danger';
                                                }
                                            } else {
                                                theClass = '';
                                            }

                                            convert[index] = {
                                                code: tag,
                                                description: success[index].name,
                                                diffHours: diff,
                                                id: index,
                                                theClass: theClass
                                            };
                                        });

                                })(i);
                            }
                        }
                        $scope.Agencies = convert;
                    });
            };

            //    var map = {};
            //    for (var i in $scope.gridContent) {
            //        if ($scope.gridContent.hasOwnProperty(i)) {
            //            if (i.indexOf("$") != 0) {
            //                map[$scope.gridContent[i].tag] = i;
            //                ManualRunRestService.getAgencyStatus($scope.gridContent[i]).$promise.then(function (success) {
            //                    var keys = Object.keys(success);
            //                    for (var j in keys) {
            //                        if (keys.hasOwnProperty(j)) {
            //                            $scope.gridContent[map[success.tag]][keys[j]] = success[keys[j]];
            //                        }
            //                    }
            //                })
            //            }
            //        }
            //    }
            //};

            //$scope.$watch('gridContent', function (newValue, oldValue) {
            //    if (newValue != oldValue) {
            //        $scope.AgencyStatusGrid1.content = newValue.slice(0, newValue.length / 3);
            //        $scope.AgencyStatusGrid2.content = newValue.slice(newValue.length / 3, (newValue.length / 3) * 2);
            //        $scope.AgencyStatusGrid3.content = newValue.slice((newValue.length / 3) * 2);
            //    }
            //});

            $scope.runIndexing = function (item) {
                $scope.disability = true;
                ManualRunRestService.runReading({tag: item}).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'), "success");
                        $scope.Action.refresh();
                        $scope.disability = false;
                    },
                    function (failure) {
                        notify($scope.translate('app.failure'), "failure");
                        $scope.disability = true;
                    }
                )
            };


            (function () {
                $scope.Action.refresh();
            })();

            $interval(function () {
                $scope.Action.refresh();
            }, 60 * 5000);
        }]);


})(angular.module('application'));