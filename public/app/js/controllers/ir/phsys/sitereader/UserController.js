/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */



(function (module) {

    module.controller('UserCtrl', ['$scope', '$timeout', '$http', 'ModelService', 'UserRestService', '$location', 'PrintService'
        , 'UserCrudService',
        function ($scope, $timeout, $http, ModelService, UserRestService, $location, PrintService
            , UserCrudService) {

            function gotoMainPage() {
                $location.url("/");
            }

            $scope.Action = {};
            $scope.SearchUser = {};
            $scope.selected = {};

            $scope.UserGrid = {
                columnDefs: getUserGridHeaders(),
                data: 'UserGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.UserGrid.onRegisterApi = function (gridApi) {
                $scope.UserGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.UserGrid.isSelected = row.isSelected;
                    $scope.UserGrid.gridApi.grid.selectedRowId = row.entity.id;
                })
            };

            (function () {
                var count = 0;
                var i;
                for (i = 0; i < $scope.UserGrid.columnDefs.length; i++) {
                    if ($scope.UserGrid.columnDefs[i].visible != false) {
                        count += 1;
                    }
                }
                var array = makeCollectiveArray(88, count - 1);
                var visibleIndex = 0;

                for (i = 0; i < $scope.UserGrid.columnDefs.length - 1; i++) {
                    var columnDef = $scope.UserGrid.columnDefs[i];
                    if (columnDef.visible != false) {
                        columnDef.width = array[visibleIndex++] + "%";
                    }
                }
                $scope.UserGrid.columnDefs[$scope.UserGrid.columnDefs.length - 1].width = "12%";
            })();

            $scope.$watch('UserGrid.content', function () {
                $scope.UserGrid.isSelected = false;
            });

            $scope.UserGrid.content = [];


            function getUserGridHeaders() {
                return [
                    {
                        field: 'id',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.id.label')
                        , visible: false
                    },
                    {
                        field: 'firstName',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.firstName.label')
                    },
                    {
                        field: 'lastName',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.lastName.label')
                    },
                    {
                        field: 'password',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.password.label')
                        , visible: false
                    },
                    {
                        field: 'username',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.username.label')
                    },
                    {
                        field: 'role',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.role.label')
                        , cellFilter: 'TranslationFilter:"view.ir.phsys.sitereader.UserManagementRole"'
                    },
                    {
                        field: 'campus',
                        displayName: $scope.translate('view.ir.phsys.sitereader.User.campus.label')
                        , cellFilter: 'GeneralParameterFilter:"CAMPUS"'
                    },
                    {
                        name: "operations",
                        displayName: '',
                        cellTemplate: '<div ng-show="row.entity.id==grid.selectedRowId"> <button class="btn btn-primary btn-small glyphicon glyphicon-pencil" tooltip="{{translate(\'app.button.edit\')}}"ng-click="$emit(\'edit\',row.entity)"></button><button class="btn btn-primary btn-small glyphicon glyphicon-th-list" tooltip="{{translate(\'app.button.show\')}}"ng-click="$emit(\'show\',row.entity)"></button><button class="btn btn-danger btn-small glyphicon glyphicon-minus" ng-click="$emit(\'delete\',row.entity)"tooltip="{{translate(\'app.button.delete\')}}"><button class="btn btn-primary btn-small glyphicon glyphicon-edit" tooltip="{{translate(\'app.changePassword\')}}" ng-click="$emit(\'changePassword\',row.entity)"></button></button></div>'
                    }
                ];
            }

            $scope.$on('show', function (event, args) {
                UserCrudService.show(args);
            });
            $scope.$on('changePassword', function (event, args) {
                    UserCrudService.changePassword(args).then(
                        function (result) {
                            UserRestService.changePassword(result).$promise.then(
                                function (success) {
                                    notify($scope.translate('app.success'), "success");
                                }
                            );
                        });
                }
            );

            $scope.Action.insert = function () {
                var model = {};
                UserCrudService.create(model).then(
                    function (result) {
                        UserRestService.create(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                            }
                        );
                    });
            };

            $scope.Action.reset = function () {
                $scope.SearchUser = {};
            };

            $scope.Action.showPrint = function () {
                PrintService.print('/ir/phsys/sitereader/users/report/', getModel());
            };

            $scope.$on('edit', function (event, args) {
                UserCrudService.edit(args).then(
                    function (result) {
                        UserRestService.update(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                                ModelService.changeGridItem($scope.UserGrid.content, "id", result);
                            }
                        );
                    });
            });

            function getModel() {
                return $scope.SearchUser;
            }

            $scope.Action.find = function () {

                UserRestService.searchUser(getModel()).$promise.then(
                    function (data) {
                        $scope.UserGrid.content = data;
                        if (data.length <= 0) {
                            notify($scope.translate('app.not-found'), "warning");
                        }
                    }
                );
            };
            $scope.$on('delete', function (event, args) {
                question().then(function (success) {
                    UserRestService.delete({id: args.id}).$promise.then(
                        function (success) {
                            notify($scope.translate('app.success'), "success");
                            $scope.UserGrid.isSelected = false;
                            $timeout(function () {
                                $scope.$apply(function () {
                                    ModelService.removeGridItem($scope.UserGrid.content, args);
                                });
                            })
                        }
                    );
                });
            });
            $scope.Action.cancel = gotoMainPage;

            $scope.Action.fillSelected = function () {
                $scope.selected = $scope.UserGrid.gridApi.selection.getSelectedRows()[0];
            };


            $scope.Action.searchColumn = function (columnName, value) {
                return UserRestService.searchColumns({column: columnName, value: value}).$promise.then(
                    function (success) {
                        return success.map(function (item) {
                            var str = "";
                            for (var i in item) {
                                if (item.hasOwnProperty(i)) {
                                    str += item[i];
                                }
                            }
                            return str;
                        });
                    }
                );
            };


            (function () {
                UserRestService.getUserManagementRoles().$promise.then(
                    function (success) {

                        $scope.UserManagementRole = ModelService.convertEnum(success, 'ir.phsys.sitereader.UserManagementRole');

                    });
            })();
        }]);
})(angular.module('application'));