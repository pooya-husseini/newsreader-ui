/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */



(function (module) {

    module.controller('LogsCtrl', ['$scope', '$timeout', '$http', 'ModelService', 'LogsRestService', '$location', 'PrintService'
        , 'LogsCrudService',
        function ($scope, $timeout, $http, ModelService, LogsRestService, $location, PrintService
            , LogsCrudService) {




            //function setConnected(connected) {
            //    document.getElementById('connect').disabled = connected;
            //    document.getElementById('disconnect').disabled = !connected;
            //    document.getElementById('conversationDiv').style.visibility = connected ? 'visible' : 'hidden';
            //    document.getElementById('response').innerHTML = '';
            //}



            function gotoMainPage() {
                $location.url("/");
            }

            $scope.Action = {};
            $scope.SearchLogs = {};
            $scope.Model = {};
            $scope.selected = {};



            $scope.LogsGrid = {
                columnDefs: getLogsGridHeaders(),
                data: 'LogsGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.LogsGrid.onRegisterApi = function (gridApi) {
                $scope.LogsGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.LogsGrid.isSelected = row.isSelected;
                    $scope.LogsGrid.gridApi.grid.selectedRowId = row.entity.id;
                })
            };

            (function () {
                var count = 0;
                var i;
                for (i = 0; i < $scope.LogsGrid.columnDefs.length; i++) {
                    if ($scope.LogsGrid.columnDefs[i].visible != false) {
                        count += 1;
                    }
                }
                var array = makeCollectiveArray(88, count - 1);
                var visibleIndex = 0;

                for (i = 0; i < $scope.LogsGrid.columnDefs.length - 1; i++) {
                    var columnDef = $scope.LogsGrid.columnDefs[i];
                    if (columnDef.visible != false) {
                        columnDef.width = array[visibleIndex++] + "%";
                    }
                }
                $scope.LogsGrid.columnDefs[$scope.LogsGrid.columnDefs.length - 1].width = "12%";
            })();

            $scope.$watch('LogsGrid.content', function () {
                $scope.LogsGrid.isSelected = false;
            });

            $scope.LogsGrid.content = [];


            function getLogsGridHeaders() {
                return [
                    {
                        field: 'level',
                        displayName: $scope.translate('view.ir.phsys.sitereader.Logs.level.label')
                    },
                    {
                        field: 'id',
                        displayName: $scope.translate('view.ir.phsys.sitereader.Logs.id.label')
                        , visible: false
                    },
                    {
                        name: "operations",
                        displayName: '',
                        cellTemplate: '<div ng-show="row.entity.id==grid.selectedRowId"> <button class="btn btn-primary btn-small glyphicon glyphicon-pencil" tooltip="{{translate(\'app.button.edit\')}}"ng-click="$emit(\'edit\',row.entity)"></button><button class="btn btn-primary btn-small glyphicon glyphicon-th-list" tooltip="{{translate(\'app.button.show\')}}"ng-click="$emit(\'show\',row.entity)"></button><button class="btn btn-danger btn-small glyphicon glyphicon-minus" ng-click="$emit(\'delete\',row.entity)"tooltip="{{translate(\'app.button.delete\')}}"></button></div>'
                    }
                ];
            }

            $scope.$on('show', function (event, args) {
                LogsCrudService.show(args);
            });
            $scope.Action.insert = function () {
                var model = {};
                LogsCrudService.create(model).then(
                    function (result) {
                        LogsRestService.create(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                            }
                        );
                    });
            };

            $scope.Action.reset = function () {
                $scope.SearchLogs = {};
            };

            $scope.Action.showPrint = function () {
                PrintService.print('/ir/phsys/sitereader/logs/report/', getModel());
            };

            $scope.$on('edit', function (event, args) {
                LogsCrudService.edit(args).then(
                    function (result) {
                        LogsRestService.update(result).$promise.then(
                            function (success) {
                                notify($scope.translate('app.success'), "success");
                                ModelService.changeGridItem($scope.LogsGrid.content, "id", result);
                            }
                        );
                    });
            });

            function getModel() {
                return $scope.SearchLogs;
            }

            $scope.Action.find = function () {

                LogsRestService.searchLogs(getModel()).$promise.then(
                    function (data) {
                        $scope.LogsGrid.content = data;
                        if (data.length <= 0) {
                            notify($scope.translate('app.not-found'), "warning");
                        }
                    }
                );
            };
            $scope.$on('delete', function (event, args) {
                question().then(function (success) {
                    LogsRestService.delete({id: args.id}).$promise.then(
                        function (success) {
                            notify($scope.translate('app.success'), "success");
                            $scope.LogsGrid.isSelected = false;
                            $timeout(function () {
                                $scope.$apply(function () {
                                    ModelService.removeGridItem($scope.LogsGrid.content, args);
                                });
                            })
                        }
                    );
                });
            });
            $scope.Action.cancel = gotoMainPage;

            $scope.Action.fillSelected = function () {
                $scope.selected = $scope.LogsGrid.gridApi.selection.getSelectedRows()[0];
            };


            $scope.Action.searchColumn = function (columnName, value) {
                return LogsRestService.searchColumns({column: columnName, value: value}).$promise.then(
                    function (success) {
                        return success.map(function (item) {
                            var str = "";
                            for (var i in item) {
                                if (item.hasOwnProperty(i)) {
                                    str += item[i];
                                }
                            }
                            return str;
                        });
                    }
                );
            };

        }]);
})(angular.module('application'));

angular.module('application').directive('showTail', function () {
    return function (scope, elem, attr) {
        scope.$watch(function () {
                return elem[0].value;
            },
            function (e) {
                elem[0].scrollTop = elem[0].scrollHeight;
            });
    }

});
