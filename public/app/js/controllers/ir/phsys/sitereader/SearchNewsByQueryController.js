/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 4:05:03 PM
* @version 1.0.0
*/



(function(module){

module.controller('SearchNewsByQueryCtrl',['$scope','$timeout','$http','ModelService','SearchNewsByQueryRestService','$location','PrintService'
,'SearchNewsByQueryCrudService',
function ($scope,$timeout,$http,ModelService,SearchNewsByQueryRestService,$location,PrintService
,SearchNewsByQueryCrudService) {

    function gotoMainPage(){
        $location.url("/");
    }

    $scope.Action = {};
    $scope.SearchSearchNewsByQuery = {};
    $scope.selected = {};

    
    $scope.SearchNewsByQueryGrid = {
        columnDefs:getSearchNewsByQueryGridHeaders(),
        data: 'SearchNewsByQueryGrid.content',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.SearchNewsByQueryGrid.onRegisterApi = function (gridApi) {
        $scope.SearchNewsByQueryGrid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.SearchNewsByQueryGrid.isSelected = row.isSelected;
            $scope.SearchNewsByQueryGrid.gridApi.grid.selectedRowId = row.entity.id;
        })
    };

    (function(){
        var count = 0;
        var i;
        for(i=0;i< $scope.SearchNewsByQueryGrid.columnDefs.length;i++){
            if($scope.SearchNewsByQueryGrid.columnDefs[i].visible!=false){
                count+=1;
            }
        }
        var array = makeCollectiveArray(88, count - 1);
        var visibleIndex = 0;

        for(i=0;i< $scope.SearchNewsByQueryGrid.columnDefs.length-1;i++){
            var columnDef = $scope.SearchNewsByQueryGrid.columnDefs[i];
            if(columnDef.visible!=false){
                columnDef.width = array[visibleIndex++] + "%";
            }
        }
        $scope.SearchNewsByQueryGrid.columnDefs[$scope.SearchNewsByQueryGrid.columnDefs.length - 1].width = "12%";
    })();

    $scope.$watch('SearchNewsByQueryGrid.content', function () {
        $scope.SearchNewsByQueryGrid.isSelected = false;
    });

    $scope.SearchNewsByQueryGrid.content=[];

    
        function getSearchNewsByQueryGridHeaders(){
            return  [
                                    {
                        field: 'query', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNewsByQuery.query.label')
                                                                                                                    },
                                    {
                        field: 'id', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNewsByQuery.id.label')
                        ,visible:false                                                                                            },
                                    {
                        field: 'tag', 
                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNewsByQuery.tag.label')
                                                                         ,cellFilter: 'GeneralParameterFilter:"newsTag"'                                             },
                                                {
                    name: "operations",
                    displayName: '',
                    cellTemplate: '<div ng-show="row.entity.id==grid.selectedRowId"> <button class="btn btn-primary btn-small glyphicon glyphicon-pencil" tooltip="{{translate(\'app.button.edit\')}}"ng-click="$emit(\'edit\',row.entity)"></button><button class="btn btn-primary btn-small glyphicon glyphicon-th-list" tooltip="{{translate(\'app.button.show\')}}"ng-click="$emit(\'show\',row.entity)"></button><button class="btn btn-danger btn-small glyphicon glyphicon-minus" ng-click="$emit(\'delete\',row.entity)"tooltip="{{translate(\'app.button.delete\')}}"></button></div>'
                }
                ];
        }
    
                $scope.$on('show', function (event, args) {
                    SearchNewsByQueryCrudService.show(args);
                });
    $scope.Action.insert = function () {
        var model={};
                SearchNewsByQueryCrudService.create(model).then(
            function(result){
            SearchNewsByQueryRestService.create(result).$promise.then(
                function (success) {
                    notify($scope.translate('app.success'),  "success");
                }
            );
        });
    };
                    
    $scope.Action.reset = function () {
        $scope.SearchSearchNewsByQuery = {};
    };

    $scope.Action.showPrint = function () {
                PrintService.print('/ir/phsys/sitereader/SearchNewsByqueries/report/',getModel());
    };

                $scope.$on('edit', function (event, args) {
        SearchNewsByQueryCrudService.edit(args).then(
            function(result){
                SearchNewsByQueryRestService.update(result).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'),"success");
                        ModelService.changeGridItem($scope.SearchNewsByQueryGrid.content,"id",result);
                    }
                );
            });
                    });

    function getModel(){
                return $scope.SearchSearchNewsByQuery;
    }
    $scope.Action.find=function(){

        SearchNewsByQueryRestService.searchSearchNewsByQuery(getModel()).$promise.then(
            function (data) {
                $scope.SearchNewsByQueryGrid.content=data;
                if(data.length<=0){
                    notify($scope.translate('app.not-found'), "warning");
                }
            }

        );
    };
        $scope.$on('delete', function (event, args) {
            question().then(function(success) {
                SearchNewsByQueryRestService.delete({id:args.id}).$promise.then(
                    function (success) {
                        notify($scope.translate('app.success'), "success");
                        $scope.SearchNewsByQueryGrid.isSelected=false;
                        $timeout(function() {
                            $scope.$apply(function () {
                                ModelService.removeGridItem($scope.SearchNewsByQueryGrid.content,args);
                            });
                        })
                    }
            );
            });
        });
    $scope.Action.cancel = gotoMainPage;

    $scope.Action.fillSelected = function(){
        $scope.selected = $scope.SearchNewsByQueryGrid.gridApi.selection.getSelectedRows()[0];
    };



    $scope.Action.searchColumn = function (columnName, value) {
        return SearchNewsByQueryRestService.searchColumns({column: columnName, value: value}).$promise.then(
            function (success) {
                return success.map(function (item) {
                    var str = "";
                    for(var i in item){
                        if(item.hasOwnProperty(i)){
                            str += item[i];
                        }
                    }
                    return str;
                });
            }
        );
    };

    }]);
})(angular.module('application'));