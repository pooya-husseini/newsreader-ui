Chart.defaults.global.bezierCurveTension = 0;
(function (module) {

    module.controller('ChartsController', ['$scope', '$timeout', '$http', 'ModelService', '$location', 'PrintService', 'projectCache',
        'ManualRunRestService', '$filter',
        function ($scope, $timeout, $http, ModelService, $location, PrintService, projectCache, ManualRunRestService, $filter) {

            $scope.handle = function ($event) {
                console.log($event);
            };
            $scope.Action = {};
            $scope.chartParams = {
                chartLabels: [],
                chartTags: [],
                chartData: [],
                colours: [
                    '#F7464A', // red
                    '#FDB45C', // yellow
                    '#46BFBD', // green
                    '#97BBCD', // blue
                    '#DCDCDC', // light grey
                    '#949FB1', // grey
                    '#4D5360'  // dark grey
                ],
                series: ['Failure', "Empty"]
            };

            $scope.Action.refresh = function () {
                if($scope.chartParams.chartLabels.length==0){
                    $scope.Action.loadAgencies();
                }
                for (var i in $scope.chartParams.chartLabels) {
                    if (i.indexOf("$") != 0) {
                        (function (i) {

                            ManualRunRestService.getAgencyStatus({tag: $scope.chartParams.chartTags[i]}).$promise.then(function (success) {
                                $scope.chartParams.chartData[0][parseInt(i)] = success["failureCount"];
                                //$scope.chartParams.chartData[1][parseInt(i)] = success["successCount"];
                                $scope.chartParams.chartData[1][parseInt(i)] = success["emptyCount"];
                            })
                        })(i);
                    }
                }
            };

            $scope.Action.loadAgencies = function () {
                var tags = projectCache.get('tag.keys');
                var empty = [];
                for (var i in tags) {
                    var name = $filter('PublisherNameFilter')(tags[i]);

                    $scope.chartParams.chartLabels.push(name);
                    $scope.chartParams.chartTags.push(tags[i]);
                    empty.push(0);
                }
                $scope.chartParams.chartData.push(empty.slice(0));
                $scope.chartParams.chartData.push(empty.slice(0));
                //$scope.refresh();
            };

            //(function () {
            //    var tags = projectCache.get('tag.keys');
            //    var empty = [];
            //    for(var i in tags){
            //        var name=$filter('PublisherNameFilter')(tags[i]);
            //
            //        $scope.chartParams.chartLabels.push(name);
            //        $scope.chartParams.chartTags.push(tags[i]);
            //        empty.push(0);
            //    }
            //    $scope.chartParams.chartData.push(empty.slice(0));
            //    $scope.chartParams.chartData.push(empty.slice(0));
            //    $scope.refresh();
            //
            //    //ManualRunRestService.getAgencies().$promise.then(
            //    //    function (success) {
            //    //        //var convert = [];
            //    //        //$scope.chartLabels = [];
            //    //        //$scope.chartData = [];
            //    //
            //    //        var empty = [];
            //    //        for (var i in success) {
            //    //            //= ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"];
            //    //            if (i.indexOf("$") != 0) {
            //    //                $scope.chartParams.chartLabels.push(success[i].name);
            //    //                $scope.chartParams.chartTags.push(success[i].tag);
            //    //                empty.push(0);
            //    //            }
            //    //        }
            //    //        $scope.chartParams.chartData.push(empty.slice(0));
            //    //        $scope.chartParams.chartData.push(empty.slice(0));
            //    //        //$scope.chartParams.chartData.push(empty.slice(0));
            //    //        //$scope.chartData.push(empty.slice(0));
            //    //        //$scope.chartData.push(empty.slice(0));
            //    //        //$scope.Agencies = convert;
            //    //
            //    //    });
            //})();

        }])
})(angular.module('application'));