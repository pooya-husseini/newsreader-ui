/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */
(function (module) {
    module.controller("MenuCtrl", ["$scope", "$rootScope", "$timeout", '$location', function ($scope, $rootScope, $timeout, $location) {

        $rootScope.menus = [
            {
                title: "ir.phsys.sitereader", details: [
                {domain: "ir.phsys.sitereader", title: "SearchNewsByQuery"},
                {domain: "ir.phsys.sitereader", title: "Charts"},
                {domain: "ir.phsys.sitereader", title: "Logs"},
                {domain: "ir.phsys.sitereader", title: "NotSuccessContents"},
                {domain: "ir.phsys.sitereader", title: "User"},
                {domain: "ir.phsys.sitereader", title: "ManualRun"},
                {domain: "ir.phsys.sitereader", title: "GeneralParameter"},
                {domain: "ir.phsys.sitereader", title: "Configuration"},
                {domain: "ir.phsys.sitereader", title: "SearchNews"},
            ]
            },
        ];


        $scope.searchedMenu = JSON.parse(JSON.stringify($scope.menus));

        $scope.Select = function (r, x) {
            $timeout(function () {
                $location.url(r.replace(/\./g, "/") + "/" + x);
                $rootScope.$apply(function () {
                    $rootScope.SelectedPage = r + "." + x;
                    $scope.selectedIndex = -1;
                })
            })
        };

        $scope.selectedIndex = -1;
        $scope.searchedItemsCount = 0;

        function getSearchedMenuSubItem(index) {
            var accuIndex = -1;
            for (var i = 0; i < $scope.searchedMenu.length; i++) {
                for (var j = 0; j < $scope.searchedMenu[i].details.length; j++) {
                    accuIndex++;
                    if (accuIndex == index) {
                        return $scope.searchedMenu[i].details[j];
                    }
                }
            }
        }

        $scope.selectMenuItem = function ($event) {
            switch ($event.keyCode) {
                case 40:
                    $scope.selectedIndex = ($scope.selectedIndex + 1) % $scope.searchedItemsCount;
                    break;
                case 38:
                    $scope.selectedIndex = ($scope.searchedItemsCount + $scope.selectedIndex - 1) % $scope.searchedItemsCount;
                    break;
                case 13:

                    if ($scope.selectedIndex >= 0) {
                        var subItem = getSearchedMenuSubItem($scope.selectedIndex);
                        $scope.Select(subItem.domain, subItem.title);
                    }
                    break;
            }
        };

        $scope.getSearchedMenu = function () {
            return $scope.searchedMenu;
        };

        $scope.$watch('itemToSearch', function (newValue, oldValue) {
            if (oldValue != newValue) {
                $scope.selectedIndex = -1;
                $scope.searchedMenu = JSON.parse(JSON.stringify($scope.menus));
                if (newValue == undefined) {
                    return $scope.menus;
                }
                var lowerCaseItem = newValue.toLowerCase();
                $scope.searchedItemsCount = 0;
                for (var k = 0; k < $scope.menus.length; k++) {
                    var details = JSON.parse(JSON.stringify($scope.menus[k].details.slice(0)));
                    $scope.searchedMenu[k].details.length = 0;
                    $scope.searchedMenu[k].expanded = true;
                    for (var i = 0; i < details.length; i++) {
                        var detail = details[i];
                        var translate = $rootScope.translate('menu.' + detail.domain + '.' + detail.title);
                        if (translate != undefined) {
                            if (translate.toLowerCase().indexOf(lowerCaseItem) >= 0) {
                                $scope.searchedMenu[k].details.push(details[i]);
                            }
                        }
                    }
                    if ($scope.searchedMenu[k].details.length < 0) {
                        $scope.searchedMenu.splice(k, 1);
                    } else {
                        $scope.searchedItemsCount += $scope.searchedMenu[k].details.length;
                    }
                }
            }
        });
    }]);
})(angular.module('services'));
