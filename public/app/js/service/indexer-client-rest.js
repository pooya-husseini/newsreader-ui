/**
 * Created by pooya on 2/9/15.
 */


(function (module) {
    module.service("MainRestService", ["$resource", function ($resource) {
        return $resource("/rest/:url:query", {}, {
             removeAll: {method: "DELETE"},
            query: {method: "GET", isArray: true, params: {query: "@query"}},
            agencies: {method: "GET", isArray: true, params: {url: "agencies"}},
            save: {method: "POST", params: {url: "save"}},
            startIndexing: {method: "POST", isArray: false, params: {url: "@tag"}},
            indexLength: {method: "GET", params: {url: "index/length"}},
            indexStatus: {method: "GET", params: {url: "index/status"}}
        });
    }]);
})(angular.module("application"));