/**
 * Created by sahami on 26/11/2014.
 */
(function (module) {
    /**
     Following Filter Translate items via receiving Translation Resource
     as sample users can consider 'view.com.caspco.auditmanagement.document.MissionReasonType' resource
     */
    module.filter('enumTranslate', ['$rootScope', function ($rootScope) {
            return function (item, resource) {
                var resourceKey = resource + item;
                return  $rootScope.translate(resourceKey);
            };
        }
        ]
    );

})(angular.module('application'));