(function (module) {

    module.filter('link', ['bu$configuration', function (bu$configuration) {
        return function (value) {
            if (!angular.isString(value)) {
                return value;
            }
            value = value || '';
            value = value.replace(/\./g, '/').replace(/\/html$/, '.html');
            if (!bu$configuration('html5mode')) {
                value = '#/' + value;
            } else if (!value) {
                value = '#';
            }
            value = value.replace(/\/{2,}/, "/");
            return value;
        }
    }]);

})(angular.module('application'));