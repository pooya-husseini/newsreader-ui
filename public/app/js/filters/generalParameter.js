/**
 * Created by keshvari on 10/20/14.
 */
(function (module) {
    module.filter('generalParameter', [function () {
        return function (item, parameterType, serviceName) {
            console.log(serviceName);
            var service = module.$injector.get(serviceName);
            return service.get(item, parameterType);
        };
    }]);

})(angular.module('application'));