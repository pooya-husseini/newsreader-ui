(function (module) {
    module.filter('persianDate', [function () {

            return function (item) {

                var monthNames = {
                    1:'فروردین',
                    2:'اردیبهشت',
                    3:'خرداد',
                    4:'تیر',
                    5:'مرداد',
                    6:'شهریور',
                    7:'مهر',
                    8:'آبان',
                    9:'آذر',
                    10:'دی',
                    11:'بهمن',
                    12:'اسفند'
                };

                if (angular.isUndefined(item)) {
                    return '';
                }

                var dateTime = item.split("T");
                var time = dateTime[1];
                var date = dateTime[0].split("-");
                var gYear = parseInt(date[0]);
                var gmonth = parseInt(date[1]);
                var gDay = parseInt(date[2]);
                var jalali = JalaliDate.gregorianToJalali(gYear, gmonth, gDay);

                //return normalizeGDate(jalali, "/") + " " + time;
                return  jalali[2]+' '+monthNames[jalali[1]] + ' '+jalali[0] + "    "+  time;
            };
        }
        ]
    );

})(angular.module('application'));