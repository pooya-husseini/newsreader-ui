/**
 * Created by keshvari on 12/6/14.
 */
(function (module) {
    module.filter('chooseFromJson', function () {
        return function (input,propertyName) {
            for (key in input){
                if(input.hasOwnProperty(propertyName)){
                    return input[propertyName];
                }
            }
        };
    });
})(angular.module('application'));