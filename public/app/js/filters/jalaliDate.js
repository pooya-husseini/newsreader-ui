/**
 * Created by keshvari on 8/19/14.
 */

(function (module) {
   module.filter('jalaliDate',['persianDateFilter','EnToFaNumberFilter',function(persianDateFilter,EnToFaNumberFilter){
        return function (item,format) {
            return EnToFaNumberFilter(persianDateFilter(item,format))
        };
    }
   ]
   );

})(angular.module('application'));