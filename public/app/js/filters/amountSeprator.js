/**
 * Created by keshvari on 10/4/14.
 */
(function (module) {
    module.filter('amount', function () {
        return function (number, precision) {
            var options = {};
            options.thousand = ',';
            options.decimal = '.';
            options.format = "%s%v";
            options.symbol = "";
            options.precision = precision;

            return accounting.formatMoney(number, options);
        };
    });
})(angular.module('application'));