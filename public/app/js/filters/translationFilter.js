/**
 * Created by sahami on 26/11/2014.
 */
(function (module) {
    /**
     Following Filter Translate items via receiving Translation Resource
     as sample users can consider 'view.com.caspco.auditmanagement.document.MissionReasonType' resource
     */
    module.filter('TranslationFilter', ['$rootScope', function ($rootScope) {
            return function (item, resource) {
                var resourceKey = resource;
                if (resource.endsWith(".")) {
                    resourceKey += item;
                } else {
                    resourceKey += "." + item;
                }
                var translate = $rootScope.translate(resourceKey);
                if(translate==undefined){
                    return item;
                }
                return translate;
            };
        }
        ]
    );

})(angular.module('application'));