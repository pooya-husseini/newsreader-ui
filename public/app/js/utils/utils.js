/**
 * Created by pooya on 10/13/15.
 */
(function (module) {
    'use strict';

    module.service('UtilService', ['$rootScope', '$q', '$http', '$timeout','propertyFileReader', function ($rootScope, $q, $http, $timeout,propertyFileReader) {
        this.loadPropertyFilePromise = function (address, keyFunction, valueFunction) {
            var defer = $q.defer();
            var promises = [];
            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                angular.forEach(properties, function (value, key) {
                    if (keyFunction) {
                        promises.push(keyFunction(key));
                    }
                    if (valueFunction) {
                        promises.push(valueFunction(value));
                    }
                });
                $q.all(promises).then(function () {
                    defer.resolve();
                }, function () {
                    defer.reject();
                })
            });
            return defer.promise;
        };

        this.loadPropertyFile = function (address, keyFunction, valueFunction) {
            var defer = $q.defer();
            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                angular.forEach(properties, function (value, key) {
                    if (keyFunction) {
                        keyFunction(key)
                    }
                    if (valueFunction) {
                        valueFunction(value)
                    }
                });
                defer.resolve();
            });
            return defer.promise;
        };

        this.loadPropertyFileAccumulative = function (address, keyFunction, valueFunction) {

            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                var responseKeys = [];
                var responseValues = [];
                angular.forEach(properties, function (value, key) {
                    responseKeys.push(key);
                    responseValues.push(value);
                });
                if (keyFunction) {
                    keyFunction(responseKeys)
                }
                if (valueFunction) {
                    valueFunction(responseValues)
                }
            });
        };

        this.loadPropertyFileAccumulativePromise = function (address, keyFunction, valueFunction) {
            var defer = $q.defer();

            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                var responseKeys = [];
                var responseValues = [];
                var promises = [];
                angular.forEach(properties, function (value, key) {
                    responseKeys.push(key);
                    responseValues.push(value);
                });

                if (keyFunction != undefined) {
                    promises.push(keyFunction(responseKeys));
                }
                if (valueFunction != undefined) {
                    promises.push(valueFunction(responseValues));
                }

                $q.all(promises).then(function () {
                    defer.resolve();
                }, function () {
                    defer.reject();
                })
            });

            return defer.promise;
        };
        this.loadAllPromises = function (promises) {
            var defer = $q.defer();
            $q.all(promises).then(function () {
                defer.resolve();
            }, function () {
                defer.reject();
            });
            return defer.promise;
        };

    }]);

})(angular.module('application'));