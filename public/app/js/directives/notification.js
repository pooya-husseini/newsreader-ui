(function (module) {
    module.directive('uiNotification', function () {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "/app/templates/notification.html",
            scope: {
                glyph: '@',
                color: "@",
                title: '@',
                caption: '@',
                count: '=?',
                actions: '=?'
            },
            controller: ['$scope', '$injector', function ($scope, $injector) {
                var controller = this;
                $scope.perform = function (item) {
                    if (item == null) {
                        return;
                    }
                    $injector.invoke(item.action, controller, {
                        $scope: $scope,
                        $item: item,
                        $items: $scope.actions.actions,
                        $count: {
                            get: function () {
                                return $scope.count;
                            },
                            set: function (count) {
                                $scope.count = count;
                            },
                            increase: function (by) {
                                if (!by) {
                                    by = 1;
                                }
                                $scope.count += by;
                            },
                            decrease: function (by) {
                                if (!by) {
                                    by = 1;
                                }
                                $scope.count -= by;
                            }
                        }
                    });
                };
            }]
        };
    });
})(angular.module('application'));