(function (module) {

    module.directive("uiAssetToggleButton", [function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            templateUrl: "/app/templates/assetToggleButton.html",
            scope: {
                target: "@"
            }
        };
    }]);

})(angular.module('application'));