/**
 * Created by sahami on 2014/09/07.
 */
(function (module) {
    module.directive("uiDatePicker", ['$timeout', '$q'  , function ($timeout, $q) {
        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/templates/datepicker.html',
            scope: {
                label: "@",
                placeholder: "@",
                id: "@",
                orientation: "@",
                labelSize: "@",
                model: '=',
                disabled: "@",
                dateformat: "@",
                notNull: "@"
            },
            link: function (scope, element, attrs) {
//                $timeout(function(){
//                    scope.$apply(function () {
//                        if(( scope.visible=='undefined' || scope.visible===undefined) ){
//                            scope.visible=true;
//                        }
//                    });});
                scope.$watch('pdate', function () {

                    if (scope.pdate) {
                        var date = scope.pdate.toString().replace(' ', '/').split('/');
                        var gregoriandate = JalaliDate.jalaliToGregorian(date[0], date[1], date[2]);
                        scope.model = normalizeGDate(gregoriandate, "-");
                        $timeout(function () {
                            scope.$apply();
                        });
                    }
                    else if (scope.model == undefined) {
                        scope.model = undefined;
                    }
                });

                scope.$watch('model', function () {

                    if (scope.model) {
                        var date = scope.model.toString().replace(/\//g, '-').split('-');
                        if (date[0] < '1500') {
                            scope.pdate = normalizeGDate(date);
                            $timeout(function () {
                                scope.$apply();
                            });
                            return;
                        }
                        var PersianDate = JalaliDate.gregorianToJalali(date[0], date[1], date[2]);
                        scope.pdate = normalizeGDate(PersianDate);
                        $timeout(function () {
                            scope.$apply();
                        });

                    }
                    else if (scope.model == undefined) {
                        scope.pdate = undefined;
                    }
                });

                $timeout(function () {
                    $timeout(function(){
                        scope.$apply(function () {
                            console.log(scope.disabled);
                            if( scope.disabled===undefined){
                                scope.disabled=false
                                ;
                            }

                            if (scope.disabled !== "true") {
                                Calendar.setup({
                                    inputField: attrs.id,   //id of the input field
                                    button: attrs.id + '_button',   //trigger for the calendar (button ID)
                                    ifFormat: scope.format,       //format of the input field
                                    showsTime: false,
                                    dateType: 'jalali',
                                    showOthers: true,
                                    langNumbers: true,
                                    weekNumbers: true,
                                    electric: true,
                                    date: null,
                                    onUpdate: function (item) {
                                        var date = Date.prototype.setUTCDateOnly(item.date);
                                        scope.pdate = Date.prototype.print('%Y/%m/%e', 'jalali', date);
                                        $timeout(function () {
                                            scope.$apply();
                                        });
                                    }
                                });
                            }

                        });
                    });



                });
            }
        }
    }]);
})(angular.module('application'));
