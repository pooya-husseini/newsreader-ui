/**
 * Created by keshvari on 7/27/14.
 */
/**
 * Created by keshvari on 7/24/14.
 */
(function (module) {

    module.directive("uiFileDownloader", ["$q", "$http","$timeout", function ($q, $http,$timeout) {

        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl:'/app/templates/fileDownloader.html' ,
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                notNull: "@",
                labelSize: "@",
                model: '=',
                descriptor: '&?',
                disabled: "=",
                downloadDisabled: "=",
                downloadAddress: "=",
                downloadLable: "@"
            },
            link: function(scope,element,attr){
                scope.$watch('disabled', function (newValue, oldValue) {

                });
            }
        }
    }]);
})(angular.module('application'));


