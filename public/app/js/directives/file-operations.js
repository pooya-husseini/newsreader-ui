/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive("uiInputText", ["$q", "$http", "$timeout", function ($q, $http, $timeout) {
        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/templates/form-operations.html',
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                notNull: "@",
                labelSize: "@",
                model: '=',
                descriptor: '&?',
                disabled: "=",
                typeAhead: "@",
                autofocus: "@"

            },
            link: function (scope, element, attr) {
                if (scope.typeAhead != undefined) {
                    scope.getItems = function (viewValue) {
                        return scope.$parent.Action.searchColumn(scope.typeAhead, viewValue);
                    };
                } else {
                    scope.getItems = function (viewValue) {
                        return "";
                    }
                }

                //
                //scope.$watch('disabled', function (newValue, oldValue) {
                //
                //});
            }
        };
    }]);
})(angular.module('application'));


