/**
 * Created by keshvari on 7/24/14.
 */
(function (module) {
    module.controller('AlertController', ['$scope', '$attrs', function ($scope, $attrs) {
        $scope.closeable = 'close' in $attrs;
    }]);

    module.directive('uiAlert', function () {
        return {
            restrict:'EA',
            controller:'AlertController',
            templateUrl: "/app/templates/alert.html",
            transclude:true,
            replace:true,
            scope: {
                type: '@',
                close: '&'
            }
        };
    });
})(angular.module('application'));