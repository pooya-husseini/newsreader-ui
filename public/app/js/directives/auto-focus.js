/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('autoFocus', function ($timeout) {
        return function (scope, element, attrs) {
            scope.$watch(attrs.autoFocus, function (val) {
                if (angular.isDefined(val) && val == 'true') {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        };
    })
})(angular.module("services"));