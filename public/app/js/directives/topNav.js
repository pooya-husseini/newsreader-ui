(function (module) {
    module.directive('uiTopNav', function () {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "/app/templates/topNav.html",
            transclude: true
        };
    });
})(angular.module('application'));