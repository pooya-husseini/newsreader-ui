(function (module) {
    module.directive('uiSecondaryNavigation', function () {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "/app/templates/secondaryNavigation.html"
        };
    });
})(angular.module('application'));