/**
 * Created by keshvari on 7/27/14.
 */
directive('wizard', function factory() {
    var directiveDefinitionObject = {
        transclude:true,
        replace:true,
        compile: function compile(tElement, tAttrs, transclude) {

            return function (scope, element, attrs) {
            }
        }
    };
    return directiveDefinitionObject;
});