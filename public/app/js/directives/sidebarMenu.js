(function (module) {

    module.directive("uiSidebarMenu", [function () {
        return {
            restrict: "E",
            transclude: true,
            replace: true,
            scope: {
                title: "@",
                expanded: '=?',
                glyph: "@"
            },
            templateUrl: "/app/templates/sidebarMenu.html"
        };
    }]);

})(angular.module('application'));