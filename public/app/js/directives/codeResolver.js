(function (module) {

    /**
     * Add querySelectorAll() to jqLite.
     *
     * jqLite find() is limited to lookups by tag name.
     * TODO This will change with future versions of AngularJS, to be removed when this happens
     *
     * See jqLite.find - why not use querySelectorAll? https://github.com/angular/angular.js/issues/3586
     * See feat(jqLite): use querySelectorAll instead of getElementsByTagName in jqLite.find https://github.com/angular/angular.js/pull/3598
     */
    if (angular.element.prototype.querySelectorAll === undefined) {
        angular.element.prototype.querySelectorAll = function (selector) {
            return angular.element(this[0].querySelectorAll(selector));
        };
    }


    // See Rename minErr and make it accessible from outside https://github.com/angular/angular.js/issues/6913
    module.service('codeResolverMinErr', function () {
        var minErr = angular.$$minErr('application');
        return function () {
            var error = minErr.apply(this, arguments);
            var message = error.message.replace(new RegExp('\nhttp://errors.angularjs.org/.*'), '');
            return new Error(message);
        };
    })
    module.provider('codeResolverConfig', function () {
        var coderResolverServiceName = "",
            codeResolverOperationName = "";

        this.setServiceSpecification = function (serviceName, operationName) {
            coderResolverServiceName = serviceName;
            codeResolverOperationName = operationName;
        };

        this.$get = function () {
            return {
                serviceName: coderResolverServiceName,
                operationName: codeResolverOperationName
            }
        }
    })
    /**
     * Parses "repeat" attribute.
     *
     * Taken from AngularJS ngRepeat source code
     * See https://github.com/angular/angular.js/blob/v1.2.15/src/ng/directive/ngRepeat.js#L211
     *
     * Original discussion about parsing "repeat" attribute instead of fully relying on ng-repeat:
     * https://github.com/angular-ui/ui-select/commit/5dd63ad#commitcomment-5504697
     */
    module.service('RepeatParser', [
        'codeResolverMinErr', function (codeResolverMinErr) {
            var self = this;

            /**
             * Example:
             * expression = "address in addresses | filter: {street: $select.search} track by $index"
             * lhs = "address",
             * rhs = "addresses | filter: {street: $select.search}",
             * trackByExp = "$index",
             * valueIdentifier = "address",
             * keyIdentifier = undefined
             */
            self.parse = function (expression) {
                if (!expression) {
                    throw codeResolverMinErr('repeat', "Expected 'repeat' expression.");
                }

                var match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);

                if (!match) {
                    throw codeResolverMinErr('iexp', "Expected expression in form of '_item_ in _collection_[ track by _id_]' but got '{0}'.",
                        expression);
                }

                var lhs = match[1]; // Left-hand side
                var rhs = match[2]; // Right-hand side
                var trackByExp = match[3];

                match = lhs.match(/^(?:([\$\w]+)|\(([\$\w]+)\s*,\s*([\$\w]+)\))$/);
                if (!match) {
                    throw codeResolverMinErr('iidexp', "'_item_' in '_item_ in _collection_' should be an identifier or '(_key_, _value_)' expression, but got '{0}'.",
                        lhs);
                }

                // Unused for now
                // var valueIdentifier = match[3] || match[1];
                // var keyIdentifier = match[2];

                return {
                    lhs: lhs,
                    rhs: rhs,
                    trackByExp: trackByExp
                };
            };

            self.getGroupNgRepeatExpression = function () {
                return '($group, $items) in $select.groups';
            };

            self.getNgRepeatExpression = function (lhs, rhs, trackByExp, grouped) {
                var expression = lhs + ' in ' + (grouped ? '$items' : rhs);
                if (trackByExp) {
                    expression += ' track by ' + trackByExp;
                }
                return expression;
            };
        }
    ])
    /**
     * Contains ui-select "intelligence".
     *
     * The goal is to limit dependency on the DOM whenever possible and
     * put as much logic in the controller (instead of the link functions) as possible so it can be easily tested.
     */
    module.controller('codeResolverCtrl',
        [
            '$scope', '$element', '$timeout', 'RepeatParser', 'codeResolverMinErr', '$filter','$rootScope',
            function ($scope, $element, $timeout, RepeatParser, codeResolverMinErr, $filter,$rootScope) {

                var ctrl = this;

                var EMPTY_SEARCH = '';

                ctrl.search = EMPTY_SEARCH;
                ctrl.activeIndex = 0;
                ctrl.items = [];
                ctrl.selected = undefined;
                ctrl.open = false;
                ctrl.focus = false;
                ctrl.focusser = undefined; //Reference to input element used to handle focus events
                ctrl.disabled = undefined; // Initialized inside uiSelect directive link function
                ctrl.resetSearchInput = undefined; // Initialized inside uiSelect directive link function
                ctrl.refreshDelay = undefined; // Initialized inside uiSelectChoices directive link function

                $scope.searchParameter = {};

                var _searchInput = $element.querySelectorAll('input.ui-select-search');
                if (_searchInput.length !== 1) {
                    throw codeResolverMinErr('searchInput', "Expected 1 input.ui-select-search but got '{0}'.", _searchInput.length);
                }

                // Most of the time the user does not want to empty the search input when in typeahead mode
                function _resetSearchInput() {
                    if (ctrl.resetSearchInput) {
                        ctrl.search = EMPTY_SEARCH;
                        //reset activeIndex
                        if (ctrl.selected && ctrl.items.length) {
                            ctrl.activeIndex = ctrl.items.indexOf(ctrl.selected);
                        }
                    }
                }

                // When the user clicks on ui-select, displays the dropdown list
                ctrl.activate = function (initSearchValue) {
                    if (!ctrl.open && !ctrl.disabled) {
                        _resetSearchInput();
                        ctrl.open = true;

                        // Give it time to appear before focus
                        $timeout(function () {
                            ctrl.search = initSearchValue || ctrl.search;
                            _searchInput[0].focus();
                        });
                    }
                    else {
                        ctrl.close();
                    }
                };

                ctrl.parseRepeatAttr = function (repeatAttr, groupByExp) {
                    function updateGroups(items) {
                        ctrl.groups = {};
                        angular.forEach(items, function (item) {
                            var groupFn = $scope.$eval(groupByExp);
                            var groupValue = angular.isFunction(groupFn) ? groupFn(item) : item[groupFn];
                            if (!ctrl.groups[groupValue]) {
                                ctrl.groups[groupValue] = [item];
                            } else {
                                ctrl.groups[groupValue].push(item);
                            }
                        });
                        ctrl.items = [];
                        angular.forEach(Object.keys(ctrl.groups).sort(), function (group) {
                            ctrl.items = ctrl.items.concat(ctrl.groups[group]);
                        });
                    }

                    function setPlainItems(items) {
                        ctrl.items = items;
                    }

                    var repeat = RepeatParser.parse(repeatAttr),
                        setItemsFn = groupByExp ? updateGroups : setPlainItems;

                    ctrl.isGrouped = !!groupByExp;
                    ctrl.itemProperty = repeat.lhs;

                    // See https://github.com/angular/angular.js/blob/v1.2.15/src/ng/directive/ngRepeat.js#L259
                    $scope.$watchCollection(repeat.rhs, function (items) {
                        if (items === undefined || items === null) {
                            // If the user specifies undefined or null => reset the collection
                            // Special case: items can be undefined if the user did not initialized the collection on the scope
                            // i.e $scope.addresses = [] is missing
                            ctrl.items = [];
                        } else {
                            if (!items) {
                                throw codeResolverMinErr('items', "Expected an array but got '{0}'.", items);
                            } else {
                                // Regular case
                                setItemsFn(items);
                            }
                        }

                    });
                };

                ctrl.createFilter = function (filters) {
                    var arrayOfFilters = filters.split(/;/);
                    var result = "";
                    arrayOfFilters.forEach(function (value, key) {
                        result += value.split(':')[0] + ' : $select.search';
                        if (arrayOfFilters.length > 1 && key !== (arrayOfFilters.length - 1))
                            result += ',';
                    });
                    return result;
                }

                ctrl.prepareItems = function (items) {
                    if (angular.isArray(items)) {
                        return items;
                    } else if (angular.isArray($scope.$parent[items])) {
                        return $scope.$parent[items];
                    } else if (!angular.isUndefined(module.$injector.get('GeneralParameterRestService'))) {
                        var promise = module.$injector.get('GeneralParameterRestService')['searchGeneralParameter']({mainType: $scope.typeParameter});
                        promise.$promise.then(function (data) {
                            if (!angular.isUndefined($scope.searchParameter[$scope.selectedName])) {
                                ctrl.selected = $filter('filter')(data, $scope.searchParameter)[0];
                            }
                        });
                        return promise;
                    }
                    return null;
                };

                ctrl.createMatchDirective = function (filters) {
                    var arrayOfFilters = filters.split(';');
                    var result = "";
                    arrayOfFilters.forEach(function (value, key) {
                        var fieldName = value.split(':')[0];
                        var fieldType = value.split(':')[1];
                        var prefix = "app.";
                        $scope.translationPrefix == undefined || (prefix = $scope.translationPrefix + ".");

                        result += "<div class='codeResolver" + fieldName + "' style=\"display: inline-block;\">" +
                        "<code-resolver-match placeholder=\"" + $rootScope.translate(prefix + fieldName) + "\" fieldType=\"" + fieldType + "\">{{$select.selected." + fieldName + "}}</code-resolver-match>" +
                        "</div>";
                    });
                    return result;
                };

                var _refreshDelayPromise;

                /**
                 * Typeahead mode: lets the user refresh the collection using his own function.
                 *
                 * See Expose $select.search for external / remote filtering https://github.com/angular-ui/ui-select/pull/31
                 */
                ctrl.refresh = function (refreshAttr) {
                    if (refreshAttr !== undefined) {

                        // Debounce
                        // See https://github.com/angular-ui/bootstrap/blob/0.10.0/src/typeahead/typeahead.js#L155
                        // FYI AngularStrap typeahead does not have debouncing: https://github.com/mgcrea/angular-strap/blob/v2.0.0-rc.4/src/typeahead/typeahead.js#L177
                        if (_refreshDelayPromise) {
                            $timeout.cancel(_refreshDelayPromise);
                        }
                        _refreshDelayPromise = $timeout(function () {
                            $scope.$eval(refreshAttr);
                        }, ctrl.refreshDelay);
                    }
                };

                ctrl.setActiveItem = function (item) {
                    ctrl.activeIndex = ctrl.items.indexOf(item);
                };

                ctrl.isActive = function (itemScope) {
                    return ctrl.items.indexOf(itemScope[ctrl.itemProperty]) === ctrl.activeIndex;
                };

                // When the user clicks on an item inside the dropdown
                ctrl.select = function (item) {
                    if (item == $scope) {
                        //user selected null item
                        ctrl.selected = undefined;
                    }
                    else {
                        ctrl.selected = item;
                    }
                    ctrl.close();
                    // Using a watch instead of $scope.ngModel.$setViewValue(item)
                };

                // Closes the dropdown
                ctrl.close = function () {
                    if (ctrl.open) {
                        _resetSearchInput();
                        ctrl.open = false;
                        ctrl.focusser[0].focus();
                    }
                };

                var Key = {
                    Enter: 13,
                    Tab: 9,
                    Up: 38,
                    Down: 40,
                    Escape: 27
                };

                function _onKeydown(key) {
                    var processed = true;
                    switch (key) {
                        case Key.Down:
                            if (ctrl.activeIndex < ctrl.items.length - 1) {
                                ctrl.activeIndex++;
                            }
                            break;
                        case Key.Up:
                            if (ctrl.activeIndex > 0) {
                                ctrl.activeIndex--;
                            }
                            break;
                        case Key.Tab:
                        case Key.Enter:
                            ctrl.select(ctrl.items[ctrl.activeIndex]);
                            break;
                        case Key.Escape:
                            ctrl.close();
                            break;
                        default:
                            processed = false;
                    }
                    return processed;
                }

                // Bind to keyboard shortcuts
                _searchInput.on('keydown', function (e) {
                    // Keyboard shortcuts are all about the items,
                    // does not make sense (and will crash) if ctrl.items is empty
                    if (ctrl.items && ctrl.items.length >= 0) {
                        var key = e.which;
                        $timeout(function () {
                            $scope.$apply(function () {
                                var processed = _onKeydown(key);
                                if (processed) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                            });
                        }, 0);

                        switch (key) {
                            case Key.Down:
                            case Key.Up:
                                _ensureHighlightVisible();
                                break;
                        }
                    }
                });

                // See https://github.com/ivaynberg/select2/blob/3.4.6/select2.js#L1431
                function _ensureHighlightVisible() {
                    var container = $element.querySelectorAll('.ui-select-choices-content');
                    var choices = container.querySelectorAll('.ui-select-choices-row');
                    if (choices.length < 1) {
                        throw codeResolverMinErr('choices', "Expected multiple .ui-select-choices-row but got '{0}'.", choices.length);
                    }

                    var highlighted = choices[ctrl.activeIndex];
                    var posY = highlighted.offsetTop + highlighted.clientHeight - container[0].scrollTop;
                    var height = container[0].offsetHeight;

                    if (posY > height) {
                        container[0].scrollTop += posY - height;
                    } else if (posY < highlighted.clientHeight) {
                        if (ctrl.isGrouped && ctrl.activeIndex === 0)
                            container[0].scrollTop = 0; //To make group header visible when going all the way up
                        else
                            container[0].scrollTop -= highlighted.clientHeight - posY;
                    }
                }

                $scope.$on('$destroy', function () {
                    _searchInput.off('keydown');
                });
            }
        ]);
    module.directive('codeResolver',
        [
            '$document', 'codeResolverMinErr', '$compile', 'RepeatParser', '$filter',
            function ($document, codeResolverMinErr, $compile, RepeatParser, $filter) {

                return {
                    restrict: 'E',
//                    template: "<div class=\"select2 select2-container\" ng-class=\"{'select2-container-active select2-dropdown-open': $select.open, 'select2-container-disabled': $select.disabled, 'select2-container-active': $select.focus}\"><div><div class=\"ui-select-match\"></div></div><div class=\"select2-drop select2-with-searchbox select2-drop-active\" ng-class=\"{'select2-display-none': !$select.open}\"><div class=\"select2-search\"><input type=\"text\" autocomplete=\"off\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\" class=\"ui-select-search select2-input\" ng-model=\"$select.search\"></div><ul class=\"ui-select-choices ui-select-choices-content select2-results\"><li class=\"ui-select-choices-group\" ng-class=\"{'select2-result-with-children': $select.isGrouped}\"><div ng-show=\"$select.isGrouped\" class=\"ui-select-choices-group-label select2-result-label\">{{$group}}</div><ul class=\"select2-result-sub\"><li class=\"ui-select-choices-row\" ng-class=\"{'select2-highlighted': $select.isActive(this)}\"><div class=\"select2-result-label ui-select-choices-row-inner\"></div></li></ul></li></ul></div></div>",
                    templateUrl: 'app/templates/codeResolver.html',
                    replace: true,
                    transclude: true,
                    require: ['codeResolver', 'ngModel'],
                    scope: {
                        sourceItems: '=',
                        typeParameter: '@',
                        disabled: '=?',
                        selectedName: '@',
                        filters: '@',
                        placeholder: "@",
                        translationPrefix: "@",
                        id: "@",
                        validation: "@",
                        state: "@",
                        feedback: "@",
                        orientation: "@",
                        labelSize: "@",
                        label: "@"
                    },
                    controller: 'codeResolverCtrl',
                    controllerAs: '$select',

                    link: function (scope, element, attrs, ctrls, transcludeFn) {
                        var $select = ctrls[0];
                        var ngModel = ctrls[1];
                        var model = 'source';

//                        if (angular.isUndefined(scope.datasource || scope.typeParameter) || angular.isUndefined(scope.selectedName)) {
//                            throw { name: "Bad parameters" };
//                        }

                        //Idea from: https://github.com/ivaynberg/select2/blob/79b5bf6db918d7560bdd959109b7bcfb47edaf43/select2.js#L1954
                        var focusser = angular.element("<input ng-disabled='$select.disabled' class='ui-select-focusser ui-select-offscreen' type='text' aria-haspopup='true' role='button' />");
                        $compile(focusser)(scope);
                        $select.focusser = focusser;

                        element.append(focusser);
                        focusser.bind("focus", function () {
                            scope.$evalAsync(function () {
                                $select.focus = true;
                            });
                        });
                        focusser.bind("blur", function () {
                            scope.$evalAsync(function () {
                                $select.focus = false;
                            });
                        });
                        focusser.bind("keydown", function (e) {
                            if (e.which === KEY.BACKSPACE) {
                                e.preventDefault();
                                e.stopPropagation();
                                $select.select(undefined);
                                scope.$digest();
                                return;
                            }

                            if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e) || e.which === KEY.ESC) {
                                return;
                            }

                            if (e.which == KEY.DOWN || e.which == KEY.UP || e.which == KEY.ENTER || e.which == KEY.SPACE) {
                                e.preventDefault();
                                e.stopPropagation();
                                $select.activate();
                            }

                            scope.$digest();
                        });

                        focusser.bind("keyup input", function (e) {

                            if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e) || e.which === KEY.ESC || e.which == KEY.ENTER || e.which === KEY.BACKSPACE) {
                                return;
                            }
                            $select.activate(focusser.val()); //User pressed some regualar key, so we pass it to the search input
                            focusser.val('');
                            scope.$digest();

                        });

                        //TODO Refactor to reuse the KEY object from uiSelectCtrl
                        var KEY = {
                            TAB: 9,
                            ENTER: 13,
                            ESC: 27,
                            SPACE: 32,
                            LEFT: 37,
                            UP: 38,
                            RIGHT: 39,
                            DOWN: 40,
                            SHIFT: 16,
                            CTRL: 17,
                            ALT: 18,
                            PAGE_UP: 33,
                            PAGE_DOWN: 34,
                            HOME: 36,
                            END: 35,
                            BACKSPACE: 8,
                            DELETE: 46,
                            isArrow: function (k) {
                                k = k.which ? k.which : k;
                                switch (k) {
                                    case KEY.LEFT:
                                    case KEY.RIGHT:
                                    case KEY.UP:
                                    case KEY.DOWN:
                                        return true;
                                }
                                return false;
                            },
                            isControl: function (e) {
                                var k = e.which;
                                switch (k) {
                                    case KEY.SHIFT:
                                    case KEY.CTRL:
                                    case KEY.ALT:
                                        return true;
                                }

                                if (e.metaKey) return true;

                                return false;
                            },
                            isFunctionKey: function (k) {
                                k = k.which ? k.which : k;
                                return k >= 112 && k <= 123;
                            }
                        };

                        scope.$watch(function () {
                            return scope.disabled;
                        }, function () {
                            // No need to use $eval() (thanks to ng-disabled) since we already get a boolean instead of a string
                            $select.disabled = scope.disabled !== undefined ? scope.disabled : false;
                        });

                        attrs.$observe('resetSearchInput', function () {
                            // $eval() is needed otherwise we get a string instead of a boolean
                            var resetSearchInput = scope.$eval(attrs.resetSearchInput);
                            $select.resetSearchInput = resetSearchInput !== undefined ? resetSearchInput : true;
                        });

                        scope.$watch('$select.selected', function (newValue) {
                            if (angular.isUndefined(newValue)) {
                                if (scope['source'].length != 0) {
                                    ngModel.$setViewValue(undefined);
                                }
                                return;
                            }
                            if (ngModel.$viewValue !== newValue[scope.selectedName]) {
                                ngModel.$setViewValue(newValue[scope.selectedName]);
                            }
                        });

                        ngModel.$render = function () {
                            if (angular.isUndefined(ngModel.$viewValue)) {
                                $select.selected = undefined;
                            }
                            else {
                                scope.searchParameter[scope.selectedName] = ngModel.$viewValue;
                                if ($select.items.length > 0) {
                                    $select.selected = $filter('filter')($select.items, scope.searchParameter)[0];
                                }
                                else {
                                    if (scope['source'].length > 0) {
                                        $select.selected = $filter('filter')(scope['source'], scope.searchParameter)[0];
                                    }
                                }
                            }
                        };

                        function onDocumentClick(e) {
                            var contains = false;

                            if (window.jQuery) {
                                // Firefox 3.6 does not support element.contains()
                                // See Node.contains https://developer.mozilla.org/en-US/docs/Web/API/Node.contains
                                contains = window.jQuery.contains(element[0], e.target);
                            } else {
                                contains = element[0].contains(e.target);
                            }

                            if (!contains) {
                                $select.close();
                                scope.$digest();
                            }
                        }

                        // See Click everywhere but here event http://stackoverflow.com/questions/12931369
                        $document.on('click', onDocumentClick);

                        scope.$on('$destroy', function () {
                            $document.off('click', onDocumentClick);
                        });

                        // Add filter to directive

                        scope[model] = $select.prepareItems(scope.sourceItems || scope.typeParameter);
                        scope.$watch(function () {
                            return scope.sourceItems;
                        }, function () {
                            if (!angular.isUndefined(scope.sourceItems)) {
                                scope[model] = $select.prepareItems(scope.sourceItems);
                            }
                        });

                        var filter = "item in " + model + " | propsFilter: {" + $select.createFilter(scope.filters) + "}";

                        var repeat = RepeatParser.parse(filter);
                        var choices = element.querySelectorAll('.ui-select-choices-row');
                        if (choices.length !== 1) {
                            throw codeResolverMinErr('rows', "Expected 1 .ui-select-choices-row but got '{0}'.", choices.length);
                        }

                        choices.attr('ng-repeat', RepeatParser.getNgRepeatExpression(repeat.lhs, '$select.items', repeat.trackByExp, undefined))
                            .attr('ng-mouseenter', '$select.setActiveItem(' + repeat.lhs + ')')
                            .attr('ng-click', '$select.select(' + repeat.lhs + ')');

                        $select.parseRepeatAttr(filter, undefined);

                        scope.$watch('$select.search', function () {
                            $select.activeIndex = 0;
                            $select.refresh(attrs.refresh);
                        });


                        // Move transcluded elements to their correct position in main template
                        transcludeFn(scope, function (clone) {
                            var matchDirective = element.querySelectorAll('.ui-select-match');
                            if (matchDirective.length !== 1)
                                throw codeResolverMinErr('rows', "Expected 1 .ui-select-match but got '{0}'.", matchDirective.length);
                            matchDirective.append($select.createMatchDirective(scope.filters));

                            var rowsInner = element.querySelectorAll('.ui-select-choices-row-inner');
                            if (rowsInner.length !== 1)
                                throw codeResolverMinErr('rows', "Expected 1 .ui-select-choices-row-inner but got '{0}'.", rowsInner.length);

                            rowsInner.append(clone);

                            //At here we compile element again, but match elements don't have any transclude item so
                            angular.forEach(element.querySelectorAll('[ng-transclude]'), function (key) {
                                angular.element(key).removeAttr('ng-transclude');
                            });

                            $compile(element)(scope);
                        });
                    }
                };
            }
        ])
    module.directive('codeResolverMatch',
        function () {
            return {
                restrict: 'E',
                require: '^codeResolver',
                replace: true,
                transclude: true,
                template: "<a class=\"select2-choice ui-select-match\" ng-class=\"{\'select2-default\': $select.selected === undefined}\" ng-click=\"$select.activate()\"><span ng-hide=\"$select.selected !== undefined\" class=\"select2-chosen\"><div class=\"placeholder\"/>}}</span> <span ng-show=\"$select.selected !== undefined\" class=\"select2-chosen\" ng-transclude></span> <span class=\"select2-arrow\"><b></b></span></a>",
                //templateUrl: function(tElement) {
                //    // Gets theme attribute from parent (ui-select)
                //  var theme = tElement.parent().attr('theme') || uiSelectConfig.theme;
                //  return '../Scripts/src/' + theme + '/match.tpl.html';
                //},
                compile: function (element, attrs) {
                    element.querySelectorAll(".placeholder").append("<span>" + attrs.placeholder + "</span>");
                    if (attrs.fieldtype === "text")
                        element.querySelectorAll(".select2-arrow").remove();
                }

            };
        }
    );
    /**
     * Highlights text that matches $select.search.
     *
     * Taken from AngularUI Bootstrap Typeahead
     * See https://github.com/angular-ui/bootstrap/blob/0.10.0/src/typeahead/typeahead.js#L340
     */
    module.filter('highlight', function () {
        function escapeRegexp(queryToEscape) {
            return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
        }

        return function (matchItem, query) {
            return query && matchItem ? matchItem.replace(new RegExp(escapeRegexp(query), 'gi'), '<span class="ui-select-highlight">$&</span>') : matchItem;
        };
    })

    module.filter('propsFilter', function () {
        return function (items, props) {
            var out = [];
            if (angular.isArray(items)) {
                items.forEach(function (item) {
                    var itemMatches = false;
                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        if (!angular.isUndefined(item[prop])) {
                            var text = props[prop].toLowerCase();
                            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        }
    });
}(angular.module('application')));
