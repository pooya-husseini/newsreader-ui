///**
// * Created by keshvari on 7/24/14.
// */
//(function (module) {
//
//    module.directive("uiInput", ["$q", "$http", function ($q, $http) {
//        return {
//            restrict: "E",
//            replace: true,
//            transclude: false,
//            template:"<div ng-include='getTemplateUrl()'></div>" ,
//            scope: {
//                type: "@",
//                label: "@",
//                placeholder: "@",
//                id: "@",
//                validation: "@",
//                state: "@",
//                feedback: "@",
//                value: "@",
//                orientation: "@",
//                labelSize: "@",
//                ngModel: '=?',
//                descriptor: '&?',
//                disabled: "@"
//            },
//            defaults: {
//                label: " ",
//                placeholder: "",
//                validation: "",
//                state: "normal",
//                feedback: ""
//            },
//            link: function ($scope, $element, $attrs) {
//
//                    $scope.getTemplateUrl = function(){
//                        if($scope.type == "text"){
//                            return 'app/templates/formInput.html';
//                        }else if($scope.type == "radio"){
//                            return 'app/templates/radio.html';
//                        }else if($scope.type == "textarea"){
//                            return 'app/templates/textarea.html'
//                        }else if($scope.type == "checkbox"){
//                            return 'app/templates/checkbox.html'
//                        }else if($scope.type == "checkList"){
//                            return 'app/templates/checkList.html'
//                        }else if($scope.type == "combo"){
//                            return 'app/templates/combo.html'
//                        }else if($scope.type == "static"){
//                            return 'app/templates/static.html'
//                        }
//
//                    }
//
//            }
//
//        }
//    }]);
//})(angular.module('application'));
//
//
