/**
 * Created by heman on 11/12/14.
 */
(function(module){
    "use strict";

    module.directive('ui.caspGrid',function factory(){
        return{
            restrict : 'E',
            templateUrl: '/app/templates/caspGrid.html',

        }
    })
}(angular.module('application')));

