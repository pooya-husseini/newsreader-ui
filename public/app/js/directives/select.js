(function (module) {
    module.directive('uiSelect', function factory() {
        return {
            replace: true,
            restrict: "E",
            templateUrl: '/app/templates/select.html',
            transclude: true,
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                model: '=',
                descriptor: '&?',
                selection: "@",
                disabled: "@",
                items: '=?',
                notNull:'@',
                selected: '@'
            }
        };
    });
})
    (angular.module('application'));


