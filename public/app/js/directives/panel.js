/**
 * Created by keshvari on 7/6/14.
 */
(function (module) {
    module.directive('uiPanel',['$timeout',function ($timeout) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                title: '@',
                type: '@',
                collapsible: "@",
                visible:"@",
                disabled:"@"
            },
            transclude: true,
            templateUrl: '/app/templates/panel.html',
            link: function ($scope) {
                console.log('linking panel');
                if(angular.isUndefined($scope.collapsible)){
                    $scope.collapsible = "false";
                }
                if(angular.isUndefined($scope.type)){
                    $scope.type = "default";
                }
                $scope.isOpen = $scope.collapsible == "true";


                $timeout(function () {
                    $scope.$apply(function () {
                        $scope.visible=angular.isUndefined($scope.visible)?true:$scope.visible;
                        $scope.disabled=angular.isUndefined($scope.disabled)?false:$scope.disabled;
                    });
                });
            }
        };
    }]);
})(angular.module('application'));
