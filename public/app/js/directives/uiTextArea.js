/**
 * Created by keshvari on 10/12/14.
 */
(function (module) {

    module.directive("uiTextarea", [function () {

        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl:'/app/templates/textarea.html' ,
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                model: '=',
                descriptor: '&?',
                disabled: "@",
                notNull: "@"
            }
        }
    }]);
})(angular.module('application'));