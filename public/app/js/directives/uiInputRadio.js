/**
 * Created by keshvari on 7/27/14.
 */
(function (module) {

    module.directive("uiInputRadio", ["$q", "$http", function ($q, $http) {
        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl:'/app/templates/radio.html' ,
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                model: '=?',
                descriptor: '&?',
                disabled: "@"
            }
        }
    }]);
})(angular.module('application'));


