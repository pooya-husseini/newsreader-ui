/**
 * Created by keshvari on 7/27/14.
 */
/**
 * Created by keshvari on 7/27/14.
 */
(function (module) {

    module.directive("uiInputCheckbox", ['$timeout', function ($timeout) {
        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/templates/checkbox.html',
            scope: {
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                model: '=?',
                descriptor: '&?',
                disabled: "@",
                selection: "@",
                visible: "@"
            },
            link: function (scope) {
                $timeout(function(){
                    scope.$apply(function(){
                        scope.visible = angular.isDefined(scope.visible) ? scope.visible : true;
                        scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
                    });
                })
            }
        }
    }]);
})(angular.module('application'));


