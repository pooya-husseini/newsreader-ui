(function (module) {

    module.directive("uiSidebar", [function () {
        return {
            restrict: "E",
            replace: true,
            transclude: true,
            templateUrl: '/app/templates/sidebar.html'
        };
    }]);

})(angular.module('application'));