/**
 * Created by keshvari on 9/16/14.
 */
(function(module){
    module.directive('uiDate', function () {
        return{
            restrict:'E',
            replace:true,
            templateUrl:'/app/templates/uiDate.html',
            scope:{
                model:"=",
                closeText:"@",
                orientation:"@",
                labelSize:"@",
                state:"@",
                label:"@",
                editable:"@",
                placeholder:"@",
                today:"@",
                longFormat:"@"
            },
            link: function ($scope, element, attrs) {
                if($scope.today == 'true'){
                    $scope.today = function() {
                        $scope.model = new Date();
                    };
                    $scope.today();

                }

                $scope.longFormat = $scope.model.getTime();


                $scope.clear = function () {
                    $scope.model = null;
                };

                // Disable weekend selection
                $scope.disabled = function(date, mode) {
                    return ( mode === 'day' &&date.getDay() === 5  );
                };

                $scope.toggleMin = function() {
                    $scope.minDate = $scope.minDate ? null : new Date();
                };
                $scope.toggleMin();

                $scope.openPersian = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.persianIsOpen = true;
                    $scope.gregorianIsOpen = false;
                };

                $scope.openGregorian = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.gregorianIsOpen = true;
                    $scope.persianIsOpen = false;
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 6
                };

                $scope.initDate = new Date('2016-15-20');
                $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];
            }
        }

    });
})(angular.module('application'));