(function (module) {

    module.directive("uiThumbnailCard", [function () {
        return {
            restrict: "E",
            replace: true,
            transclude: true,
            templateUrl: '/app/templates/thumbnailCard.html',
            scope: {
                image: "@",
                type: "@"
            }
        };
    }]);

})(angular.module('application'));