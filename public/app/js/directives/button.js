(function (module) {

    module.directive("uiButton",['$parse', '$exceptionHandler', function ($parse, $exceptionHandler) {
        return {
            restrict: "E",
            replace: true,
            transclude: true,
            templateUrl:'/app/templates/button.html' ,
            scope: {
                type: "@",
                size: "@",
                block: "@",
                active: "@",
                disabled:"=",
                action: '@'
            },
            compile: function ($element) {
                $($element).attr('ng-transclude', null);
                var ngClick = $element.attr('ng-click');
                if (ngClick != 'act();') {
                    $exceptionHandler('You have specified your action via the `ng-click` directive (' + ngClick + ') ' +
                        'which is reserved by the `button` directive. Try using the `action` attribute with your `button`' +
                        'directive instead. Cowardly refusing to link the directive in.', 'Error in node "' + $($element).uniquePath() + '"');
                    return function () {};
                }
                return function ($scope) {
                    var expression = undefined;
                    $scope.act = function () {
                        if ($scope.disable) {
                            return;
                        }
                        if (angular.isUndefined(expression)) {
                            if ($scope.action) {
                                expression = $parse($scope.action);
                            } else {
                                expression = function () {};
                            }
                        }
                        var scope = $scope;
                        while (scope && !scope.$$childHead) {
                            scope = scope.$parent;
                        }
                        if (scope) {
                            expression(scope);
                        }
                    };
                }
            }
        }
    }]);
})(angular.module('application'));
