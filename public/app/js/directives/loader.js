(function (module) {
    module.directive('uiLoader', function () {
        return {
            restrict: "A",
            link: function ($scope, $element) {
                $element.addClass('loader');
                angular.element('body').append($element);
            }
        }
    });
})(angular.module('application'));