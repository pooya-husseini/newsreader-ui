(function (module) {

    module.directive("uiSidebarMenuItem", [function () {
        return {
            require: "^uiSidebarMenu",
            restrict: "E",
            transclude: true,
            replace: true,
            templateUrl: '/app/templates/sidebarMenuItem.html',
            scope: {
                link: "@",
                glyph: "@"
            }
        };
    }]);
})(angular.module('application'));