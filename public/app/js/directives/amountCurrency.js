/**
 * Created by safari on 08/10/2014.
 */
(function (module) {
    module.directive('uiAmountCurrency', ['$filter', function ($filter) {

        var currencyCtrl = function ($scope, $element) {
            var self = this;

            $scope.currencyDef = [
                { code: 'RI', precision: 0, banknoteName: 'ریال', unitName: 'ریال', coinName: '', disabled: true },
                { code: 'USD', precision: 2, banknoteName: 'دلارآمریکا', unitName: 'دلار', coinName: 'سنت', disabled: true},
                { code: 'EUR', precision: 2, banknoteName: 'یرو', unitName: 'یرو', coinName: 'سنت', disabled: true},
                { code: 'GBP', precision: 0, banknoteName: 'پوند', unitName: 'پوند', coinName: '', disabled: true }
            ];

            $scope.currency = undefined;

            var th = ['', 'هزار', 'میلیون', 'میلیارد', 'بیلیون'];

            var dg = ['', 'یک', 'دو', 'سه', 'چهار', 'پنج', 'شش', 'هفت', 'هشت', 'نه'];
            var tn = ['ده', 'یازده', 'دوازده', 'سیزده', 'چهارده', 'پانزده', 'شانزده', 'هفده', 'هجده', 'نوزده'];
            var tw = ['بیست', 'سی', 'چهل', 'پنجاه', 'شصت', 'هفتاده', 'هشتاده', 'نود'];
            var hn = ['', 'یکصد', 'دویست', 'سیصد', 'چهارصد', 'پانصد', 'ششصد', 'هفتصد', 'هشتصد', 'نهصد'];


            self.toword = "";

            self.toWords = function (s) {
                if (angular.isUndefined(s))
                    return 'not a number';
                s = s.toString();
                s = s.replace(/[\, ]/g, '');
                if (s != parseFloat(s))
                    return 'not a number';
                var x = s.indexOf('.');
                if (x == -1) x = s.length;
                if (x > 15)
                    return 'too big';
                var n = s.split('');
                var str = '';
                var sk = 0;
                for (var i = 0; i < x; i++) {
                    if ((x - i) % 3 === 2) {
                        if (n[i] === '1') {
                            if (str.length > 0) {
                                str += ' و ';
                            }
                            str += tn[Number(n[i + 1])] + ' ';
                            i++;
                            sk = 1;
                        }
                        else if (n[i] !== '0') {
                            if (str.length > 0) {
                                str += ' و ';
                            }
                            str += tw[n[i] - 2] + ' ';
                            sk = 1;
                        }
                    }
                    else if (n[i] !== '0') {

                        if ((x - i) % 3 === 0) {
                            if (str.length > 0) {
                                str += ' و ';
                            }
                            str += hn[n[i]] + ' ';
                        }
                        else {
                            if (str.length > 0) {
                                str += ' و ';
                            }
                            str += dg[n[i]] + ' ';
                        }
                        sk = 1;
                    }
                    if ((x - i) % 3 === 1) {
                        var mustUsedAnd = x - i - 1;
                        if (sk) {
                            str += th[mustUsedAnd / 3] + ' ';
                        }
                        sk = 0;
                    }
                }
                if (str !== "") {
                    str += ' ' + $scope.currency.unitName + ' ';
                }
                if (x != s.length) {
                    if (str !== "") {
                        str += ' و ';
                    }
                    s = s.slice(x + 1, s.length);
                    n = s.split('');
                    var y = s.length;
                    var sk = 0;
                    for (var i = 0; i < y; i++) {
                        if (n[i] === '0')
                            continue;
                        if (i === 0) {
                            if (n[i] === '1') {
                                var num = n[i + 1];
                                if ((num === undefined) || isNaN(num))
                                    num = 0;
                                str += tn[num] + ' ';
                                sk = 1;
                                i++;
                            }
                            else {
                                str += tw[n[i] - 2] + ' ';
                                sk = 1;
                            }
                        }
                        else {
                            if (sk === 1) {
                                str += ' و '
                                sk = 0;
                            }
                            str += dg[n[i]];
                        }
                    }
                    str += ' ' + $scope.currency.coinName + ' ';
                }
                return str.replace(/\s+/g, ' ');
            };

            self.toNumberRegex = new RegExp('[^0-9\\.]', 'g');

            self.filterFunc = function (value, currency) {
                return $filter('currency')(value, currency);
            };

            self.getCaretPosition = function (input) {
                if (!input) return 0;
                if (input.selectionStart !== undefined) {
                    return input.selectionStart;
                } else if (document.selection) {
                    // Curse you IE
                    input.focus();
                    var selection = document.selection.createRange();
                    selection.moveStart('character', input.value ? -input.value.length : 0);
                    return selection.text.length;
                }
                return 0;
            };

            self.setCaretPosition = function (input, pos) {
                if (!input) return 0;
                if (input.offsetWidth === 0 || input.offsetHeight === 0) {
                    return; // Input's hidden
                }
                if (input.setSelectionRange) {
                    input.focus();
                    input.setSelectionRange(pos, pos);
                }
                else if (input.createTextRange) {
                    // Curse you IE
                    var range = input.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                }
            };

            self.toNumber = function (currencyStr, currency) {
                var value = parseFloat(currencyStr.replace(self.toNumberRegex, ''));
                if($scope.currency.precision > 0){
                    var strValue = value.toString();
                    if(strValue.indexOf('.') !== -1 && strValue.length - strValue.indexOf('.') >= 4){
                        strValue = strValue.slice(0, strValue.length - 1);
                    }
                    value = parseFloat(strValue);
                }
                return value
            };

            self.standarCharacterPressed = function (viewValue, key) {
                viewValue = self.toNumber(viewValue, $scope.currency);
                var integerValue = Math.floor(viewValue);
                var decimalValue = viewValue - integerValue;
                switch (key.toLowerCase()) {
                    case 'h':
                        integerValue *= 100;
                        break;
                    case 't':
                        integerValue *= 1000;
                        break;
                    case 'm':
                        integerValue *= 1000000;
                        break;
                    case 'b':
                        integerValue *= 1000000000;
                        break;
                    case '*':
                        integerValue *= 1000;
                        break;
                }
                return integerValue + decimalValue;
            };

            self.init = function (inputElement) {
                var ngModelController = inputElement.controller('ngModel');
                ngModelController.$formatters.push(function (newViewValue) {
                    if (angular.isUndefined(newViewValue) || angular.isUndefined($scope.currency)) {
                        return;
                    }
                    return self.filterFunc(newViewValue, $scope.currency);
                });
                ngModelController.$parsers.push(function (newViewValue) {
                    if (angular.isUndefined(newViewValue) || angular.isUndefined($scope.currency)) {
                        return;
                    } else if (newViewValue === '.00') {
                        newViewValue = "";
                    }
                    var oldModelValue = ngModelController.$modelValue;
                    var newModelValue;

                    ///ignored conditions
                    if (!(angular.isUndefined(oldModelValue) || isNaN(oldModelValue)) &&
                        (($scope.currency.precision > 0 && newViewValue.length > 0 && (newViewValue.indexOf('.') === -1 || (newViewValue.match(/\./g) || []).length > 1)) ||
                            ($scope.currency.precision === 0 && (newViewValue.match(/\./g) || []).length > 0) ||
                            (self.toNumber(newViewValue, $scope.currency) > 999999999999999)))
                        newModelValue = oldModelValue;
                    else {
                        ///Remove obtrusive zero at middle of input
                        if (oldModelValue < 1 &&
                            (($scope.currency.precision > 0 && newViewValue.match(/\d[0][.]\d\d/)) ||
                                ($scope.currency.precision === 0 && newViewValue.match(/\d[0]/)))) {
                            newViewValue = newViewValue.slice(0, 1) + newViewValue.slice(2, newViewValue.length);
                        }
                        //var match = newViewValue.match(/[H|T|M|B]/i);
                        var match = newViewValue.match(/[\*]/i);
                        if (match !== null && match.length == 1)
                            newModelValue = self.standarCharacterPressed(newViewValue, newViewValue.charAt(match.index));
                        else
                            newModelValue = self.toNumber(newViewValue, $scope.currency);
                    }
                    if (newModelValue === undefined) {
                        newModelValue = 0;
                    }
                    else if (isNaN(newModelValue)) {
                        ngModelController.$viewValue = "";
                    }
                    else {
                        ngModelController.$viewValue = self.filterFunc(newModelValue, $scope.currency);
                    }

                    var pos = self.getCaretPosition(inputElement[0]);
                    inputElement.val(ngModelController.$viewValue);

                    var newPos;
                    if ($scope.currency.precision > 0 && pos > newViewValue.indexOf('.'))
                        newPos = pos;
                    else
                        newPos = pos + ngModelController.$viewValue.length -
                            newViewValue.length;
//                    if ((oldModelValue === undefined) || isNaN(oldModelValue)) {
//                        newPos -= 3;
//                    }
                    self.setCaretPosition(inputElement[0], newPos);
                    return newModelValue;
                });
            };

            self.amountChanged = function () {
                scope.toword = amountCurrencyCtrl.toWords(scope.amount);
            }
        };

        return {
            link: function postLink(scope, elem, attrs, ctrls) {
                var amountCurrencyCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                amountCurrencyCtrl.init(elem.find('input'));

                scope.$watch(function(){
                    return scope.disabled;
                }, function(){
                    if (angular.isUndefined(scope.disabled)) {
                        scope.disabled = false;
                    }
                })
                if (angular.isUndefined(scope.currencyOptions)) {
                    scope.currencyOptions = scope.currencyDef;
                    scope.currencyOptions[0]['disabled'] = false;
                }
                if (angular.isUndefined(scope.currencyCode)) {
                    angular.forEach(scope.currencyOptions, function (value, index) {
                        if (index === 0) {
                            value['disabled'] = false;
                        }
                        else {
                            value['disabled'] = true;
                        }
                    })
                    scope.currencyCode = scope.currencyOptions[0].code;
                }
                scope.$watch(function () {
                    return scope.currencyCode;
                }, function () {
                    if (angular.isUndefined(scope.currencyCode)) {
                        return
                    }
                    if (scope.currencyCode === "") {
                        scope.currency = undefined;
                        return;
                    }
                    var findedCurrency = $filter("filter")(scope.currencyOptions, { code: scope.currencyCode }, true);
                    if (findedCurrency.length == 0)
                        throw new Error("Currency not found: " + scope.currencyCode);

                    scope.currency = findedCurrency[0];

                    if (scope.currency.precision == 0)
                        scope.amount = Math.floor(scope.amount);
                    var newValue = amountCurrencyCtrl.filterFunc(scope.amount, scope.currency);
                    elem.find('input').val(newValue);
                    scope.toword = amountCurrencyCtrl.toWords(scope.amount);
                    var options = {
                        content: scope.toword,
                        placement: 'auto'
                    };
                    angular.element('#toword').popover(options)
                });

//                scope.$watch(function () {
//                    return scope.model;
//                }, function () {
//                    if (angular.isUndefined(scope.currency)) {
//                        return;
//                    }
//                    scope.amount = scope.model;
//                    scope.toword = amountCurrencyCtrl.toWords(scope.amount);
//                    var options = {
//                        content: scope.toword,
//                        placement: 'auto'
//                    };
//                    angular.element('#toword').popover(options)
//                });

                scope.$watch(function () {
                    return scope.amount;
                }, function () {
                    if (angular.isUndefined(scope.currency)) {
                        return;
                    }
                    if (angular.isUndefined(scope.amount) || isNaN(scope.amount)) {
                        scope.amount = ngModelCtrl.$modelValue;
                    } else {
                        ngModelCtrl.$setViewValue(scope.amount);
                    }
                    scope.toword = amountCurrencyCtrl.toWords(scope.amount);
                    var options = {
                        content: scope.toword,
                        placement: 'auto'
                    };
                    angular.element('#toword').popover(options)
                });
            },
            restrict: 'E',
            require: ['uiAmountCurrency', 'ngModel'],
            scope: {
                model: "=",
                currencyCode: "=?",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                disabled: "=?",
                currencyOptions: "=?"
            },
            controller: currencyCtrl,

            templateUrl: "/app/templates/amountCurrency.html"
        }
    }]);
    module.filter('currency', function () {
        return function (number, currency) {
            if (angular.isUndefined(number) || isNaN(number)) {
                return "";
            }
            var options = {};
            options.thousand = ',';
            options.decimal = '.';
            options.format = "%s%v";
            options.symbol = "";
            options.precision = currency.precision;

            return accounting.formatMoney(number, options);
        };
    });

})(angular.module('application'));