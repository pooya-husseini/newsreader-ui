/**
 * Created by heman on 12/11/14.
 */

(function(module){
    "use strict";
    module.factory('underscore',['$window',function ($window) {
        if('_' in $window)
            return $window._;
        else
            throw "Underscore library has not loaded, please check index file";
    }])

})(angular.module('services'));
