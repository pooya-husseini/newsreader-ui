/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */


module.service("GeneralParameterRestService", ['$resource', 'projectCache', function ($resource, projectCache) {
    var resource = $resource('/ir/phsys/sitereader/generalparameters/:url:id', {}, {
        findById: {method: 'GET', params: {id: '@id'}},
        searchColumns: {method: 'GET', isArray: true, params: {url: 'searchColumn'}},
        get: {method: 'GET', isArray: true, params: {url: 'all'}},
        searchGeneralParameter: {method: 'POST', isArray: true, params: {url: 'searchgeneralParameters'}},
        create: {
            method: 'POST', params: {url: 'create'}, transformRequest: function (data, h) {
                addItemToCache(data);
                return JSON.stringify(data);
            }
        },
        update: {
            method: 'PUT', params: {id: '@id'}
            , transformResponse: function (data, h) {
                var obj = JSON.parse(data);
                addItemToCache(obj);
                return obj;
            }
        },
        delete: {
            method: 'DELETE', params: {id: '@id'}, transformRequest: function (data, h) {
                return JSON.stringify(data);
            }

        },
        getGeneralParameterTypes: {method: 'GET', isArray: true, params: {url: 'generalParameterTypes'}},
    });


    function addItemToCache(data) {
        var key = data.code + '.' + data.mainType;
        var value = data.description;
        projectCache.put(key, value);
    }

    function removeItemFromCache(data) {
        var key = data.code + '.' + data.mainType;
        projectCache.remove(key);
    }


    resource.get().$promise.then(function (success) {
        for (var index in success) {
            if (success.hasOwnProperty(index)) {

                addItemToCache(success[index]);

            }
        }
    });
    return resource;
}]);