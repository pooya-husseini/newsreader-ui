/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 4:05:03 PM
 * @version 1.0.0
 */


module.service("SearchNewsRestService", ['$resource', function ($resource) {
    var resource = $resource('/ir/phsys/sitereader/:url?start=:start&rows=:rows', {}, {
        findById: {method: 'GET', params: {id: '@id'}},
        searchColumns: {method: 'GET', isArray: true, params: {url: 'searchColumn'}},
        get: {method: 'GET', isArray: true, params: {url: 'all'}},
        searchNews: {method: 'POST', isArray: true, params: {url: 'searchNews', start: '@start', rows: '@rows'}},
        create: {method: 'POST', params: {url: 'create'}},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {
            method: 'DELETE', params: {id: '@id'}
        }
    });

    return resource;
}]);