/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 4:05:03 PM
* @version 1.0.0
*/


(function (module) {
module.service( "SearchNewsByQueryCrudService",  ['$modal', function ($modal) {
    return {


        select: function () {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/SearchSearchNewsByQuery.html",
                size: 'lg',
                controller: 'SearchNewsByQueryCrudCtrl',
                resolve: {
                    data: function () {
                        return undefined;
                    }
                }
            }).result;
        },

        edit: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/EditSearchNewsByQuery.html",
                size: 'lg',
                controller: 'SearchNewsByQueryCrudCtrl',
             resolve: {
                    data: function () {
                        return model;
                    }
                }

            }).result;
        },
        create: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/CreateSearchNewsByQuery.html",
                size: 'lg',
                controller: 'SearchNewsByQueryCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        },
        show: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/ShowSearchNewsByQuery.html",
                size: 'lg',
                controller: 'SearchNewsByQueryCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        }
    }

}]).controller('SearchNewsByQueryCrudCtrl', [
'$scope',
'SearchNewsByQueryRestService',
'$modalInstance',
'data',
'ModelService'
,'SearchNewsByQueryCrudService',
'$timeout'
,
function ($scope,
                        SearchNewsByQueryRestService,
                        $modalInstance,
                        data,
                        ModelService
                        ,SearchNewsByQueryCrudService,
                        $timeout
                                                )

{
    $scope.Model = {};
    $scope.Action = {};
    $scope.selected = {};

    if (data != undefined) {
        $scope.Model = data;
    }

    function getModel(){
                return $scope.Model;
    }


    
    $scope.Action.reset = function () {
        $scope.Model = {};
    };

    $scope.SearchNewsByQueryGrid = {
        columnDefs:getSearchNewsByQueryGridHeaders(),
        data: 'SearchNewsByQueryGrid.content',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enablePaginationControls: false,
        paginationPageSize: 10,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.SearchNewsByQueryGrid.content=[];

    $scope.$watch('SearchNewsByQueryGrid.content', function () {
        $scope.SearchNewsByQueryGrid.isSelected = false;
    });

    $scope.SearchNewsByQueryGrid.onRegisterApi = function (gridApi) {
        $scope.SearchNewsByQueryGrid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.SearchNewsByQueryGrid.isSelected = row.isSelected;
        });
    };

            function getSearchNewsByQueryGridHeaders(){
            return  [
                {
            field: 'query',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.SearchNewsByQuery.query.label')
                                            },
                {
            field: 'id',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.SearchNewsByQuery.id.label')
            ,visible:false                                },
                {
            field: 'tag',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.SearchNewsByQuery.tag.label')
                                     ,cellFilter: 'GeneralParameterFilter:"newsTag"'         },
                    ];
    }
    
    $scope.Action.find = function () {
       SearchNewsByQueryRestService.searchSearchNewsByQuery(getModel()).$promise.then(
        function (data) {
                $scope.SearchNewsByQueryGrid.content=data
            }
        );
    };
    $scope.Action.insert=function(){
        $modalInstance.close(getModel());
    };
    $scope.Action.add = function () {
        var selected =  $scope.SearchNewsByQueryGrid.gridApi.selection.getSelectedRows()[0]
        $modalInstance.close(selected);
    };
    $scope.Action.cancel = function () {
        $modalInstance.dismiss();
    };


    window.setTimeout(function(){
        $(window).resize();
        $(window).resize();
    }, 1000);

        }]);
})(angular.module("application"));