/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 3:03:29 PM
* @version 1.0.0
*/


(function (module) {
module.service( "LogsCrudService",  ['$modal', function ($modal) {
    return {


        select: function () {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/SearchLogs.html",
                size: 'lg',
                controller: 'LogsCrudCtrl',
                resolve: {
                    data: function () {
                        return undefined;
                    }
                }
            }).result;
        },

        edit: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/EditLogs.html",
                size: 'lg',
                controller: 'LogsCrudCtrl',
             resolve: {
                    data: function () {
                        return model;
                    }
                }

            }).result;
        },
        create: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/CreateLogs.html",
                size: 'lg',
                controller: 'LogsCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        },
        show: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/ShowLogs.html",
                size: 'lg',
                controller: 'LogsCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        }
    }

}]).controller('LogsCrudCtrl', [
'$scope',
'LogsRestService',
'$modalInstance',
'data',
'ModelService'
,'LogsCrudService',
'$timeout'
,
function ($scope,
                        LogsRestService,
                        $modalInstance,
                        data,
                        ModelService
                        ,LogsCrudService,
                        $timeout
                                                )

{
    $scope.Model = {};
    $scope.Action = {};
    $scope.selected = {};

    if (data != undefined) {
        $scope.Model = data;
    }

    function getModel(){
                return $scope.Model;
    }


    
    $scope.Action.reset = function () {
        $scope.Model = {};
    };

    $scope.LogsGrid = {
        columnDefs:getLogsGridHeaders(),
        data: 'LogsGrid.content',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enablePaginationControls: false,
        paginationPageSize: 10,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.LogsGrid.content=[];

    $scope.$watch('LogsGrid.content', function () {
        $scope.LogsGrid.isSelected = false;
    });

    $scope.LogsGrid.onRegisterApi = function (gridApi) {
        $scope.LogsGrid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.LogsGrid.isSelected = row.isSelected;
        });
    };

            function getLogsGridHeaders(){
            return  [
                {
            field: 'level',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.Logs.level.label')
                                            },
                {
            field: 'id',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.Logs.id.label')
            ,visible:false                                },
                    ];
    }
    
    $scope.Action.find = function () {
       LogsRestService.searchLogs(getModel()).$promise.then(
        function (data) {
                $scope.LogsGrid.content=data
            }
        );
    };
    $scope.Action.insert=function(){
        $modalInstance.close(getModel());
    };
    $scope.Action.add = function () {
        var selected =  $scope.LogsGrid.gridApi.selection.getSelectedRows()[0]
        $modalInstance.close(selected);
    };
    $scope.Action.cancel = function () {
        $modalInstance.dismiss();
    };


    window.setTimeout(function(){
        $(window).resize();
        $(window).resize();
    }, 1000);

        }]);
})(angular.module("application"));