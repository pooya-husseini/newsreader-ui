/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */


module.service("ManualRunRestService", ['$resource', 'projectCache', function ($resource, projectCache) {
    var resource = $resource('/ir/phsys/sitereader/:url:tag', {}, {
        findById: {method: 'GET', params: {id: '@id'}},
        searchColumns: {method: 'GET', isArray: true, params: {url: 'searchColumn'}},
        get: {method: 'GET', isArray: true, params: {url: 'all'}},
        searchManualRun: {method: 'POST', isArray: true, params: {url: 'searchmanualRuns'}},
        create: {method: 'POST', params: {url: 'create'}},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {
            method: 'DELETE', params: {id: '@id'}
        },
        getAgencies: {method: 'GET', isArray: true, params: {url: 'agencies'}},
        getAgencyStatus: {method: 'POST', isArray: false, params: {url: 'status'}},
        getNotSuccessStatus: {method: 'POST', isArray: true, params: {url: 'notSuccess'}},
        getAllNonSuccessStatus: {method: 'POST', isArray: true, params: {url: 'allNotSuccess'}},
        makeStatusFixed: {method: 'POST', isArray: true, params: {url: 'makeStatusFinished'}},
        runReading: {method: 'GET', isArray: false, params: {url: 'run/', tag: '@tag'}}
    });

    resource.getAgencies().$promise.then(
        function (success) {
            var keys = [];
            for (var i in success) {
                if (i.indexOf("$") != 0) {
                    keys.push(success[i].tag);
                    projectCache.put('tag.' + success[i].tag, success[i].name);
                }

            }
            projectCache.put("tag.keys", keys);
        }, function (failure) {
            notify(failure, "error")
        }
    );


    return resource;
}]);