/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:29 PM
 * @version 1.0.0
 */


(function (module) {
    module.service("ManualPopupService", ['$modal', function ($modal) {
        return {
            showStatus: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/ShowManualRun.html",
                    size: 'lg',
                    controller: 'ManualRunCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }
                }).result;
            }
        }

    }]).controller('ManualRunCrudCtrl', [
        '$scope',
        'ManualRunRestService',
        '$modalInstance',
        'data',
        'ModelService'
        , 'ManualPopupService',
        '$timeout'
        ,
        function ($scope, ManualRunRestService, $modalInstance, data, ModelService , ManualRunCrudService, $timeout) {

            $scope.Action = {};
            $scope.selected = {};

            $scope.StatusGrid = {
                columnDefs: getStatusGridHeaders(),
                data: 'StatusGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enablePaginationControls: false,
                paginationPageSize: 10,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                rowTemplate:'<div  ><div class="ui-grid-cell" ng-class="{warningContent : row.entity.status==\'empty\',failureContent : row.entity.status==\'failed\'}" ng-repeat="col in colContainer.renderedColumns track by col.colDef.name" ui-grid-cell></div></div>'

            };

            $scope.StatusGrid.content = [];

            if (data != undefined) {
                $scope.StatusGrid.content = data;
            }

            $scope.$watch('StatusGridGrid.content', function () {
                $scope.StatusGrid.isSelected = false;
            });

            $scope.StatusGrid.onRegisterApi = function (gridApi) {
                $scope.StatusGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.StatusGrid.isSelected = row.isSelected;
                });
            };

            function getStatusGridHeaders() {
                return [
                    {
                        field: 'publisher',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.publisher'),
                        width:"10%"
                    },
                    {
                        field: 'title',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.title'),
                        width:"61%"

                    },
                    {
                        field: 'links',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.link'),
                        cellTemplate:'<a href="{{row.entity.links}}" target="_blank"><div class="glyphicon glyphicon-link"></div></a>',
                        width:"7%"

                    },
                    {
                        field: 'pubDate',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.pubDate'),
                        width:"22%",
                        cellFilter:'date:\'yyyy-MM-dd HH:mm:ss\''
                    },
                    {
                        field: 'status',
                        displayName: $scope.translate('view.ir.phsys.sitereader.ManualRun.id.label')
                        , visible: false
                    }
                ];
            }

            window.setTimeout(function () {
                $(window).resize();
                $(window).resize();
            }, 1000);

        }]);
})(angular.module("application"));