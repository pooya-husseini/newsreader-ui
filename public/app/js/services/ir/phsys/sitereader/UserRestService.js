/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:28 PM
 * @version 1.0.0
 */


module.service("UserRestService", ['$resource', function ($resource) {
    var resource = $resource('/ir/phsys/sitereader/users/:url:id', {}, {
        findById: {method: 'GET', params: {id: '@id'}},
        searchColumns: {method: 'GET', isArray: true, params: {url: 'searchColumn'}},
        get: {method: 'GET', isArray: true, params: {url: 'all'}},
        searchUser: {method: 'POST', isArray: true, params: {url: 'searchusers'}},
        changePassword: {method: 'PUT', params: {url: "changePassword"}},
        create: {method: 'POST', params: {url: 'create'}},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {
            method: 'DELETE', params: {id: '@id'}
        },
        getUserManagementRoles: {method: 'GET', isArray: true, params: {url: 'userManagementRoles'}},
    });

    return resource;
}]);