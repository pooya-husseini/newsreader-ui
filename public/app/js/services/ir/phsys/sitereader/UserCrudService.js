/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 3:03:29 PM
 * @version 1.0.0
 */


(function (module) {
    module.service("UserCrudService", ['$modal', function ($modal) {
        return {


            changePassword: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/ChangePassword.html",
                    size: 'sm',
                    controller: 'UserCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }

                }).result;
            },

            select: function () {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/SearchUser.html",
                    size: 'lg',
                    controller: 'UserCrudCtrl',
                    resolve: {
                        data: function () {
                            return undefined;
                        }
                    }
                }).result;
            },

            edit: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/EditUser.html",
                    size: 'lg',
                    controller: 'UserCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }

                }).result;
            },
            create: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/CreateUser.html",
                    size: 'lg',
                    controller: 'UserCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }
                }).result;
            },
            show: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/ShowUser.html",
                    size: 'lg',
                    controller: 'UserCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }
                }).result;
            }
        }

    }]).controller('UserCrudCtrl', [
        '$scope',
        'UserRestService',
        '$modalInstance',
        'data',
        'ModelService'
        , 'UserCrudService',
        '$timeout'
        ,
        function ($scope,
                  UserRestService,
                  $modalInstance,
                  data,
                  ModelService
            , UserCrudService,
                  $timeout) {
            $scope.Model = {};
            $scope.Action = {};
            $scope.selected = {};

            if (data != undefined) {
                $scope.Model = data;
            }

            function getModel() {
                return $scope.Model;
            }


            $scope.Action.reset = function () {
                $scope.Model = {};
            };

            $scope.UserGrid = {
                columnDefs: getUserGridHeaders(),
                data: 'UserGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enablePaginationControls: false,
                paginationPageSize: 10,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.UserGrid.content = [];

            $scope.$watch('UserGrid.content', function () {
                $scope.UserGrid.isSelected = false;
            });

            $scope.UserGrid.onRegisterApi = function (gridApi) {
                $scope.UserGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.UserGrid.isSelected = row.isSelected;
                });
            };

            function getUserGridHeaders() {
                return [
                    {
                        field: 'id',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.id.label')
                        , visible: false
                    },
                    {
                        field: 'firstName',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.firstName.label')
                    },
                    {
                        field: 'lastName',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.lastName.label')
                    },
                    {
                        field: 'password',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.password.label')
                        , visible: false
                    },
                    {
                        field: 'username',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.username.label')
                    },
                    {
                        field: 'role',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.role.label')
                    },
                    {
                        field: 'campus',

                        displayName: $scope.translate('view.ir.phsys.sitereader.User.campus.label')
                        , cellFilter: 'GeneralParameterFilter:"CAMPUS"'
                    },
                ];
            }

            $scope.Action.find = function () {
                UserRestService.searchUser(getModel()).$promise.then(
                    function (data) {
                        $scope.UserGrid.content = data
                    }
                );
            };
            $scope.Action.insert = function () {
                $modalInstance.close(getModel());
            };
            $scope.Action.add = function () {
                var selected = $scope.UserGrid.gridApi.selection.getSelectedRows()[0]
                $modalInstance.close(selected);
            };
            $scope.Action.cancel = function () {
                $modalInstance.dismiss();
            };


            window.setTimeout(function () {
                $(window).resize();
                $(window).resize();
            }, 1000);

            (function () {
                UserRestService.getUserManagementRoles().$promise.then(
                    function (success) {

                        $scope.UserManagementRole = ModelService.convertEnum(success, 'ir.phsys.sitereader.UserManagementRole');
                    });
            })();

        }]);
})(angular.module("application"));