/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Aug 12, 2015 4:05:03 PM
 * @version 1.0.0
 */


(function (module) {
    module.service("SearchNewsCrudService", ['$modal', function ($modal) {
        return {


            select: function () {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/SearchSearchNews.html",
                    size: 'lg',
                    controller: 'SearchNewsCrudCtrl',
                    resolve: {
                        data: function () {
                            return undefined;
                        }
                    }
                }).result;
            },

            edit: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/EditSearchNews.html",
                    size: 'lg',
                    controller: 'SearchNewsCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }

                }).result;
            },
            create: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/CreateSearchNews.html",
                    size: 'lg',
                    controller: 'SearchNewsCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }
                }).result;
            },
            show: function (model) {
                return $modal.open({
                    templateUrl: "/app/view/ir/phsys/sitereader/ShowSearchNews.html",
                    size: 'lg',
                    controller: 'SearchNewsCrudCtrl',
                    resolve: {
                        data: function () {
                            return model;
                        }
                    }
                }).result;
            }
        }

    }]).controller('SearchNewsCrudCtrl', [
        '$scope',
        'SearchNewsRestService',
        '$modalInstance',
        'data',
        'ModelService'
        , 'SearchNewsCrudService',
        '$timeout'
        ,
        function ($scope,
                  SearchNewsRestService,
                  $modalInstance,
                  data,
                  ModelService
            , SearchNewsCrudService,
                  $timeout) {
            $scope.Model = {};
            $scope.Action = {};
            $scope.selected = {};

            if (data != undefined) {
                $scope.Model = data;
            }

            function getModel() {
                return $scope.Model;
            }


            $scope.Action.reset = function () {
                $scope.Model = {};
            };

            $scope.SearchNewsGrid = {
                columnDefs: getSearchNewsGridHeaders(),
                data: 'SearchNewsGrid.content',
                multiSelect: false,
                isSelected: true,
                enableHighlighting: true,
                enableColumnReordering: true,
                enableColumnResize: true,
                enablePaginationControls: false,
                paginationPageSize: 10,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.SearchNewsGrid.content = [];

            $scope.$watch('SearchNewsGrid.content', function () {
                $scope.SearchNewsGrid.isSelected = false;
            });

            $scope.SearchNewsGrid.onRegisterApi = function (gridApi) {
                $scope.SearchNewsGrid.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.SearchNewsGrid.isSelected = row.isSelected;
                });
            };

            function getSearchNewsGridHeaders() {
                return [
                    {
                        field: 'id',

                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.id.label')
                        , visible: false
                    },
                    {
                        field: 'title',

                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.title.label')
                    },
                    {
                        field: 'text',

                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.text.label')
                    },
                    {
                        field: 'category',

                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.category.label')
                    },
                    {
                        field: 'publisher',

                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.publisher.label')
                    },
                    {
                        field: 'pubDate',

                        displayName: $scope.translate('view.ir.phsys.sitereader.SearchNews.pubDate.label')
                        , cellFilter: 'persianDate'
                    },
                ];
            }

            $scope.Action.find = function () {
                SearchNewsRestService.searchSearchNews(getModel()).$promise.then(
                    function (data) {
                        $scope.SearchNewsGrid.content = data
                    }
                );
            };
            $scope.Action.insert = function () {
                $modalInstance.close(getModel());
            };
            $scope.Action.add = function () {
                var selected = $scope.SearchNewsGrid.gridApi.selection.getSelectedRows()[0]
                $modalInstance.close(selected);
            };
            $scope.Action.cancel = function () {
                $modalInstance.dismiss();
            };


            window.setTimeout(function () {
                $(window).resize();
                $(window).resize();
            }, 1000);

        }]);
})(angular.module("application"));