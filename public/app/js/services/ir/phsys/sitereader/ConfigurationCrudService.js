/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 3:03:29 PM
* @version 1.0.0
*/


(function (module) {
module.service( "ConfigurationCrudService",  ['$modal', function ($modal) {
    return {


        select: function () {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/SearchConfiguration.html",
                size: 'lg',
                controller: 'ConfigurationCrudCtrl',
                resolve: {
                    data: function () {
                        return undefined;
                    }
                }
            }).result;
        },

        edit: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/EditConfiguration.html",
                size: 'lg',
                controller: 'ConfigurationCrudCtrl',
             resolve: {
                    data: function () {
                        return model;
                    }
                }

            }).result;
        },
        create: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/CreateConfiguration.html",
                size: 'lg',
                controller: 'ConfigurationCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        },
        show: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/ShowConfiguration.html",
                size: 'lg',
                controller: 'ConfigurationCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        }
    }

}]).controller('ConfigurationCrudCtrl', [
'$scope',
'ConfigurationRestService',
'$modalInstance',
'data',
'ModelService'
,'ConfigurationCrudService',
'$timeout'
,
function ($scope,
                        ConfigurationRestService,
                        $modalInstance,
                        data,
                        ModelService
                        ,ConfigurationCrudService,
                        $timeout
                                                )

{
    $scope.Model = {};
    $scope.Action = {};
    $scope.selected = {};

    if (data != undefined) {
        $scope.Model = data;
    }

    function getModel(){
                return $scope.Model;
    }


    
    $scope.Action.reset = function () {
        $scope.Model = {};
    };

    $scope.ConfigurationGrid = {
        columnDefs:getConfigurationGridHeaders(),
        data: 'ConfigurationGrid.content',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enablePaginationControls: false,
        paginationPageSize: 10,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.ConfigurationGrid.content=[];

    $scope.$watch('ConfigurationGrid.content', function () {
        $scope.ConfigurationGrid.isSelected = false;
    });

    $scope.ConfigurationGrid.onRegisterApi = function (gridApi) {
        $scope.ConfigurationGrid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.ConfigurationGrid.isSelected = row.isSelected;
        });
    };

            function getConfigurationGridHeaders(){
            return  [
                {
            field: 'name',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.Configuration.name.label')
                                            },
                {
            field: 'id',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.Configuration.id.label')
            ,visible:false                                },
                    ];
    }
    
    $scope.Action.find = function () {
       ConfigurationRestService.searchConfiguration(getModel()).$promise.then(
        function (data) {
                $scope.ConfigurationGrid.content=data
            }
        );
    };
    $scope.Action.insert=function(){
        $modalInstance.close(getModel());
    };
    $scope.Action.add = function () {
        var selected =  $scope.ConfigurationGrid.gridApi.selection.getSelectedRows()[0]
        $modalInstance.close(selected);
    };
    $scope.Action.cancel = function () {
        $modalInstance.dismiss();
    };


    window.setTimeout(function(){
        $(window).resize();
        $(window).resize();
    }, 1000);

        }]);
})(angular.module("application"));