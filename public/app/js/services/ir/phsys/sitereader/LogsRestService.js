/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 3:03:28 PM
* @version 1.0.0
*/


module.service("LogsRestService",  ['$resource'  , function ($resource ) {
    var resource = $resource('/ir/phsys/sitereader/logs/:url:id', {},  {
            findById: { method: 'GET', params: {id: '@id'}},
            searchColumns: { method: 'GET', isArray: true, params: {url: 'searchColumn'} },
            get: { method: 'GET', isArray: true, params: {url: 'all'}},
            searchLogs: { method: 'POST', isArray: true, params: {url: 'searchlogs'}},
            create: { method: 'POST', params: {url: 'create'}},
            update: { method: 'PUT', params: {id: '@id'}  },
            delete: { method: 'DELETE', params: {id: '@id'} 
},
                            });

    return resource;
}]);