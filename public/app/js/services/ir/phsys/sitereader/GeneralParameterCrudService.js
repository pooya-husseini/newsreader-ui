/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Aug 12, 2015 3:03:29 PM
* @version 1.0.0
*/


(function (module) {
module.service( "GeneralParameterCrudService",  ['$modal', function ($modal) {
    return {


        select: function () {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/SearchGeneralParameter.html",
                size: 'lg',
                controller: 'GeneralParameterCrudCtrl',
                resolve: {
                    data: function () {
                        return undefined;
                    }
                }
            }).result;
        },

        edit: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/EditGeneralParameter.html",
                size: 'lg',
                controller: 'GeneralParameterCrudCtrl',
             resolve: {
                    data: function () {
                        return model;
                    }
                }

            }).result;
        },
        create: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/CreateGeneralParameter.html",
                size: 'lg',
                controller: 'GeneralParameterCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        },
        show: function (model) {
            return $modal.open({
                templateUrl: "/app/view/ir/phsys/sitereader/ShowGeneralParameter.html",
                size: 'lg',
                controller: 'GeneralParameterCrudCtrl',
            resolve: {
                    data: function () {
                        return model;
                    }
                }
            }).result;
        }
    }

}]).controller('GeneralParameterCrudCtrl', [
'$scope',
'GeneralParameterRestService',
'$modalInstance',
'data',
'ModelService'
,'GeneralParameterCrudService',
'$timeout'
,
function ($scope,
                        GeneralParameterRestService,
                        $modalInstance,
                        data,
                        ModelService
                        ,GeneralParameterCrudService,
                        $timeout
                                                )

{
    $scope.Model = {};
    $scope.Action = {};
    $scope.selected = {};

    if (data != undefined) {
        $scope.Model = data;
    }

    function getModel(){
                return $scope.Model;
    }


    
    $scope.Action.reset = function () {
        $scope.Model = {};
    };

    $scope.GeneralParameterGrid = {
        columnDefs:getGeneralParameterGridHeaders(),
        data: 'GeneralParameterGrid.content',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enablePaginationControls: false,
        paginationPageSize: 10,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.GeneralParameterGrid.content=[];

    $scope.$watch('GeneralParameterGrid.content', function () {
        $scope.GeneralParameterGrid.isSelected = false;
    });

    $scope.GeneralParameterGrid.onRegisterApi = function (gridApi) {
        $scope.GeneralParameterGrid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.GeneralParameterGrid.isSelected = row.isSelected;
        });
    };

            function getGeneralParameterGridHeaders(){
            return  [
                {
            field: 'id',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.id.label')
            ,visible:false                                },
                {
            field: 'mainType',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.mainType.label')
                                            },
                {
            field: 'code',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.code.label')
                                            },
                {
            field: 'description',
        
            displayName: $scope.translate('view.ir.phsys.sitereader.GeneralParameter.description.label')
                                            },
                    ];
    }
    
    $scope.Action.find = function () {
       GeneralParameterRestService.searchGeneralParameter(getModel()).$promise.then(
        function (data) {
                $scope.GeneralParameterGrid.content=data
            }
        );
    };
    $scope.Action.insert=function(){
        $modalInstance.close(getModel());
    };
    $scope.Action.add = function () {
        var selected =  $scope.GeneralParameterGrid.gridApi.selection.getSelectedRows()[0]
        $modalInstance.close(selected);
    };
    $scope.Action.cancel = function () {
        $modalInstance.dismiss();
    };


    window.setTimeout(function(){
        $(window).resize();
        $(window).resize();
    }, 1000);

                (function() {
            GeneralParameterRestService.getGeneralParameterTypes().$promise.then(
            function(success){
            
            $scope.GeneralParameterType = ModelService.convertEnum(success,'ir.phsys.sitereader.enums.GeneralParameterType');
        });
        })();

        }]);
})(angular.module("application"));