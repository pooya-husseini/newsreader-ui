(function (module) {
    'use strict';

    function arraysEqual(a1, a2) {
        return JSON.stringify(a1) == JSON.stringify(a2);
    }

    function copyArray(arr) {
        var a = [];
        for (var i in arr) {
            if (arr.hasOwnProperty(i)) {
                a.push(copyObject(arr[i]))
            }
        }
        return a;
    }

    function copyObject(obj) {
        if (null == obj || "object" != typeof obj) return obj;

        var copy = {};

        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = obj[attr];

            }
        }
        return copy;
    }

    function weakCopyObject(obj1, obj2) {
        for (var attr in obj2) {
            if (obj2.hasOwnProperty(attr)) {
                obj1[attr] = obj2[attr];
            }
        }
    }


    module.service('ModelService', ['$rootScope', '$q', '$http', '$timeout', function ($rootScope, $q, $http, $timeout) {

        this.extractSelectedItem = function (grid) {
            if (grid == undefined || grid.data == undefined) {
                return {};
            }

            var selected = copyArray(grid.selectedItems);

            if (selected.length > 1) {
                return selected
            } else if (selected.length === 1) {
                return selected[0];
            } else {
                return {};
            }

        };

        this.changeGridItem = function (model, idName, item) {

            for (var i in model) {
                if (model.hasOwnProperty(i)) {
                    if (model[i][idName] == item[idName]) {
                        weakCopyObject(model[i], item);
                        break;
                    }
                }
            }
        };
        this.removeGridItem = function (model, item) {
            for (var i in model) {
                if (model.hasOwnProperty(i)) {
                    if (arraysEqual(model[i], item)) {
                        model.splice(i, 1);
                        break;
                    }
                }
            }
        };
        this.addToModel = function (model, data) {
            var result = copyArray(model);

            if (data instanceof Array) {
                result = result.concat(data);
            } else {
                result.push(data);
            }

            return result;
        };

        this.getMimeType = function (extension) {
            switch (extension) {
                case "docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "pdf":
                    return "application/pdf";
                case "html":
                    return "text/html";
                case "xml":
                    return "application/html";
                case "csv":
                    return "text/plain";
            }
        };


        //this.saveData = (function () {
        //    var a = document.createElement("a");
        //    document.body.appendChild(a);
        //    a.style = "display: none";
        //    return function (data, fileName, mimeType) {
        //
        //        var blob = new Blob([data], {type: mimeType}),
        //            url = window.URL.createObjectURL(blob);
        //        a.href = url;
        //        a.download = fileName;
        //        a.click();
        //        window.URL.revokeObjectURL(url);
        //
        //    };
        //}());

        this.saveData = (function () {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style.display = "none";
            return function (data, fileName,mimeType) {
                var blob = new Blob([data], {type: mimeType}),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
            };
        })();

        this.upload = function (item, url) {
            var children = item.getElementsByTagName("input");

            var fileInput = {};
            for (var i = 0; i < children.length; i++) {
                if (children[i].className.indexOf('ng-hide') < 0) {
                    fileInput = children[i];
                    break;
                }
            }
            var formData = new FormData();
            formData.append("file", fileInput.files[0]);

            return $http({
                method: 'POST',
                url: url,
                headers: {'Content-Type': undefined},
                data: formData,
                transformRequest: angular.identity
            });
        };
        this.populateGrid = function (grid, data) {
            if (data instanceof Array) {
                grid.data = data;
            } else {
                grid.data = [data];
            }
        };
        this.convertEnum = function (e, className) {
            var result = [];
            for (var row in e) {
                if (e.hasOwnProperty(row)) {
                    var value = e[row].value;
                    if (value == undefined) {
                        continue;
                    }
                    var caption = $rootScope.translate('view.' + className + "." + value);

                    result.push(
                        {code: value, description: caption,index:row}
                    );

                }
            }
            return result;
        };

    }]);

})(angular.module('services'));