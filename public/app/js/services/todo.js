(function (module) {

    module.service('todo', ['$http', '$rootScope', function ($http, $rootScope) {
        var onDone = function ($item, $count) {
            if (!$item.ran) {
                $item.ran = true;
                $item.className = 'done';
                $count.decrease();
            }
        };
        $rootScope.todo = {
            actions: [
                {
                    title: "First",
                    action: onDone
                },
                {
                    title: "Second",
                    action: onDone
                },
                {
                    title: "Third",
                    action: onDone
                },
                {
                    title: "Fourth",
                    action: onDone
                }
            ]
        };
        $rootScope.todo.count = $rootScope.todo.actions.length;
    }]);

    module.run(['todo', function () {}]);


})(angular.module('services'));