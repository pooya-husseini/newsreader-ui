(function (module) {

    module.service('user', ['$http', '$rootScope', function ($http, $rootScope) {
        $http.get("/login/user").success(function (success) {
            $rootScope.user = {
                username: success.username,
                displayName: success.firstName,
                fullName: success.firstName + "  " + success.lastName,
                avatar: success.image,
                status: 'busy',
                role: success.role
            };
        });
    }]);
    module.run(['user', function () {
    }]);


})(angular.module('services'));