(function (module) {

    'use strict';

    module.factory('modal', ['$injector', '$q', function ($injector, $q) {
        return function (options, locals) {
            if (angular.isFunction(options)) {
                options = $injector.invoke(options, null, locals || {});
            }
            var deferred = $q.defer();
            if (angular.isFunction(options.then)) {
                options.then(function () {
                    modalAlert(options).then(deferred.resolve, deferred.reject);
                }, deferred.reject);
            }
            return deferred.promise;
        };
    }]);

})(angular.module('services'));