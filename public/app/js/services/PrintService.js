(function (module) {
    module.service("PrintService", ["$modal", function (e) {
        return {
            print: function (url, model) {
                return e.open({
                    templateUrl: "/app/view/PrintDialog.html",
                    size: "sm",
                    controller: "PrintCtrl",
                    windowClass:"modal-print-content",
                    resolve: {
                        url: function () {
                            return url;
                        },
                        model: function () {
                            return model;
                        }
                    }
                }).result
            }
        }
    }]).controller("PrintCtrl", ["$scope", "$modalInstance", "url", 'model', "ModelService", "$timeout", "$http", function ($scope, $modalInstance, url, Model, ModelService, $timeout, $http) {

        $scope.Action = {}, url != undefined && ($scope.url = url), Model != undefined && ($scope.Model = Model),
            $scope.Action.report = function (x) {
                var i = $scope.url + x + "/" + $scope.getLocale();
                $http.post(i, $scope.Model, {responseType: "arraybuffer"}).then(function (e) {
                    ModelService.saveData(e.data, "Report." + x, ModelService.getMimeType(x))

                })
            }
    }])
})(angular.module("application"));