/**
 * @author Mohammad Milad Naseri (m.m.naseri@gmail.com)
 * @date 14/7/16 AD
 */
'use strict';
(function ($) {
    var utils = {
        /**
         * Given an object, determines whether or not it is a function
         * @param {*} fn
         * @returns {boolean}
         */
        isFunction: function (fn) {
            return fn && fn.apply && fn.apply.apply === fn.apply;
        },
        /**
         * Given an object, determines whether or not it is an array
         * @param {*} fn
         * @returns {boolean}
         */
        isArray: function (fn) {
            return fn && fn.length && utils.isFunction(fn.push) && utils.isFunction(fn.pop);
        },
        /**
         * Iterates over the given object or array
         * @param {Object|Array} obj the object to iterate
         * @param {Function} iterator the iterator function called with the signature `(value, property)`
         * @param {*} [context] the context of the call
         */
        each: function (obj, iterator, context) {
            context = context || this;
            if (utils.isArray(obj)) {
                for (var i = 0; i < obj.length; i ++) {
                    iterator.apply(context, [obj[i], i]);
                }
            } else {
                for (var key in obj) {
                    if (!obj.hasOwnProperty(key)) {
                        continue;
                    }
                    iterator.apply(context, [obj[key], key]);
                }
            }
        },
        /**
         * Extends the source object by copying extended properties of the target into it
         * @param {Object} source the source object
         * @param {Object} target the object being extended
         * @returns {Object} the extended object
         */
        extend: function (source, target) {
            utils.each(target, function (value, property) {
                source[property] = typeof value == 'object' ? utils.extend({}, value) : value;
            });
            return source;
        },
        /**
         * Augments the given object by copying applicable properties of the target into it. Should they share a property,
         * the property on the source will be augmented, and not replaced
         * @param {Object} source
         * @param {Object} target
         * @returns {Object} a reference to the augmented object
         */
        augment: function (source, target) {
            utils.each(target, function (value, property) {
                if (typeof value == 'object' && typeof source[property] == 'object') {
                    source[property] = utils.augment(source[property], value);
                } else {
                    source[property] = value;
                }
            });
            return source;
        }};

    /**
     * Creates a modal dialog that will display the specified content.
     *
     * @param {String|Object} options The options for the modal. If it is a string, it will be set as `options.content`.
     * The options object has multiple properties:
     *  - `title`: the title for the modal (default: 'Alert')
     *  - `content`: the contents of the modal (default: ''). If this is a promise, the modal will remain in a non-interactive
     *  state until the promise is resolved with the contents of the dialog
     *  - `loading`: the content to display if the modal's contents is a promise
     *  - `buttons`: an array of the buttons to be displayed
     *  Each button is an object with the following properties:
     *   - `caption`: the caption of the button
     *   - `type`: a Bootstrap button type (default,primary,success,danger,info)
     *   - `action`: an action of the deferred object to be performed (resolve,reject,notify)
     *   - `callback`: a callback function that will be called right before the action takes place
     * @returns {Promise} a promise that will be resolved with [element, button] where element is a jQuery object
     * for the element representing the dialog (which might come in handy for accessing dialog content) and button is
     * the button that resulted in the closing (or null if it was issued via keyboard or the dismiss button)
     */
    function modalAlert(options) {
        if (typeof options != 'object') {
            options = {
                content: options
            };
        }
        if (!utils.isArray(options.buttons)) {
            delete options.buttons;

        }
        options = utils.extend({}, utils.augment({
            title: "Alert",
            content: "",
            loading: "Loading ...",
            buttons: [
//                {
//                    caption: "Cancel",
//                    type: "danger",
//                    action: "reject"
//                },
                {
                    caption: "OK",
                    type: "primary",
                    action: "resolve"
                }
            ]
        }, options));
        var deferred = $.Deferred();
        var $element= $('<div class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title"></h4></div><div class="modal-body"></div><div class="modal-footer"></div></div></div></div>');
        $element.find('.modal-title').html(options.title);
        $element.find('.modal-body').html(options.content);
        $element.find('.modal-header button.close').click(function () {
            deferred.reject($element, null);
            $element.modal('hide');
        });
        var footer = $element.find('.modal-footer');
        footer.css({
            'text-align': 'center'
        });
        utils.each(options.buttons, function (button, index) {
            button = utils.augment({
                caption: typeof button == 'string' ? button : "(untitled)",
                type: 'default',
                action: 'resolve'
            }, button);
            var element = options.buttons[index].element = $("<button type='button' class='btn'></button>");
            element.html(button.caption);
            element.addClass('btn-' + button.type);

            element.click(function () {
                if (utils.isFunction(button.callback)) {
                    button.callback($element, button);
                }
                deferred[button.action].apply(deferred, [$element, button]);
                $element.modal('hide');
            });
            footer.append(element);
        });
        if (utils.isFunction(options.content)) {
            $element.find('.modal-body').html(options.loading);
            $element.find('.modal-footer .btn').prop('disabled', true);
            options.content.then(function (content) {
                $element.find('.modal-footer .btn').prop('disabled', false);
                $element.find('.modal-body').html(content);
            });
        }
        $('body').append($element);
        $element.modal();
        $element.modal('show');
        $element.on('hidden.bs.modal', function () {
            $element.remove();
            deferred.reject($element, null);
        });
        return deferred.promise();
    }
    window.modalAlert = modalAlert.bind(null);
})(window.jQuery);