(function (module) {

    module.service('messages', ['$http', '$rootScope', function ($http, $rootScope) {
        var onRead = function ($item, $count) {
            if (!$item.ran) {
                $item.ran = true;
                $item.unread = false;
                $count.decrease();
            }
        };
        $rootScope.messages = {
            actions: [
                {
                    title: "First",
                    action: onRead,
                    unread: true
                },
                {
                    title: "Second",
                    action: onRead,
                    unread: true
                }
            ]
        };
        $rootScope.messages.count = $rootScope.messages.actions.length;
    }]);

    module.run(['messages', function () {}]);

})(angular.module('services'));