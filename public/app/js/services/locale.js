(function (module) {

    module.provider('locale', function () {
        var direction = "ltr";
        var locale = "en_US";
        var localeCodes = {
            'fa': 'IR',
            'en': 'US',
            'fr': 'FR',
            'de': 'GE',
            'it': 'IT',
            'es': 'SP'
        };
        var presetLocales = {
            'fa': 'rtl',
            'ar': 'rtl',
            'he': 'rtl',
            'en': 'ltr',
            'fr': 'ltr',
            'de': 'ltr',
            'it': 'ltr',
            'es': 'ltr',
            'pr': 'ltr'
        };
        this.setDirection = function (dir) {
            direction = dir;
            this.update();
        };
        this.setLocale = function (l) {
            locale = l;
            if (locale.indexOf('_') == -1 && angular.isDefined(localeCodes[locale])) {
                locale = locale + "_" + localeCodes[locale];
            }
            if (angular.isDefined(presetLocales[l]) || angular.isDefined(presetLocales[l.split('_', 2)[0]])) {
                this.setDirection(presetLocales[l] || presetLocales[l.split('_', 2)[0]]);
            }
        };
        this.getDirection = function () {
            return direction;
        };
        this.getLocale = function () {
            return locale;
        };
        this.update = function () {
            $('link[data-direction]').each(function () {
                var $this = $(this);
                $this.attr('rel', null);
                if ($this.attr('data-direction') == direction) {
                    $this.attr('rel', 'stylesheet');
                }
            });
        };
        var localeProvider = this;
        this.$get = function () {
            return localeProvider;
        };
    });

})(angular.module('services'));