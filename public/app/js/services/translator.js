(function (module) {

    /**
     * This is the translator service that will fetch property files from a given location and enable them
     * under namespaces.
     */
    module.provider('translator', function () {
        var config = {
            /**
             * The location of property files read by the service
             * @type {String}
             */
            location: '/app/resources/i18n',
            /**
             * The extension of the property files
             * @type {String}
             */
            extension: 'properties',
            /**
             * The default namespace. All messages under this namespace will be available with and without the namespace
             * prefix.
             * @type {String}
             */
            defaultNamespace: 'messages',
            /**
             * These are the namespaces that will be loaded initially by the service.
             */
            namespaces: ['messages'],
            /**
             * This means that each property file will be loaded only once. This is usually true, unless
             * we are making the property files dynamic on the server side.
             */
            loadOnce: true,
            /**
             * This flag enables caching of downloaded property files. For highly modularized property files
             * this should be specially true.
             */
            cacheFiles: true
        };
        //enabling all configuration properties under a setter/getter function
        angular.forEach(config, function (value, key) {
            this[key] = function (replacement) {
                if (angular.isDefined(replacement)) {
                    config[key] = replacement;
                } else {
                    return config[key];
                }
            };
        }, this);
        //This is a registry of all loaded namespaces. Each namespace will be loaded only once, if
        //we have set the loadOnce property to `true`.
        var loaded = {};
        this.config = function (replacement) {
            if (angular.isDefined(replacement)) {
                config = replacement;
            } else {
                return config;
            }
        };
        this.$get = function (propertyFileReader, $rootScope, $http, locale, bu$registryFactory, $cacheFactory, $q) {
            //This is the cache used for caching property files.
            var cache = config.cacheFiles ? $cacheFactory('propertyFileCache') : false;
            //This is the registry used for the translations.
            var registry = bu$registryFactory('translations');
            //We enabled the translations on the root scope to be used with the filter
            $rootScope.translations = {};
            /**
             * Loads the given namespace and then updates the translations on the root scope.
             * @param {String} namespace* you can set up more than one namespace to be loaded at the same time.
             * @returns {promise}
             */
            registry.load = function (namespace) {
                //This is a pattern used for determining the paths of property files. %n% should be
                //replaced with the namespace.
                var propertyFile = config.location + "/%n%." + locale.getLocale() + "." + config.extension;
                if (arguments.length > 1) {
                    var promises = [];
                    for (var i = 0; i < arguments.length; i++) {
                        promises.push(registry.load(arguments[i]));
                    }
                    return $q.all(promises);
                }
                var deferred = $q.defer();
                if (angular.isDefined(loaded[namespace])) {
                    loaded[namespace].then(function () {
                        var result = {
                            $$namespace: namespace
                        };
                        var list = registry.list();
                        angular.forEach(list, function (key) {
                            if (key.substring(0, namespace.length + 1) == namespace + '.') {
                                result[key.substring(namespace.length + 1)] = registry.get(key);
                            }
                        });
                        deferred.resolve(result);
                    });
                    return deferred.promise;
                }
                loaded[namespace] = $http.get(propertyFile.replace('%n%', namespace), {
                    cache: cache
                }).then(function (result) {
                    var properties = propertyFileReader(result.data);
                    angular.forEach(properties, function (value, key) {
                        registry.register(namespace + "." + key, value);
                        if (config.defaultNamespace == namespace) {
                            registry.register(key, value);
                        }
                    });
                    var list = registry.list();
                    angular.forEach(list, function (key) {
                        $rootScope.translations[key] = registry.get(key);
                    });
//                    $rootScope.$apply.postpone($rootScope);
                    properties.$$namespace = namespace;
                    deferred.resolve(properties);
                }, deferred.reject);
                return deferred.promise;
            };
            var _get = registry.get;
            registry.get = function (id, defaultValue) {
                var value = _get(id);
                if (angular.isUndefined(value)) {
                    return defaultValue;
                }
                return value;
            };
            for (var i = 0; i < config.namespaces.length; i++) {
                registry.load(config.namespaces[i]);
            }
            registry.loaded = function () {
                return angular.extend({}, loaded);
            };
            registry.clear = function () {
                loaded = {};
                $rootScope.translations = {};
                var list = registry.list();
                angular.forEach(list, function (item) {
                    registry.unregister(item);
                });
            };
            registry.reload = function () {
                var loaded = registry.loaded();
                registry.clear();
                var namespaces = [];
                angular.forEach(loaded, function (promise, namespace) {
                    namespaces.push(namespace);
                });
                return registry.load.apply(registry, namespaces);
            };
            return registry;
        };
        this.$get.$inject = ['propertyFileReader', '$rootScope', '$http', 'locale', 'bu$registryFactory', '$cacheFactory', '$q'];
    });

    /**
     * This is so that the translator service is instantiated upon run regardless of the dependencies set in the project
     */
    module.run(['translator', 'translationFunction', function () {
    }]);

    /**
     * This enables the `translate` filter which should be used as `translate:translations`
     */
    module.filter('translate', ['$rootScope', function ($rootScope) {
        return function (text, translations) {
            if (angular.isUndefined(translations)) {
                translations = $rootScope.translations;
            }
            if (!angular.isString(text)) {
                return text;
            }
            if (angular.isDefined(translations[text])) {
                return translations[text];
            }
            return text;
        }
    }]);

    /**
     * This is the translate directive which translates the specified properties using the loaded translations.
     * This directive is an attribute.
     */
    module.directive('translate', ['$rootScope', function ($rootScope) {
        return {
            restrict: "A",
            link: function ($scope, $element, $attrs) {
                $rootScope.$watch('translations', function (translations) {
                    $attrs.$observe('translate', function (expression) {
                        expression = expression.split(';');
                        var i;
                        for (i = 0; i < expression.length; i++) {
                            expression[i] = expression[i].split('=', 2);
                            if (expression[i].length == 0) {
                                continue;
                            }
                            if (expression[i].length == 1) {
                                expression[i] = ['innerHTML', expression[i][0]];
                            }
                        }
                        for (i = 0; i < expression.length; i++) {
                            if (expression[i].length != 2) {
                                continue;
                            }
                            $element[0][expression[i][0]] = angular.isDefined(translations[expression[i][1]]) ? translations[expression[i][1]] : expression[i][1];
                        }
                    });
                });
            }
        };
    }]);

    /**
     * We now enable a policy (translation) on the route provider to let users specify dependencies on the translations for
     * each page.
     */
    module.provider('translation', function () {
//        routeProvider.policy('translation', ['translator', function (translator) {
//            return translator.load.apply(null, this.get());
//        }]);
        this.$get = function () {
        };
        this.$new = function () {
            var args = arguments;
            return {
                type: "translation",
                get: function () {
                    return args;
                }
            }
        };
    });

    /**
     * This service simply adds a `translate()` function to the root scope. Since the root scope will be re-rendered
     * every time a new translation set is downloaded, this function will be called whenever appropriate regardless
     * of the situation.
     */
    module.service('translationFunction', ['$rootScope', 'translator', 'locale', '$route', function ($rootScope, translator, locale, $route) {

        $rootScope.translate = function (text) {
            if($rootScope.translations.hasOwnProperty(text)) {
                return $rootScope.translations[text];
            }else{
                return "";
            }
        };
        $rootScope.$on('bu.ui.text', function (event, descriptor) {
            var text = descriptor.text();
            descriptor.set($rootScope.translations[text] ? $rootScope.translations[text] : text)
        });
        $rootScope.localeChanging = false;
        $rootScope.getLocale = function () {
            return locale.getLocale();
        };


        $rootScope.localeDirection = 'rtl';


        $rootScope.setLocale = function (newLocale) {
            if (locale.getLocale() == newLocale) {
                return;
            }
            $rootScope.localeChanging = true;
            locale.setLocale(newLocale);
            if (newLocale != 'fa_IR') {
                $rootScope.localeDirection = 'ltr';
            }else{
                $rootScope.localeDirection = 'rtl';
            }
            translator.reload().then(function () {
                $route.reload();
            });
        };
        $rootScope.$on('$routeChangeSuccess', function () {
            $rootScope.localeChanging = false;
        });
    }]);

})(angular.module('services'));