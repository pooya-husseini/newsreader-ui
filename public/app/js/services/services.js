/**
 * We define the services module here, to which all other services will be attached.
 */
angular.module('services', ['ngRoute']);


var toolkit = angular.module("services", [], null);
/**
 * Registry factory provider
 */
toolkit.provider("bu$registryFactory", function () {
    //we collect a hash of all created registries.
    var registries = {};
    /**
     * Flag determining whether or not instances of registries should be kept read-only, or if we should let commands
     * to recreate them be executed without error
     * @type {boolean}
     */
    var recreationDisallowed = true;
    /**
     * This flag determines whether or not we should demand for explicit creation of a registry before we allow access
     * to it
     * @type {boolean}
     */
    var initOnDemand = false;
    //this is an API, usage should not be analyzed
    //noinspection JSUnusedGlobalSymbols
    this.allowRecreation = function (allow) {
        recreationDisallowed = !allow;
    };
    //this is an API, usage should not be analyzed
    //noinspection JSUnusedGlobalSymbols
    this.allowInitOnDemand = function (allow) {
        initOnDemand = allow;
    };
    /**
     * We will initialize a registry here
     * @param {String} name
     */
    var init = function (name) {
        /**
         * This is the storage used by the registry
         * @type {{}}
         */
        var storage = {};
        /**
         * This is the callback hash used by event triggers
         * @type {{}}
         */
        var callbacks = {};
        /**
         * This function will run all callbacks listening to a certain event and return the values
         * @param {String} event the event being triggered
         * @param {*} original the original value being passed on to callbacks
         * @param {*} context the context of the event. This is usually the registry itself.
         * @param {Array|Function} args the arguments that should be passed to the event callback. If this is a function
         * it will be first invoked to determine the actual arguments
         * @returns {*}
         */
        var runCallbacks = function (event, original, context, args) {
            if (!callbacks[event]) {
                return original;
            }
            for (var i = 0; i < callbacks[event].length; i++) {
                var currentArgs = args;
                if (angular.isFunction(currentArgs)) {
                    currentArgs = currentArgs.apply(context, [event, original]);
                }
                var returned = callbacks[event][i].apply(context, currentArgs);
                if (returned !== null && typeof returned != "undefined") {
                    original = returned;
                }
            }
            return original;
        };
        /**
         * This holds the size of the registry
         * @type {number}
         */
        var size = 0;
        //this is an API, usage should not be analyzed
        //noinspection JSUnusedGlobalSymbols
        /**
         * The registry object to be used from the outside
         * @type {{register: register, unregister: unregister, get: get, list: list, info: info, on: on, off: off, trigger: trigger}}
         */
        var registry = registries[name] = {
            /**
             * Registers a new item with the given id. This triggers the `register` event on the registry.
             * @param {String} id
             * @param {*} item
             */
            register: function (id, item) {
                storage[id] = runCallbacks('register', item, registry, function (event, original) {
                    return [id, original];
                });
                if (typeof storage[id] != "undefined") {
                    size++;
                }
            },
            /**
             * Unregisters a given item from the registry. This triggers the `unregister` event.
             * @param {String} id
             */
            unregister: function (id) {
                runCallbacks('unregister', storage[id], registry, [id, storage[id]]);
                if (typeof storage[id] != "undefined") {
                    size--;
                }
                delete storage[id];
            },
            /**
             * Returns the item registered with the registry under the given id. This triggers the `get`
             * event, which allows for modification of items prior to dispensing.
             * @param {String} id
             * @returns {*}
             */
            get: function (id) {
                return runCallbacks('get', storage[id], registry, function (event, original) {
                    return [id, original];
                });
            },
            /**
             * Returns a list of IDs of all items registered with the registry
             * @returns {Array}
             */
            list: function () {
                var list = [];
                angular.forEach(storage, function (value, key) {
                    list.push(key);
                });
                return list;
            },
            /**
             * Returns an object containing the id and the size of the registry
             * @returns {{id: String, size: number}}
             */
            info: function () {
                return {
                    id: name,
                    size: size
                };
            },
            /**
             * Adds a callback to the queue for a given event
             * @param {String} event
             * @param {Function} callback
             * @returns {number} the id of the callback. This can be used to unregister it using `off`.
             */
            on: function (event, callback) {
                if (typeof callbacks[event] == "undefined") {
                    callbacks[event] = [];
                }
                callbacks[event].push(callback);
                return callbacks[event].length - 1;
            },
            /**
             * Un-registers a callback from the event listeners.
             * @param {String} event
             * @param {number} callbackIndex
             * @returns {boolean} true if the callback was unregistered
             */
            off: function (event, callbackIndex) {
                if (typeof callbacks[event] == "undefined" || callbackIndex < 0 || callbackIndex >= callbacks[event].length) {
                    return false;
                }
                callbacks[event].splice(callbackIndex, 1);
                return true;
            },
            /**
             * Triggers an event on the registry, passing all arguments as the arguments to the listeners.
             * @param {String} event
             * @param {*} [_]*
             */
            trigger: function (event, _) {
                var args = [];
                for (var i = 1; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
                runCallbacks(event, {}, registry, args);
            }
        };
    };
    /**
     * This is the actual registry factory which upon invocation will act as the constructor for the registry
     * @param {String} name
     * @returns {{register: register, unregister: unregister, get: get, list: list, info: info, on: on, off: off, trigger: trigger}}
     */
    var registryFactory = function (name) {
        if (recreationDisallowed && typeof registries[name] != "undefined") {
            throw new Error("Registry id " + name + " has already been taken");
        }
        init(name);
        return registries[name];
    };
    /**
     * Returns a previously initialized registry
     * @param {String} name
     * @returns {{register: register, unregister: unregister, get: get, list: list, info: info, on: on, off: off, trigger: trigger}}
     */
    registryFactory.get = function (name) {
        if (typeof registries[name] == "undefined") {
            if (initOnDemand) {
                init(name);
            } else {
                throw new Error("Unknown registry " + name);
            }
        }
        return registries[name];
    };
    this.$get = function () {
        return registryFactory;
    };
});


/**
 * Global configuration object accessible via 'bu$configuration'
 */
toolkit.provider('bu$configuration', function () {

    /**
     * Augment's one object with the other, so that if a property exists within the first, it is not erased
     * unless it is overridden by the second.
     * @param {Object} first the object to be augmented
     * @param {Object} second the object which will be augmenting the other
     * @returns {Object} the augmented object
     */
    var augment = function (first, second) {
        var result = first;
        angular.forEach(second, function (value, key) {
            if (typeof result[key] == "object" && typeof value == "object") {
                result[key] = augment(result[key], value);
            } else {
                result[key] = value;
            }
        });
        return result;
    };

    /**
     * Reads the value of the property at the given hierarchical address. If no such property exists, it will
     * return `undefined`.
     *
     * For instance, given the object:
     *
     *     {
         *         a: {
         *             b: 1
         *         }
         *     }
     *
     * the property index of `a.b` will result in `1`, while `a.b.x.y`, and `a.c` will both fail silently to
     * `undefined`.
     * @param {Object} obj the object to be read
     * @param {String} property the index of the property to read
     * @returns {*} the value of the specified property
     */
    var read = function (obj, property) {
        if (property.indexOf(".") == -1) {
            return obj[property];
        }
        var split = property.split(".");
        if (typeof obj[split[0]] == "object") {
            return read(obj[split[0]], split.splice(1).join("."));
        }
        return undefined;
    };

    /**
     * Works much the same way as `read`, only it overrides the given property if it exists and creates it if not.
     * @param {Object} obj the object to be modified
     * @param {String} property the index of the property
     * @param {*} value the new value of the property
     * @returns {Object} the modified object
     */
    var write = function (obj, property, value) {
        if (property.indexOf('.') == -1) {
            obj[property] = value;
            return obj;
        }
        var split = property.split(".");
        if (typeof obj[split[0]] == "undefined") {
            obj[split[0]] = {};
        }
        if (typeof obj[split[0]] == "object") {
            obj[split[0]] = write(obj[split[0]], split.splice(1).join("."), value);
        }
        return obj;
    };

    /**
     * Returns a function for the given array that will push items into the array if they do not already exist
     * @param {Array} array
     * @returns {Function}
     */
    var pushNew = function (array) {
        return function (item) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == item) {
                    return;
                }
            }
            array.push(item);
        };
    };

    /**
     * @type {Object}
     */
    var config;

    /**
     * Resets the configuration made so far, making the configuration into a bare object filled with the defaults.
     */
    this.reset = function () {
        config = function (key, value) {
            if (angular.isUndefined(value)) {
                return read(config, key);
            } else {
                config = write(config, key, value);
                return config;
            }
        };
    };
    //We reset the configuration upon built
    this.reset();
    /**
     * Given a new configuration object, will augment the existing configuration to include all the newly
     * specified values
     * @param {Object} configuration
     */
    this.set = function (configuration) {
        if (typeof configuration != "object") {
            return;
        }
        config = augment(config, configuration);
    };
    this.$get = function () {
        if (!config.base) {
            /**
             * This should point to where BoostrapUI resides in relevance to the document wherein the script
             * is being included
             * @type {string}
             */
            config.base = ".";
        }
        if (!config.templatesBase) {
            /**
             * This is the folder containing directive templates in relevance to BootstrapUI root
             * @type {string}
             */
            config.templatesBase = "templates";
        }
        if (!config.directivesBase) {
            /**
             * This is the folder containing directive definition objects
             * @type {string}
             */
            config.directivesBase = "js/directives";
        }
        if (!config.toolsBase) {
            /**
             * This is the folder containing all tool definition objects
             * @type {string}
             */
            config.toolsBase = "js/tools";
        }
        if (!config.filtersBase) {
            /**
             * This is the folder containing all filter definitions
             * @type {string}
             */
            config.filtersBase = "js/filters";
        }
        if (!config.namespace) {
            /**
             * This is the default namespace for all directives introduced via BootstrapUI.
             * @type {string}
             */
            config.namespace = "ui";
        }
        if (!config.directives) {
            /**
             * This is a list of all directives that should be preloaded by BootstrapUI
             * @type {Array}
             */
            config.directives = [];
        }
        config.directives.pushNew = pushNew(config.directives);
        config.directives.pushNew('alert');
        config.directives.pushNew('button');
        config.directives.pushNew('formAction');
        config.directives.pushNew('formContainer');
        config.directives.pushNew('formInput');
        config.directives.pushNew('formSelect');
        config.directives.pushNew('formSelectItem');
        config.directives.pushNew('grid');
        config.directives.pushNew('icon');
        if (!config.filters) {
            /**
             * This is a list of filters that should be preloaded by BootstrapUI
             * @type {Array}
             */
            config.filters = [];
        }
        config.filters.pushNew = pushNew(config.filters);
        config.filters.pushNew('paginator');
        config.filters.pushNew('range');
        if (!config.debug) {
            /**
             * This is the flag signalling whether or not debug mode is enabled
             * @type {boolean}
             */
            config.debug = false;
        }
        if (typeof config.preloadAll == "undefined") {
            /**
             * This is the flag signalling whether or not the directives should be preloaded or if the user wants
             * to preload them manually
             * @type {boolean}
             */
            config.preloadAll = true;
        }
        if (typeof config.ext != "object") {
            /**
             * This is configuration namespace reserved for extensions
             * @type {Object}
             */
            config.ext = {};
        }
        if (typeof config.tools != "object") {
            /**
             * This is configuration namespace reserved for tools
             * @type {Object}
             */
            config.tools = {};
        }
        return config;
    };
});