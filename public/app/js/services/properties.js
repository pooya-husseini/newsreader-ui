(function (module) {

    /**
     * This is a property file reader that can be invoked with the first parameter being the content of
     * the property file. It will then dispense a hash of the read properties following the conventions
     * of a Java property file.
     */
    module.provider('propertyFileReader', function () {
        this.$get = function () {
            return function (content) {
                var registry = {};
                content = content.split(/\n+/);
                for (var i = 0; i < content.length; i++) {
                    var message = content[i];
                    if (message.length == 0) {
                        continue;
                    }
                    while (i < content.length && message[message.length - 1] == '\\') {
                        message = message.substring(0, message.length - 1) + content[i + 1];
                        i++;
                    }
                    message = message.replace(/#.*/, "");
                    if (message.length == 0) {
                        continue;
                    }
                    message = message.split(/[=:]/, 2);
                    if (message.length == 1) {
                        message = [message[0], ''];
                    }
                    registry[message[0].replace(/(^\s+|\s+$)/g, '')] = message[1].replace(/(^\s+|\s+$)/g, '');
                }
                return registry;
            };
        };
    });

})(angular.module('services'));