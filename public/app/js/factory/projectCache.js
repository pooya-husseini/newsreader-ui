/**
 * Created by pooya on 20/12/14.
 */

(function (module) {
    module.factory('projectCache', function ($cacheFactory) {
        return $cacheFactory('projectCache');
    })
})(angular.module("application"));
