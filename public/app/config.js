(function (module) {

    module.factory('httpInterceptor', ['$rootScope', '$q', function ($rootScope, $q) {
        return {
            'request': function (config) {
                if (config.data != undefined) {
                    replaceObjectEmptyToNull(config.data);
                }
                return config;
            },

            'responseError': function (response) {
                // do something on error
                if (response.status === null || response.status === 500) {
                    var exceptionType = response.headers("exception_type");
                    var error = {
                        method: response.config.method,
                        url: response.config.url,
                        message: response.data,
                        status: response.status
                    };
                    if (exceptionType == undefined) {
                        notify($rootScope.translate('app.failed'), "danger");
                    } else {
                        var translation = $rootScope.translate('exception.translation.' + exceptionType);
                        if (translation == undefined) {
                            notify($rootScope.translate('app.failed'), "danger");
                        } else {
                            notify(translation, "danger");
                        }
                    }

                }
                return $q.reject(response);
            }
        };
    }]);


    module.config(['localeProvider', 'translatorProvider', '$ocLazyLoadProvider', '$httpProvider', function (localeProvider, translatorProvider, $ocLazyLoadProvider, $httpProvider) {
        localeProvider.setLocale('fa');
        translatorProvider.namespaces(['menu', 'user', 'app']);
        translatorProvider.loadOnce(false);

        $ocLazyLoadProvider.config({
            events: true,
            debug: true,
            cache: true,
            loadedModules: ['application']
        });

        $httpProvider.interceptors.push('httpInterceptor');

        if (typeof String.prototype.endsWith !== 'function') {
            String.prototype.endsWith = function (suffix) {
                return this.indexOf(suffix, this.length - suffix.length) !== -1;
            };
        }

    }]);
    function replaceObjectEmptyToNull(obj) {
        if (obj == null) {
            return;
        }
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                if (obj[i] == "") {
                    obj[i] = null;
                } else if (typeof (obj[i]) == 'object') {
                    replaceObjectEmptyToNull(obj[i]);
                }
            }
        }
    }
})(angular.module('application'));