
window.normalizeGDate = function (date, delimiter) {
    delimiter = delimiter || '/';
    return date[0] + delimiter + ('0' + date[1]).slice(-2) + delimiter + ('0' + date[2]).slice(-2);
};

window.notify = function (msg, type) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-bottom-left",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    switch (type) {
        case "warning":
            //alertify.warning(msg);
            toastr.warning(msg);
            break;
        case "normal":
            toastr.info(msg);
            break;
        case "danger":
        case "error":
            toastr.options["timeOut"] = "-1";
            toastr.options["extendedTimeOut"] = "-1";
            toastr.error(msg);
            break;
        case "success":
            toastr.success(msg);
            break;
    }
};

window.convertToJalaliDate = function (gDate) {
    var gYear = parseInt(gDate.split('-')[0]);
    var gmonth = parseInt(gDate.split('-')[1]);
    var gDay = parseInt(gDate.split('-')[2]);
    var jalali = JalaliDate.gregorianToJalali(gYear, gmonth, gDay);
    return normalizeGDate(jalali, "/");
};

window.convertToGregDate = function (gDate) {
    var gYear = parseInt(gDate.split('/')[0]);
    var gmonth = parseInt(gDate.split('/')[1]);
    var gDay = parseInt(gDate.split('/')[2]);
    return JalaliDate.jalaliToGregorian(gYear, gmonth, gDay).join('-');
};
window.makeCollectiveArray = function (a, count) {
    var r = Math.ceil(a / count);
    var result = [];
    var reminded = a;
    for (var i = 0; i < count; i++) {
        if (reminded < r) {
            result.push(reminded);
        } else {
            result.push(r);
        }
        reminded -= r;
    }
    return result;
};