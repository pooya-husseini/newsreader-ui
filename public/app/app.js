var module = angular.module('application', ['ngRoute', 'ngSanitize', 'ngAnimate', 'ngResource', 'services', 'oc.lazyLoad', 'ui.bootstrap', 'ui.grid', 'ui.grid.selection', 'ui.grid.expandable', 'ui.grid.pagination', 'ui.grid.exporter', 'chart.js']);

module.controller('ApplicationMain', ['$q', '$rootScope', 'translator', 'propertyFileReader', '$http', 'ModelService', '$injector', '$interval', '$scope',
    function ($q, $rootScope, translator, propertyFileReader, $http, ModelService, $injector, $interval, $scope) {
        const logLength = 10000;
        var stompClient = null;

        $rootScope.getLocaleLabel = function () {
            var locale=$rootScope.getLocale();
            switch (locale.toLowerCase()) {
                case "en_us":
                    return "English";
                case "fa_ir":
                    return "فارسی";
                case "de_de":
                    return "Deutsch";
            }
        };


        function connect() {
            var socket = new SockJS('/hello');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {
                //setConnected(true);
                console.log('Connected: ' + frame);
                stompClient.subscribe('/topic/logs', function (message) {
                    appendLog(message.body);
                });
            });
        }

        function disconnect() {
            if (stompClient != null) {
                stompClient.disconnect();
            }
            //setConnected(false);
            console.log("Disconnected");
        }

        //function sendName() {
        //
        //    stompClient.send("/app/hello", {}, JSON.stringify({'name': "Pooya"}));
        //}

        function appendLog(message) {
            if ($rootScope.logs == undefined) {
                $rootScope.logs = "";
            } else if ($rootScope.logs.length > logLength) {
                var number = $rootScope.logs.length - logLength;
                $rootScope.logs = $rootScope.logs.substring(number);
            }
            $scope.$apply(function () {
                $rootScope.logs += message + "\n";
            });
        }

        connect();


        $rootScope.app = {
            title: 'app.title'
        };

        $rootScope.clearLogs = function () {
            $rootScope.logs = "";
        };

        $rootScope.fireKeyDown = function ($event) {
            var s = String.fromCharCode($event.keyCode);
            var char = null;
            if (/^[a-z0-9]+$/i.test(s)) {
                char = s;
            } else if ($event.keyCode == 13) {
                char = "enter";
            } else if ($event.keyCode == 40) {
                char = "down";
            } else if ($event.keyCode == 38) {
                char = "up";
            } else if ($event.keyCode == 37) {
                char = "left";
            } else if ($event.keyCode == 39) {
                char = "right";
            } else {
                return;
            }

            var keypressed = "";
            if ($event.ctrlKey) {
                keypressed += "ctrl+"
            }
            if ($event.altKey) {
                keypressed += "alt+"
            }
            if ($event.shiftKey) {
                keypressed += "shift+"
            }
            if ($event.metaKey) {
                keypressed += "meta+"
            }
            keypressed += char;
            $rootScope.$broadcast("key_down", {
                keyPressed: keypressed
            });

        };
        //4849392581113241

//    translator.load('app');
        module.$injector = $injector;

        $rootScope.printButtons = [
            {type: "pdf", tooltip: "Pdf", imageSrc: "app/resources/images/mime/pdf.png"},
            {type: "docx", tooltip: "Microsoft Word", imageSrc: "app/resources/images/mime/docx.png"},
            {type: "html", tooltip: "Html", imageSrc: "app/resources/images/mime/html.png"},
            {type: "xlsx", tooltip: "Microsoft excel", imageSrc: "app/resources/images/mime/xlsx.png"},
            {type: "csv", tooltip: "CSV", imageSrc: "app/resources/images/mime/txt.png"}
        ];

        window.loadAllPromises = function (promises) {
            var defer = $q.defer();
            $q.all(promises).then(function () {
                defer.resolve();
            }, function () {
                defer.reject();
            });
            return defer.promise;
        };


        $rootScope.currencyDef = [
            {code: 'USD', precision: 2, banknoteName: 'دلارآمریکا', unitName: 'دلار', coinName: 'سنت'},
            {code: 'EUR', precision: 2, banknoteName: 'یورو', unitName: 'یورو', coinName: 'سنت'},
            {code: 'GBP', precision: 0, banknoteName: 'پوند', unitName: 'پوند', coinName: ''},
            {code: 'RI', precision: 0, banknoteName: 'ریال', unitName: 'ریال', coinName: ''}
        ];

        window.loadPropertyFilePromise = function (address, keyFunction, valueFunction) {
            var defer = $q.defer();
            var promises = [];
            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                angular.forEach(properties, function (value, key) {
                    if (keyFunction) {
                        promises.push(keyFunction(key));
                    }
                    if (valueFunction) {
                        promises.push(valueFunction(value));
                    }
                });
                $q.all(promises).then(function () {
                    defer.resolve();
                }, function () {
                    defer.reject();
                })
            });
            return defer.promise;
        };

        window.loadPropertyFile = function (address, keyFunction, valueFunction) {
            var defer = $q.defer();
            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                angular.forEach(properties, function (value, key) {
                    if (keyFunction) {
                        keyFunction(key)
                    }
                    if (valueFunction) {
                        valueFunction(value)
                    }
                });
                defer.resolve();
            });
            return defer.promise;
        };

        window.loadPropertyFileAccumulative = function (address, keyFunction, valueFunction) {

            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                var responseKeys = [];
                var responseValues = [];
                angular.forEach(properties, function (value, key) {
                    responseKeys.push(key);
                    responseValues.push(value);
                });
                if (keyFunction) {
                    keyFunction(responseKeys)
                }
                if (valueFunction) {
                    valueFunction(responseValues)
                }
            });
        };

        window.loadPropertyFileAccumulativePromise = function (address, keyFunction, valueFunction) {
            var defer = $q.defer();

            $http.get(address).then(function (result) {
                var properties = propertyFileReader(result.data);
                var responseKeys = [];
                var responseValues = [];
                var promises = [];
                angular.forEach(properties, function (value, key) {
                    responseKeys.push(key);
                    responseValues.push(value);
                });

                if (keyFunction != undefined) {
                    promises.push(keyFunction(responseKeys));
                }
                if (valueFunction != undefined) {
                    promises.push(valueFunction(responseValues));
                }

                $q.all(promises).then(function () {
                    defer.resolve();
                }, function () {
                    defer.reject();
                })
            });

            return defer.promise;
        };

        window.question = function (msg) {
            alertify.defaults.transition = "slide";
            alertify.defaults.theme.ok = "btn btn-primary";
            alertify.defaults.theme.cancel = "btn btn-danger";
            alertify.defaults.theme.input = "form-control";
            alertify.defaults.glossary.title = $rootScope.translate('app.question-title');
            alertify.defaults.glossary.ok = $rootScope.translate('app.yes');
            alertify.defaults.glossary.cancel = $rootScope.translate('app.no');
            var defer = $q.defer();
            msg = msg || $rootScope.translate('app.question-body');
            alertify.confirm(msg,
                function () {
                    defer.resolve();
                },
                function () {
                    defer.reject();
                });

            return defer.promise;
        };
//    window.question = function (msg) {
//        var title = $rootScope.translate('app.question-title');
//        msg = msg || $rootScope.translate('app.question-body');
//        var options = {
//            title: title,
//            content: msg,
//            loading: "Loading ...",
//            buttons: [
//                {
//                    caption: $rootScope.translate('app.yes'),
//                    type: "primary",
//                    action: "resolve"
//                },
//                {
//                    caption: $rootScope.translate('app.no'),
//                    type: "warning",
//                    action: "reject"
//                }
//            ]};
//        return modalAlert(options);
//    };

//    window.notify = function (msg, type) {
//        var title = $rootScope.translate('app.' + type);
//        var options = {
//            title: title,
//            content: msg,
//            loading: "Loading ...",
//            buttons: [
//                {
//                    caption: "OK",
//                    type: type,
//                    action: "resolve"
//                }
//            ]
//        };
//
//        return modalAlert(options);
//    };

        (function () {
            var promises = [];
            promises.push(loadPropertyFilePromise('app/resources/i18n.properties', function (x) {
                return translator.load(x);
            }));

            promises.push(translator.load('exception.translation'));
            promises.push(translator.load('AutoInject'));

            loadAllPromises(promises).then(
                function () {
                    $rootScope.i18nLoaded = true;
                }, function () {
                    $rootScope.i18nLoaded = false;
                }
            );
        })();

        //var promise = $interval(function () {
        //    $http.get('/alive').then(function () {
        //
        //    }, function () {
        //        notify($rootScope.translate('app.connection.dropped'), 'error');
        //        if (promise != undefined) {
        //            $rootScope.dropped = true;
        //            $rootScope.$broadcast("dropped", []);
        //            $interval.cancel(promise);
        //        }
        //    });
        //}, 15*60*1000);

        function getUser() {
            var defer = $q.defer();
            if ($scope.user == undefined) {
                $scope.$watch('user', function (oldValue, newValue) {
                    if (newValue != undefined && oldValue != newValue) {
                        defer.resolve(newValue);
                    }
                });
            } else {
                defer.resolve($scope.user);
            }
            return defer.promise;
        }


        //$rootScope.userIs = function (roles) {
        //
        //    if ($scope.user == undefined) {
        //        $scope.$watch('user.role', function (oldValue, newValue) {
        //            if (newValue != undefined && oldValue != newValue) {
        //                return intersect(roles, newValue).length > 0;
        //            }
        //        });
        //    } else {
        //        return intersect(roles, $scope.user.role).length > 0;
        //    }
        //};

        //$rootScope.userIs = function (roles) {
        //    getUser().then(function (user) {
        //        return intersect(roles, user.role).length > 0;
        //    });
        //};

        $rootScope.runWhenUsersIs = function (runnable, roles) {
            getUser().then(function (user) {
                if ($scope.intersect(roles, user.role).length > 0) {
                    return runnable();
                }
            });
        };

        //$rootScope.runWhenUsersIs = function (runnable, roles) {
        //    if ($scope.user == undefined) {
        //        $scope.$watch('user.role', function (oldValue, newValue) {
        //                if (newValue != undefined && oldValue != newValue) {
        //                    if (intersect(roles, newValue).length > 0) {
        //                        return runnable();
        //                    }
        //                }
        //            }
        //        )
        //    } else {
        //        if (intersect(roles, $scope.user.role).length > 0) {
        //
        //            return runnable();
        //
        //        }
        //    }
        //};
        $rootScope.intersect = function (item1, item2) {
            var a = [].concat(item1);
            var b = [].concat(item2);
            var ai = 0, bi = 0;
            var result = [];

            while (ai < a.length && bi < b.length) {
                if (a[ai] < b[bi]) {
                    ai++;
                }
                else if (a[ai] > b[bi]) {
                    bi++;
                }
                else { /* they're equal */
                    result.push(a[ai]);
                    ai++;
                    bi++;
                }
            }
            return result;
        };
    }
]);