(function (module) {
    module.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {

        $routeProvider.when('/', {
            redirectTo: '/home',
            resolve: {
                load: function ($http, $q, $ocLazyLoad) {
                    return loadModules($http, $q, $ocLazyLoad);
                }
            }
        }).when('/:page*', {
            templateUrl: function (parameters) {
                var page = parameters.page;
                if (!/\.html$/.test(page)) {
                    page = page + ".html";
                }
                return "/app/view/" + page;
            },
            resolve: {
                load: function ($http, $q, $ocLazyLoad) {
                    return loadModules($http, $q, $ocLazyLoad);
                }
            }
        });

        function loadModules($http, $q, $ocLazyLoad) {
            var defer = $q.defer();

            if (!$http.loaded) {
                var promise = loadPropertyFileAccumulativePromise('/app/resources/directives.properties', function (x) {
                    return $ocLazyLoad.load({
                        name: 'application',
                        files: x
                    })
                });

                promise.then(function () {
                    loadPropertyFileAccumulativePromise('/app/resources/controllers.properties', function (x) {
                        return $ocLazyLoad.load({
                                name: 'application',
                                files: x
                            }
                        );
                    }).then(function () {
                        defer.resolve();
                    }, function () {
                        defer.reject()
                    })
                });

            } else {
                defer.resolve();
            }
            return defer.promise;
        }
    }]);

})(angular.module('application'));