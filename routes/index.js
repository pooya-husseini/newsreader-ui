var express = require('express');
var router = express.Router();
var path = require("path");
var passport = require('passport');
var request = require('request');
var http = require('http');
var Account = require('../models/account');
var captchapng = require("captchapng");
var querystring = require('querystring');


router.get('/register', function (req, res) {
    res.sendFile(path.resolve('../views/register.html'), {});
});

router.get('/start', function (req, res) {
    res.sendFile(path.resolve('../views/start.html'), {user: req.user});
});

router.get('/captcha', function (req, res) {
    var number = parseInt(Math.random() * 90000 + 10000);
    req.session.captcha = number;
    var p = new captchapng(80, 30, number); // width,height,numeric captcha
    p.color(0, 0, 0, 0);  // First color: background (red, green, blue, alpha)
    p.color(80, 80, 80, 255); // Second color: paint (red, green, blue, alpha)
    var img = p.getBase64();
    var imgbase64 = new Buffer(img, 'base64');
    res.writeHead(200, {
        'Content-Type': 'image/png'
    });
    res.end(imgbase64);

});


//router.get('/', ensureAuthenticated, function (req, res) {
//    res.sendFile(path.resolve('../views/index.html'), {user: req.user});
//});

//router.get('/', function (req, res) {
//    if (req.isAuthenticated()) {
//        res.sendFile(path.resolve('../views/index.html'), {user: req.user});
//    } else {
//        res.redirect('/login');
//    }
//});

router.post('/register', function (req, res) {
    var reqBody = req.body;
    //var post_data = querystring.stringify({});
    request.post(
        'https://www.google.com/recaptcha/api/siteverify', {
            form: {
                secret: '6Lf9ygwTAAAAAJQ4DcD79Iuz1q9VqySWNJNo0-Rq',
                response: req.body['g-recaptcha-response']
            }
        },
        function (error, response, body) {
            if (error != null) {
                console.error(error);
            } else {
                var parse = JSON.parse(body);
                if (parse.success == true) {
                    if (reqBody.password == reqBody.repassword) {
                        Account.findByUsername(reqBody.username, function (err, obj) {
                            if (err != null) {
                                console.error(err);
                            } else {
                                if (err == null && obj == null) {
                                    Account.register(new Account({
                                        username: reqBody.username,
                                        name: reqBody.name,
                                        family: reqBody.family,
                                        email: reqBody.username,
                                        role: 'user'
                                    }), reqBody.password, function (err, account) {
                                        if (err) {
                                            console.error(err);
                                            res.redirect("/register?exception");
                                        }
                                        passport.authenticate('local')(req, res, function () {
                                            res.redirect('/');
                                        });
                                    });
                                } else {
                                    res.redirect("/register?userAlreadyRegistered");
                                }
                            }
                        });
                    } else {
                        res.redirect("/register?notSame")
                    }
                } else {
                    res.redirect("/register?captcha")
                }
            }
        }
    );
});

//router.get('/login', function (req, res) {
//    res.render('login', { user : req.user });
//res.sendFile(path.resolve('../views/login.html'), {user: req.user});
//});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/start?notValid'
}));

//router.get('/auth/yahoo',
//    //)
//    passport.authenticate('yahoo'/*, {failureRedirect: '/start'}*/),
//    function (req, res) {
//        //res.redirect('/');
//    });
//
//// GET /auth/yahoo/return
////   Use passport.authenticate() as route middleware to authenticate the
////   request.  If authentication fails, the user will be redirected back to the
////   login page.  Otherwise, the primary route function function will be called,
////   which, in this example, will redirect the user to the home page.
//router.get('/auth/yahoo/callback',function(req,res){
//        passport.authenticate('local', {failureRedirect: '/start'})(req, res, function () {
//            res.redirect('/');
//        });
//    }
//    //
//    /*function (req, res) {
//        res.redirect('/');
//    }*/);


router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/ping', function (req, res) {
    res.status(200).send("pong!");
});
//
//function ensureAuthenticated(req, res, next) {
//    if (req.isAuthenticated()) {
//        return next();
//    }
//    res.redirect('/start')
//}
/* GET home page. */
//router.get('/', function (req, res, next) {
//    res.sendFile(path.join(__dirname + "/../views/login.html"));
//});


module.exports = router;