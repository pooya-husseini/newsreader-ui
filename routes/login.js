var express = require('express');
var router = express.Router();
var path = require("path");
var passport = require('passport');

router.get('/user', function (req, res) {
    res.send({
        username: req.user.username,
        firstName: req.user.name,
        lastName: req.user.family,
        role: req.user.role,
        image: req.user.image
    })
});

module.exports = router;