var solr = require('solr-client');
var express = require('express');
var router = express.Router();
var properties = require('../utils/properties');

var client = solr.createClient(properties.solrHost, properties.solrPort, properties.solrCore);
client.basicAuth(properties.solrUsername, properties.solrPassword);

router.post('/phsys/sitereader/allNotSuccess', function (req, res, next) {
    var query = client.createQuery();
    query.q({status_s: '(failed empty)'}).start(0).rows(100).sort({
        pubDate_dt: "desc",
        author_s: "desc"
    });
    client.search(query, function (err, obj) {
        if (err != null) {
            console.error(err);
        } else {
            var result = [];
            for (var i = 0; i < obj.response.docs.length; i++) {
                var response = obj.response.docs[i];
                result.push(convertSolrObject(response));
            }
            res.send(result);
        }
    });
});
router.post('/phsys/sitereader/status', function (req, res, next) {
    var query = client.createQuery();
    query.q({status_s: 'failed', author: req.body.tag}).start(0).rows(100);
    client.search(query, function (err, objFailed) {
        if (err != null) {
            console.log(err);
        } else {
            var query = client.createQuery();
            query.q({status_s: 'empty', author: req.body.tag}).start(0).rows(100);

            client.search(query, function (err, objEmpty) {
                if (err != null) {
                    console.log(err);
                } else {
                    res.send({
                        failureCount: objFailed.response.numFound,
                        emptyCount: objEmpty.response.numFound

                    });
                }
            });
        }
    });
});

router.post('/phsys/sitereader/makeStatusFinished', function (req, res, next) {
    var body = req.body;
    body.status = 'fixed';
    var query = client.createQuery().q({id: req.body.id});
    //var solrdoc = new helios.document();
    //solrdoc.setField('id', 'value1updated', null, 'set');
    //solrdoc.setField('field_name3', 'value3', null, 'add');
    //
    //solrdoc.setField('field_name4', 'value4', 1 /*boost*/, 'set');
    //
    //solr_client.updateDoc(solrdoc, true, function(err) {
    //    if (err) console.log(err);
    //});


            client.search(query, function (err, obj) {
                if (err != null) {
                    console.error(err);
                    res.status(500).send(err);
                } else {
                    var docs = obj.response.docs;
                    if (docs.length > 0) {
                        docs[0].status_s = 'fixed';
                        delete docs[0]._version_;

                        client.deleteByID(req.body.id, {}, function (err, obj) {
                            if (err != null) {
                                console.error(err);
                                res.status(500).send(err);
                            } else {
                                client.commit({}, function (err,obj) {
                                    if (err != null) {
                                        console.error(err);
                                        res.status(500).send(err);
                                    } else {
                                        client.add(docs[0], function (err, obj) {
                                            if (err != null) {
                                                console.error(err);
                                                res.status(500).send(err);
                                            } else {
                                                client.commit({}, function (err,obj) {
                                                    if (err != null) {
                                                        console.error(err);
                                                        res.status(500).send(err);
                                                    } else {
                                                        res.status(200).send("Success!");
                                                    }
                                                });
                                            }
                                        });

                                        //res.status(200).send("Success!");
                                    }
                                });

                            }
                        });



                        //client.deleteByID(req.body.id, {}, function (err, obj) {
                        //    if (err != null) {
                        //        console.error(err);
                        //    } else {
                        //        client.commit({}, function (obj, err) {
                        //            if (err != null) {
                        //                console.log(err);
                        //            }
                        //        });
                        //        client.add(docs[0], function (err, obj) {
                        //            if (err != null) {
                        //                console.error(err);
                        //                res.status(500).send(err);
                        //            } else {
                        //                console.log(obj);
                        //                res.status(200).send("Success!");
                        //            }
                        //        });
                        //        client.commit({}, function (obj, err) {
                        //            if (err != null) {
                        //                console.log(err);
                        //            }
                        //        });
                        //    }
                        //});
                    } else {
                        console.error("Document not found");
                        res.status(500).send("Document not found");
                    }
                }
            });



});
router.post('/phsys/sitereader/searchNews', function (req, res, next) {
    var query = client.createQuery();
    var result = [];
    var businessObject = convertBusinessObject(req.body);
    query.q(businessObject)
        .start(req.body.start === null ? 0 : req.body.start)
        .rows(req.body.rows === null ? 10 : req.body.rows)
        .sort({pubDate_dt: "desc"});

    query.parameters.push('hl=true');
    query.parameters.push('hl.fl=persian_content');
    query.parameters.push('hl.snippets=100');
    query.parameters.push("hl.simple.pre=<b>");
    query.parameters.push("hl.simple.post=</b>");
    query.parameters.push('hl.fragsize=80');

    client.search(query, function (err, obj) {
        if (err !== null) {
            console.error(err)
        } else {
            var highlights = obj.highlighting;
            //obj.response.highlighting);

            for (var i = 0; i < obj.response.docs.length; i++) {
                var response = obj.response.docs[i];
                if (highlights[response.id].persian_content !== undefined) {
                    response.persian_content = "" + highlights[response.id].persian_content + "";
                }
                result.push(convertSolrObject(response));
            }

            res.send(result);
        }
    });
});

function convertSolrObject(solrObject) {
    return {
        id: solrObject.id,
        title: solrObject.title_t,
        category: solrObject.category,
        publisher: solrObject.author,
        pubDate: Date.parse(solrObject.pubDate_dt),
        links: solrObject.links_t,
        status: solrObject.status_s,
        text: solrObject.persian_content
    }
}

function convertBusinessObject(businessObject) {
    var result = {
        id: businessObject.id,
        title_t: businessObject.title,
        category: businessObject.category,
        author: businessObject.publisher,
        pubDate_dt: businessObject.pubDate,
        status_s: businessObject.status,
        links_t: businessObject.links,
        persian_text: businessObject.text
    };

    return removeEmptyItems(result);
}

function removeEmptyItems(obj) {
    var keys = Object.keys(obj).slice(0);
    var result = {};
    for (var i in keys) {
        if (obj[keys[i]] !== undefined && obj[keys[i]] != null && obj[keys[i]] != "") {
            result[keys[i]] = obj[keys[i]];
        }
    }
    return result;
}

var agencies = [{tag: 'abnanews', name: 'خبرگزاری اهل بیت'}, {tag: 'abrarnews', name: 'روزنامه ابرار'}, {
    tag: 'aftabnews',
    name: 'پایگاه خبری آفتاب'
}, {tag: 'aftabyazd', name: 'روزنامه آفتاب یزد'}, {tag: 'alvefaqnews', name: 'الوفاق'}, {
    tag: 'asreiran',
    name: 'خبرگزاری عصر ایران'
}, {tag: 'banifilm', name: 'بانی فیلم'}, {tag: 'barackobama', name: 'خبرگزاری باراک اوباما'}, {
    tag: 'borna',
    name: 'خبرگزاری برنا'
}, {tag: 'boursenews', name: 'بورس نیوز'}, {tag: 'donyaye_eghtesad', name: 'دنیای اقتصاد'}, {
    tag: 'econews',
    name: 'خبرگزاری اقتصادی ایران'
}, {tag: 'eghtesad_online', name: 'اقتصاد آنلاین'}, {
    tag: 'entekhab',
    name: 'پایگاه خبری تحلیلی انتخاب'
}, {tag: 'farda', name: 'فردا'}, {tag: 'fararu', name: 'خبرگزاری فرارو'}, {
    tag: 'fars',
    name: 'خبرگزاری فارس'
}, {tag: 'ghanun', name: 'روزنامه قانون'}, {tag: 'hayat', name: 'حیات'}, {
    tag: 'hamshahri_online',
    name: 'روزنامه همشهری'
}, {tag: 'iana', name: 'خبرگزاری کشاورزی ایران'}, {
    tag: 'ilna',
    name: 'ایلنا : خبرگزاری کار ایران'
}, {tag: 'iran_daily', name: 'Iran Daily'}, {tag: 'iran_economist', name: 'ایران اکونومیست'}, {
    tag: 'irna',
    name: 'ایرنا : خبرگزاری جمهوری اسلامی'
}, {tag: 'isna', name: 'خبرگزاری دانشجویان ایران - ایسنا'}, {
    tag: 'jahaneghtesad',
    name: 'جهان اقتصاد'
}, {tag: 'jamejam', name: 'روزنامه جام جم'}, {tag: 'jomhuri_eslami', name: 'روزنامه جمهوری اسلامی'}, {
    tag: 'kayhan',
    name: 'روزنامه کیهان'
}, {tag: 'khabar_online', name: 'خبر آنلاین'}, {tag: 'mashregh', name: 'خبرگزاری مشرق'}, {
    tag: 'mehr',
    name: 'خبرگزاری مهر'
}, {tag: 'miras_farhangi', name: 'خبرگزاری میراث فرهنگی'}, {tag: 'moj', name: 'خبرگزاری موج'}, {
    tag: 'navad',
    name: 'نود'
}, {tag: 'pana', name: 'خبرگزاری پانا'}, {tag: 'shana', name: 'شبکه اطلاع رسانی نفت و انرژی'}, {
    tag: 'taamol',
    name: 'خبرگزاری تعامل'
}, {tag: 'tabnak', name: 'سایت خبری تحلیلی تابناک'}, {
    tag: 'tse',
    name: 'بورس و اوراق بهادار تهران'
}, {tag: 'vatane_emrooz', name: 'وطن امروز'}, {tag: 'yjc', name: 'باشگاه خبرنگاران جوان'}, {
    tag: 'leader',
    name: 'حضرت آيت‌الله‌ سيدعلی خامنه‌ای'
}, {tag: 'shora_tehran', name: 'شورای شهر تهران'}, {tag: 'vana', name: 'آژانس خبری ورزش ایران'}, {
    tag: 'basij',
    name: 'خبرگزاری بسیج'
}, {tag: 'farazna', name: 'خبرگزاری فر ایران زمین'}, {tag: 'shahr', name: 'شهر'}, {
    tag: 'arya',
    name: 'خبرگزاری آریا'
}, {tag: 'javan', name: 'خبرگزاری جوان'}];


router.get('/phsys/sitereader/agencies', function (req, res, next) {
    res.send(agencies);
});


module.exports = router;


