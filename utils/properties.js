var propertiesReader = require("properties-reader");
var encryptor = require('./crypt');
var properties = propertiesReader("../resources/application.properties");

var props = {
    dbUserName: properties.get("db.username"),
    dbPassword: encryptor.decrypt(properties.get("db.password")),
    solrUsername: properties.get("solr.username"),
    solrPassword: encryptor.decrypt(properties.get("solr.password")),
    solrHost: properties.get("solr.host"),
    solrCore: properties.get("solr.core"),
    solrPort: properties.get("solr.port")
};
module.exports = props;