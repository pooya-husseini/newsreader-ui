var crypto = require("crypto");
/**
 * Get port from environment and store in Express.
 */
var algorithm = 'aes-256-gcm',
    password = '3zTvzr3p67VC61jmV54rIYu1545x4TlY',
    iv = '60iP0h6vJoEa';

var encryptor = {};

encryptor.encrypt = function (text) {
    var cipher = crypto.createCipheriv(algorithm, password, iv);
    var encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    //var tag = cipher.AuthTag();
    return encrypted;
    //tag: tag

};

encryptor.decrypt = function (encrypted) {
    var decipher = crypto.createDecipheriv(algorithm, password, iv);
    return decipher.update(encrypted, 'hex', 'utf8');
};

module.exports = encryptor;